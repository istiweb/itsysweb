-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 11, 2019 at 10:57 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `it_sysweb`
--

-- --------------------------------------------------------

--
-- Table structure for table `caches`
--

CREATE TABLE `caches` (
  `name` varchar(250) NOT NULL,
  `data` mediumtext NOT NULL,
  `expires` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `caches`
--

INSERT INTO `caches` (`name`, `data`, `expires`) VALUES
('Modules.wire/modules/', 'Page/PageFrontEdit/PageFrontEdit.module\nLanguageSupport/FieldtypePageTitleLanguage.module\nLanguageSupport/LanguageSupportFields.module\nLanguageSupport/FieldtypeTextLanguage.module\nLanguageSupport/LanguageTabs.module\nLanguageSupport/FieldtypeTextareaLanguage.module\nLanguageSupport/LanguageSupport.module\nLanguageSupport/LanguageSupportPageNames.module\nLanguageSupport/ProcessLanguageTranslator.module\nLanguageSupport/ProcessLanguage.module\nTextformatter/TextformatterSmartypants/TextformatterSmartypants.module\nTextformatter/TextformatterMarkdownExtra/TextformatterMarkdownExtra.module\nTextformatter/TextformatterPstripper.module\nTextformatter/TextformatterStripTags.module\nTextformatter/TextformatterEntities.module\nTextformatter/TextformatterNewlineBR.module\nTextformatter/TextformatterNewlineUL.module\nPagePathHistory.module\nJquery/JqueryUI/JqueryUI.module\nJquery/JqueryTableSorter/JqueryTableSorter.module\nJquery/JqueryCore/JqueryCore.module\nJquery/JqueryWireTabs/JqueryWireTabs.module\nJquery/JqueryMagnific/JqueryMagnific.module\nLazyCron.module\nImage/ImageSizerEngineIMagick/ImageSizerEngineIMagick.module\nImage/ImageSizerEngineAnimatedGif/ImageSizerEngineAnimatedGif.module\nSystem/SystemUpdater/SystemUpdater.module\nSystem/SystemNotifications/SystemNotifications.module\nSystem/SystemNotifications/FieldtypeNotifications.module\nMarkup/MarkupCache.module\nMarkup/MarkupPageArray.module\nMarkup/MarkupHTMLPurifier/MarkupHTMLPurifier.module\nMarkup/MarkupPagerNav/MarkupPagerNav.module\nMarkup/MarkupPageFields.module\nMarkup/MarkupAdminDataTable/MarkupAdminDataTable.module\nMarkup/MarkupRSS.module\nPageRender.module\nFieldtype/FieldtypeFile.module\nFieldtype/FieldtypeFieldsetOpen.module\nFieldtype/FieldtypeCache.module\nFieldtype/FieldtypePage.module\nFieldtype/FieldtypeTextarea.module\nFieldtype/FieldtypePageTable.module\nFieldtype/FieldtypeModule.module\nFieldtype/FieldtypeRepeater/FieldtypeFieldsetPage.module\nFieldtype/FieldtypeRepeater/FieldtypeRepeater.module\nFieldtype/FieldtypeRepeater/InputfieldRepeater.module\nFieldtype/FieldtypeImage.module\nFieldtype/FieldtypePageTitle.module\nFieldtype/FieldtypeURL.module\nFieldtype/FieldtypeInteger.module\nFieldtype/FieldtypeFieldsetClose.module\nFieldtype/FieldtypeOptions/FieldtypeOptions.module\nFieldtype/FieldtypePassword.module\nFieldtype/FieldtypeCheckbox.module\nFieldtype/FieldtypeFloat.module\nFieldtype/FieldtypeSelector.module\nFieldtype/FieldtypeComments/FieldtypeComments.module\nFieldtype/FieldtypeComments/InputfieldCommentsAdmin.module\nFieldtype/FieldtypeComments/CommentFilterAkismet.module\nFieldtype/FieldtypeText.module\nFieldtype/FieldtypeFieldsetTabOpen.module\nFieldtype/FieldtypeEmail.module\nFieldtype/FieldtypeDatetime.module\nAdminTheme/AdminThemeReno/AdminThemeReno.module\nAdminTheme/AdminThemeUikit/AdminThemeUikit.module\nAdminTheme/AdminThemeDefault/AdminThemeDefault.module\nPagePermissions.module\nInputfield/InputfieldInteger.module\nInputfield/InputfieldURL.module\nInputfield/InputfieldHidden.module\nInputfield/InputfieldPageTable/InputfieldPageTable.module\nInputfield/InputfieldFloat.module\nInputfield/InputfieldText.module\nInputfield/InputfieldCheckboxes/InputfieldCheckboxes.module\nInputfield/InputfieldSelect.module\nInputfield/InputfieldFieldset.module\nInputfield/InputfieldImage/InputfieldImage.module\nInputfield/InputfieldTextarea.module\nInputfield/InputfieldFile/InputfieldFile.module\nInputfield/InputfieldButton.module\nInputfield/InputfieldAsmSelect/InputfieldAsmSelect.module\nInputfield/InputfieldPage/InputfieldPage.module\nInputfield/InputfieldEmail.module\nInputfield/InputfieldPageName/InputfieldPageName.module\nInputfield/InputfieldDatetime/InputfieldDatetime.module\nInputfield/InputfieldCKEditor/InputfieldCKEditor.module\nInputfield/InputfieldMarkup.module\nInputfield/InputfieldSelectMultiple.module\nInputfield/InputfieldIcon/InputfieldIcon.module\nInputfield/InputfieldRadios/InputfieldRadios.module\nInputfield/InputfieldPageTitle/InputfieldPageTitle.module\nInputfield/InputfieldPassword/InputfieldPassword.module\nInputfield/InputfieldPageAutocomplete/InputfieldPageAutocomplete.module\nInputfield/InputfieldSelector/InputfieldSelector.module\nInputfield/InputfieldSubmit/InputfieldSubmit.module\nInputfield/InputfieldForm.module\nInputfield/InputfieldPageListSelect/InputfieldPageListSelect.module\nInputfield/InputfieldPageListSelect/InputfieldPageListSelectMultiple.module\nInputfield/InputfieldCheckbox.module\nInputfield/InputfieldName.module\nPagePaths.module\nFileCompilerTags.module\nProcess/ProcessModule/ProcessModule.module\nProcess/ProcessPageClone.module\nProcess/ProcessField/ProcessField.module\nProcess/ProcessPermission/ProcessPermission.module\nProcess/ProcessPageEditLink/ProcessPageEditLink.module\nProcess/ProcessLogger/ProcessLogger.module\nProcess/ProcessPageLister/ProcessPageLister.module\nProcess/ProcessHome.module\nProcess/ProcessRole/ProcessRole.module\nProcess/ProcessPageView.module\nProcess/ProcessPageSearch/ProcessPageSearch.module\nProcess/ProcessPageList/ProcessPageList.module\nProcess/ProcessPageType/ProcessPageType.module\nProcess/ProcessPageEdit/ProcessPageEdit.module\nProcess/ProcessPageEditImageSelect/ProcessPageEditImageSelect.module\nProcess/ProcessCommentsManager/ProcessCommentsManager.module\nProcess/ProcessList.module\nProcess/ProcessPageSort.module\nProcess/ProcessUser/ProcessUser.module\nProcess/ProcessForgotPassword.module\nProcess/ProcessPagesExportImport/ProcessPagesExportImport.module\nProcess/ProcessLogin/ProcessLogin.module\nProcess/ProcessPageTrash.module\nProcess/ProcessRecentPages/ProcessRecentPages.module\nProcess/ProcessTemplate/ProcessTemplate.module\nProcess/ProcessPageAdd/ProcessPageAdd.module\nProcess/ProcessProfile/ProcessProfile.module\nSession/SessionLoginThrottle/SessionLoginThrottle.module\nSession/SessionHandlerDB/ProcessSessionDB.module\nSession/SessionHandlerDB/SessionHandlerDB.module', '2010-04-08 03:10:10'),
('ModulesUninstalled.info', '{\"PageFrontEdit\":{\"name\":\"PageFrontEdit\",\"title\":\"Front-End Page Editor\",\"version\":3,\"versionStr\":\"0.0.3\",\"author\":\"Ryan Cramer\",\"summary\":\"Enables front-end editing of page fields.\",\"icon\":\"cube\",\"permissions\":{\"page-edit-front\":\"Use the front-end page editor\"},\"autoload\":true,\"created\":1545409848,\"installed\":false,\"configurable\":\"PageFrontEditConfig.php\",\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"license\":\"MPL 2.0\"},\"FieldtypePageTitleLanguage\":{\"name\":\"FieldtypePageTitleLanguage\",\"title\":\"Page Title (Multi-Language)\",\"version\":100,\"versionStr\":\"1.0.0\",\"author\":\"Ryan Cramer\",\"summary\":\"Field that stores a page title in multiple languages. Use this only if you want title inputs created for ALL languages on ALL pages. Otherwise create separate languaged-named title fields, i.e. title_fr, title_es, title_fi, etc. \",\"requiresVersions\":{\"LanguageSupportFields\":[\">=\",0],\"FieldtypeTextLanguage\":[\">=\",0]},\"created\":1545409848,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"LanguageSupportFields\":{\"name\":\"LanguageSupportFields\",\"title\":\"Languages Support - Fields\",\"version\":100,\"versionStr\":\"1.0.0\",\"author\":\"Ryan Cramer\",\"summary\":\"Required to use multi-language fields.\",\"requiresVersions\":{\"LanguageSupport\":[\">=\",0]},\"installs\":[\"FieldtypePageTitleLanguage\",\"FieldtypeTextareaLanguage\",\"FieldtypeTextLanguage\"],\"autoload\":true,\"singular\":true,\"created\":1545409848,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeTextLanguage\":{\"name\":\"FieldtypeTextLanguage\",\"title\":\"Text (Multi-language)\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Field that stores a single line of text in multiple languages\",\"requiresVersions\":{\"LanguageSupportFields\":[\">=\",0]},\"created\":1545409848,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"LanguageTabs\":{\"name\":\"LanguageTabs\",\"title\":\"Languages Support - Tabs\",\"version\":114,\"versionStr\":\"1.1.4\",\"author\":\"adamspruijt, ryan\",\"summary\":\"Organizes multi-language fields into tabs for a cleaner easier to use interface.\",\"requiresVersions\":{\"LanguageSupport\":[\">=\",0]},\"autoload\":\"template=admin\",\"singular\":true,\"created\":1545409848,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeTextareaLanguage\":{\"name\":\"FieldtypeTextareaLanguage\",\"title\":\"Textarea (Multi-language)\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Field that stores a multiple lines of text in multiple languages\",\"requiresVersions\":{\"LanguageSupportFields\":[\">=\",0]},\"created\":1545409848,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"LanguageSupport\":{\"name\":\"LanguageSupport\",\"title\":\"Languages Support\",\"version\":103,\"versionStr\":\"1.0.3\",\"author\":\"Ryan Cramer\",\"summary\":\"ProcessWire multi-language support.\",\"installs\":[\"ProcessLanguage\",\"ProcessLanguageTranslator\"],\"autoload\":true,\"singular\":true,\"created\":1545409848,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"addFlag\":32},\"LanguageSupportPageNames\":{\"name\":\"LanguageSupportPageNames\",\"title\":\"Languages Support - Page Names\",\"version\":10,\"versionStr\":\"0.1.0\",\"author\":\"Ryan Cramer\",\"summary\":\"Required to use multi-language page names.\",\"requiresVersions\":{\"LanguageSupport\":[\">=\",0],\"LanguageSupportFields\":[\">=\",0]},\"autoload\":true,\"singular\":true,\"created\":1545409848,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"ProcessLanguageTranslator\":{\"name\":\"ProcessLanguageTranslator\",\"title\":\"Language Translator\",\"version\":101,\"versionStr\":\"1.0.1\",\"author\":\"Ryan Cramer\",\"summary\":\"Provides language translation capabilities for ProcessWire core and modules.\",\"requiresVersions\":{\"LanguageSupport\":[\">=\",0]},\"permission\":\"lang-edit\",\"created\":1545409848,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"ProcessLanguage\":{\"name\":\"ProcessLanguage\",\"title\":\"Languages\",\"version\":103,\"versionStr\":\"1.0.3\",\"author\":\"Ryan Cramer\",\"summary\":\"Manage system languages\",\"icon\":\"language\",\"requiresVersions\":{\"LanguageSupport\":[\">=\",0]},\"permission\":\"lang-edit\",\"permissions\":{\"lang-edit\":\"Administer languages and static translation files\"},\"created\":1545409848,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"useNavJSON\":true},\"TextformatterSmartypants\":{\"name\":\"TextformatterSmartypants\",\"title\":\"SmartyPants Typographer\",\"version\":171,\"versionStr\":\"1.7.1\",\"summary\":\"Smart typography for web sites, by Michel Fortin based on SmartyPants by John Gruber. If combined with Markdown, it should be applied AFTER Markdown.\",\"created\":1545409848,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"url\":\"https:\\/\\/github.com\\/michelf\\/php-smartypants\"},\"TextformatterPstripper\":{\"name\":\"TextformatterPstripper\",\"title\":\"Paragraph Stripper\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Strips paragraph <p> tags that may have been applied by other text formatters before it. \",\"created\":1545409848,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterStripTags\":{\"name\":\"TextformatterStripTags\",\"title\":\"Strip Markup Tags\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Strips HTML\\/XHTML Markup Tags\",\"created\":1545409848,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterNewlineBR\":{\"name\":\"TextformatterNewlineBR\",\"title\":\"Newlines to XHTML Line Breaks\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Converts newlines to XHTML line break <br \\/> tags. \",\"created\":1545409848,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterNewlineUL\":{\"name\":\"TextformatterNewlineUL\",\"title\":\"Newlines to Unordered List\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Converts newlines to <li> list items and surrounds in an <ul> unordered list. \",\"created\":1545409848,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"LazyCron\":{\"name\":\"LazyCron\",\"title\":\"Lazy Cron\",\"version\":102,\"versionStr\":\"1.0.2\",\"summary\":\"Provides hooks that are automatically executed at various intervals. It is called \'lazy\' because it\'s triggered by a pageview, so the interval is guaranteed to be at least the time requested, rather than exactly the time requested. This is fine for most cases, but you can make it not lazy by connecting this to a real CRON job. See the module file for details. \",\"href\":\"https:\\/\\/processwire.com\\/api\\/modules\\/lazy-cron\\/\",\"autoload\":true,\"singular\":true,\"created\":1545409848,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"ImageSizerEngineIMagick\":{\"name\":\"ImageSizerEngineIMagick\",\"title\":\"IMagick Image Sizer\",\"version\":2,\"versionStr\":\"0.0.2\",\"author\":\"Horst Nogajski\",\"summary\":\"Upgrades image manipulations to use PHP\'s ImageMagick library when possible.\",\"created\":1545409848,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"ImageSizerEngineAnimatedGif\":{\"name\":\"ImageSizerEngineAnimatedGif\",\"title\":\"Animated GIF Image Sizer\",\"version\":1,\"versionStr\":\"0.0.1\",\"author\":\"Horst Nogajski\",\"summary\":\"Upgrades image manipulations for animated GIFs.\",\"created\":1545409848,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"SystemNotifications\":{\"name\":\"SystemNotifications\",\"title\":\"System Notifications\",\"version\":12,\"versionStr\":\"0.1.2\",\"summary\":\"Adds support for notifications in ProcessWire (currently in development)\",\"icon\":\"bell\",\"installs\":[\"FieldtypeNotifications\"],\"autoload\":true,\"created\":1545409848,\"installed\":false,\"configurable\":\"SystemNotificationsConfig.php\",\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeNotifications\":{\"name\":\"FieldtypeNotifications\",\"title\":\"Notifications\",\"version\":4,\"versionStr\":\"0.0.4\",\"summary\":\"Field that stores user notifications.\",\"requiresVersions\":{\"SystemNotifications\":[\">=\",0]},\"created\":1545409848,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"MarkupCache\":{\"name\":\"MarkupCache\",\"title\":\"Markup Cache\",\"version\":101,\"versionStr\":\"1.0.1\",\"summary\":\"A simple way to cache segments of markup in your templates. \",\"href\":\"https:\\/\\/processwire.com\\/api\\/modules\\/markupcache\\/\",\"autoload\":true,\"singular\":true,\"created\":1545409848,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"MarkupPageFields\":{\"name\":\"MarkupPageFields\",\"title\":\"Markup Page Fields\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Adds $page->renderFields() and $page->images->render() methods that return basic markup for output during development and debugging.\",\"autoload\":true,\"singular\":true,\"created\":1545409848,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"permanent\":true},\"MarkupRSS\":{\"name\":\"MarkupRSS\",\"title\":\"Markup RSS Feed\",\"version\":102,\"versionStr\":\"1.0.2\",\"summary\":\"Renders an RSS feed. Given a PageArray, renders an RSS feed of them.\",\"created\":1545409848,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeCache\":{\"name\":\"FieldtypeCache\",\"title\":\"Cache\",\"version\":102,\"versionStr\":\"1.0.2\",\"summary\":\"Caches the values of other fields for fewer runtime queries. Can also be used to combine multiple text fields and have them all be searchable under the cached field name.\",\"created\":1545409848,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypePageTable\":{\"name\":\"FieldtypePageTable\",\"title\":\"ProFields: Page Table\",\"version\":8,\"versionStr\":\"0.0.8\",\"summary\":\"A fieldtype containing a group of editable pages.\",\"installs\":[\"InputfieldPageTable\"],\"autoload\":true,\"created\":1545409848,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeFieldsetPage\":{\"name\":\"FieldtypeFieldsetPage\",\"title\":\"Fieldset (Page)\",\"version\":1,\"versionStr\":\"0.0.1\",\"summary\":\"Fieldset with fields isolated to separate namespace (page), enabling re-use of fields.\",\"requiresVersions\":{\"FieldtypeRepeater\":[\">=\",0]},\"autoload\":true,\"created\":1545409848,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeRepeater\":{\"name\":\"FieldtypeRepeater\",\"title\":\"Repeater\",\"version\":106,\"versionStr\":\"1.0.6\",\"summary\":\"Maintains a collection of fields that are repeated for any number of times.\",\"installs\":[\"InputfieldRepeater\"],\"autoload\":true,\"created\":1545409848,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"InputfieldRepeater\":{\"name\":\"InputfieldRepeater\",\"title\":\"Repeater\",\"version\":106,\"versionStr\":\"1.0.6\",\"summary\":\"Repeats fields from another template. Provides the input for FieldtypeRepeater.\",\"requiresVersions\":{\"FieldtypeRepeater\":[\">=\",0]},\"created\":1545409848,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeSelector\":{\"name\":\"FieldtypeSelector\",\"title\":\"Selector\",\"version\":13,\"versionStr\":\"0.1.3\",\"author\":\"Avoine + ProcessWire\",\"summary\":\"Build a page finding selector visually.\",\"created\":1545409848,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeComments\":{\"name\":\"FieldtypeComments\",\"title\":\"Comments\",\"version\":107,\"versionStr\":\"1.0.7\",\"summary\":\"Field that stores user posted comments for a single Page\",\"installs\":[\"InputfieldCommentsAdmin\"],\"created\":1545409848,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"InputfieldCommentsAdmin\":{\"name\":\"InputfieldCommentsAdmin\",\"title\":\"Comments Admin\",\"version\":104,\"versionStr\":\"1.0.4\",\"summary\":\"Provides an administrative interface for working with comments\",\"requiresVersions\":{\"FieldtypeComments\":[\">=\",0]},\"created\":1545409848,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"CommentFilterAkismet\":{\"name\":\"CommentFilterAkismet\",\"title\":\"Comment Filter: Akismet\",\"version\":102,\"versionStr\":\"1.0.2\",\"summary\":\"Uses the Akismet service to identify comment spam. Module plugin for the Comments Fieldtype.\",\"requiresVersions\":{\"FieldtypeComments\":[\">=\",0]},\"created\":1545409848,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"AdminThemeReno\":{\"name\":\"AdminThemeReno\",\"title\":\"Reno\",\"version\":17,\"versionStr\":\"0.1.7\",\"author\":\"Tom Reno (Renobird)\",\"summary\":\"Admin theme for ProcessWire 2.5+ by Tom Reno (Renobird)\",\"requiresVersions\":{\"AdminThemeDefault\":[\">=\",0]},\"autoload\":\"template=admin\",\"created\":1545409848,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"InputfieldPageTable\":{\"name\":\"InputfieldPageTable\",\"title\":\"ProFields: Page Table\",\"version\":13,\"versionStr\":\"0.1.3\",\"summary\":\"Inputfield to accompany FieldtypePageTable\",\"requiresVersions\":{\"FieldtypePageTable\":[\">=\",0]},\"created\":1545409848,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"InputfieldPageAutocomplete\":{\"name\":\"InputfieldPageAutocomplete\",\"title\":\"Page Auto Complete\",\"version\":112,\"versionStr\":\"1.1.2\",\"summary\":\"Multiple Page selection using auto completion and sorting capability. Intended for use as an input field for Page reference fields.\",\"created\":1545409848,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"PagePaths\":{\"name\":\"PagePaths\",\"title\":\"Page Paths\",\"version\":1,\"versionStr\":\"0.0.1\",\"summary\":\"Enables page paths\\/urls to be queryable by selectors. Also offers potential for improved load performance. Builds an index at install (may take time on a large site). Currently supports only single languages sites.\",\"autoload\":true,\"singular\":true,\"created\":1545409848,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FileCompilerTags\":{\"name\":\"FileCompilerTags\",\"title\":\"Tags File Compiler\",\"version\":1,\"versionStr\":\"0.0.1\",\"summary\":\"Enables {var} or {var.property} variables in markup sections of a file. Can be used with any API variable.\",\"created\":1545409848,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"ProcessPageClone\":{\"name\":\"ProcessPageClone\",\"title\":\"Page Clone\",\"version\":104,\"versionStr\":\"1.0.4\",\"summary\":\"Provides ability to clone\\/copy\\/duplicate pages in the admin. Adds a \\\"copy\\\" option to all applicable pages in the PageList.\",\"permission\":\"page-clone\",\"permissions\":{\"page-clone\":\"Clone a page\",\"page-clone-tree\":\"Clone a tree of pages\"},\"autoload\":\"template=admin\",\"created\":1545409848,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"page\":{\"name\":\"clone\",\"title\":\"Clone\",\"parent\":\"page\",\"status\":1024}},\"ProcessCommentsManager\":{\"name\":\"ProcessCommentsManager\",\"title\":\"Comments\",\"version\":8,\"versionStr\":\"0.0.8\",\"author\":\"Ryan Cramer\",\"summary\":\"Manage comments in your site outside of the page editor.\",\"icon\":\"comments\",\"requiresVersions\":{\"FieldtypeComments\":[\">=\",0]},\"permission\":\"comments-manager\",\"permissions\":{\"comments-manager\":\"Use the comments manager\"},\"created\":1545409848,\"installed\":false,\"searchable\":\"comments\",\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"page\":{\"name\":\"comments\",\"parent\":\"setup\",\"title\":\"Comments\"},\"nav\":[{\"url\":\"?go=approved\",\"label\":\"Approved\"},{\"url\":\"?go=pending\",\"label\":\"Pending\"},{\"url\":\"?go=spam\",\"label\":\"Spam\"},{\"url\":\"?go=all\",\"label\":\"All\"}]},\"ProcessForgotPassword\":{\"name\":\"ProcessForgotPassword\",\"title\":\"Forgot Password\",\"version\":103,\"versionStr\":\"1.0.3\",\"summary\":\"Provides password reset\\/email capability for the Login process.\",\"permission\":\"page-view\",\"created\":1545409848,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"ProcessPagesExportImport\":{\"name\":\"ProcessPagesExportImport\",\"title\":\"Pages Export\\/Import\",\"version\":1,\"versionStr\":\"0.0.1\",\"author\":\"Ryan Cramer\",\"summary\":\"Enables exporting and importing of pages. Development version, not yet recommended for production use.\",\"icon\":\"paper-plane-o\",\"permission\":\"page-edit-export\",\"created\":1545409848,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"page\":{\"name\":\"export-import\",\"parent\":\"page\",\"title\":\"Export\\/Import\"}},\"ProcessSessionDB\":{\"name\":\"ProcessSessionDB\",\"title\":\"Sessions\",\"version\":3,\"versionStr\":\"0.0.3\",\"summary\":\"Enables you to browse active database sessions.\",\"icon\":\"dashboard\",\"requiresVersions\":{\"SessionHandlerDB\":[\">=\",0]},\"created\":1545409848,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"SessionHandlerDB\":{\"name\":\"SessionHandlerDB\",\"title\":\"Session Handler Database\",\"version\":5,\"versionStr\":\"0.0.5\",\"summary\":\"Installing this module makes ProcessWire store sessions in the database rather than the file system. Note that this module will log you out after install or uninstall.\",\"installs\":[\"ProcessSessionDB\"],\"created\":1545409848,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"Helloworld\":{\"name\":\"Helloworld\",\"title\":\"Hello World\",\"version\":3,\"versionStr\":\"0.0.3\",\"summary\":\"An example module used for demonstration purposes.\",\"href\":\"https:\\/\\/processwire.com\",\"icon\":\"smile-o\",\"autoload\":true,\"singular\":true,\"created\":1545409848,\"installed\":false}}', '2010-04-08 03:10:10'),
('ModulesVerbose.info', '{\"170\":{\"summary\":\"Markdown\\/Parsedown extra lightweight markup language by Emanuil Rusev. Based on Markdown by John Gruber.\",\"core\":true,\"versionStr\":\"1.3.0\"},\"61\":{\"summary\":\"Entity encode ampersands, quotes (single and double) and greater-than\\/less-than signs using htmlspecialchars(str, ENT_QUOTES). It is recommended that you use this on all text\\/textarea fields except those using a rich text editor or a markup language like Markdown.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"152\":{\"summary\":\"Keeps track of past URLs where pages have lived and automatically redirects (301 permament) to the new location whenever the past URL is accessed.\",\"core\":true,\"versionStr\":\"0.0.4\"},\"117\":{\"summary\":\"jQuery UI as required by ProcessWire and plugins\",\"href\":\"http:\\/\\/ui.jquery.com\",\"core\":true,\"versionStr\":\"1.9.6\"},\"103\":{\"summary\":\"Provides a jQuery plugin for sorting tables.\",\"href\":\"http:\\/\\/mottie.github.io\\/tablesorter\\/\",\"core\":true,\"versionStr\":\"2.2.1\"},\"116\":{\"summary\":\"jQuery Core as required by ProcessWire Admin and plugins\",\"href\":\"http:\\/\\/jquery.com\",\"core\":true,\"versionStr\":\"1.8.3\"},\"45\":{\"summary\":\"Provides a jQuery plugin for generating tabs in ProcessWire.\",\"core\":true,\"versionStr\":\"1.0.8\"},\"151\":{\"summary\":\"Provides lightbox capability for image galleries. Replacement for FancyBox. Uses Magnific Popup by @dimsemenov.\",\"href\":\"http:\\/\\/dimsemenov.com\\/plugins\\/magnific-popup\\/\",\"core\":true,\"versionStr\":\"0.0.1\"},\"139\":{\"summary\":\"Manages system versions and upgrades.\",\"core\":true,\"versionStr\":\"0.1.6\"},\"113\":{\"summary\":\"Adds renderPager() method to all PaginatedArray types, for easy pagination output. Plus a render() method to PageArray instances.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"156\":{\"summary\":\"Front-end to the HTML Purifier library.\",\"core\":true,\"versionStr\":\"4.9.2\"},\"98\":{\"summary\":\"Generates markup for pagination navigation\",\"core\":true,\"versionStr\":\"1.0.5\"},\"67\":{\"summary\":\"Generates markup for data tables used by ProcessWire admin\",\"core\":true,\"versionStr\":\"1.0.7\"},\"115\":{\"summary\":\"Adds a render method to Page and caches page output.\",\"core\":true,\"versionStr\":\"1.0.5\"},\"6\":{\"summary\":\"Field that stores one or more files\",\"core\":true,\"versionStr\":\"1.0.5\"},\"105\":{\"summary\":\"Open a fieldset to group fields. Should be followed by a Fieldset (Close) after one or more fields.\",\"core\":true,\"versionStr\":\"1.0.1\"},\"4\":{\"summary\":\"Field that stores one or more references to ProcessWire pages\",\"core\":true,\"versionStr\":\"1.0.5\"},\"1\":{\"summary\":\"Field that stores multiple lines of text\",\"core\":true,\"versionStr\":\"1.0.7\"},\"27\":{\"summary\":\"Field that stores a reference to another module\",\"core\":true,\"versionStr\":\"1.0.1\"},\"57\":{\"summary\":\"Field that stores one or more GIF, JPG, or PNG images\",\"core\":true,\"versionStr\":\"1.0.1\"},\"111\":{\"summary\":\"Field that stores a page title\",\"core\":true,\"versionStr\":\"1.0.0\"},\"135\":{\"summary\":\"Field that stores a URL\",\"core\":true,\"versionStr\":\"1.0.1\"},\"84\":{\"summary\":\"Field that stores an integer\",\"core\":true,\"versionStr\":\"1.0.1\"},\"106\":{\"summary\":\"Close a fieldset opened by FieldsetOpen. \",\"core\":true,\"versionStr\":\"1.0.0\"},\"162\":{\"summary\":\"Field that stores single and multi select options.\",\"core\":true,\"versionStr\":\"0.0.1\"},\"133\":{\"summary\":\"Field that stores a hashed and salted password\",\"core\":true,\"versionStr\":\"1.0.1\"},\"97\":{\"summary\":\"This Fieldtype stores an ON\\/OFF toggle via a single checkbox. The ON value is 1 and OFF value is 0.\",\"core\":true,\"versionStr\":\"1.0.1\"},\"89\":{\"summary\":\"Field that stores a floating point (decimal) number\",\"core\":true,\"versionStr\":\"1.0.5\"},\"3\":{\"summary\":\"Field that stores a single line of text\",\"core\":true,\"versionStr\":\"1.0.0\"},\"107\":{\"summary\":\"Open a fieldset to group fields. Same as Fieldset (Open) except that it displays in a tab instead.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"29\":{\"summary\":\"Field that stores an e-mail address\",\"core\":true,\"versionStr\":\"1.0.0\"},\"28\":{\"summary\":\"Field that stores a date and optionally time\",\"core\":true,\"versionStr\":\"1.0.4\"},\"159\":{\"summary\":\"Uikit v3 admin theme\",\"core\":true,\"versionStr\":\"0.3.0\"},\"148\":{\"summary\":\"Minimal admin theme that supports all ProcessWire features.\",\"core\":true,\"versionStr\":\"0.1.4\"},\"114\":{\"summary\":\"Adds various permission methods to Page objects that are used by Process modules.\",\"core\":true,\"versionStr\":\"1.0.5\"},\"85\":{\"summary\":\"Integer (positive or negative)\",\"core\":true,\"versionStr\":\"1.0.4\"},\"108\":{\"summary\":\"URL in valid format\",\"core\":true,\"versionStr\":\"1.0.2\"},\"40\":{\"summary\":\"Hidden value in a form\",\"core\":true,\"versionStr\":\"1.0.1\"},\"90\":{\"summary\":\"Floating point number with precision\",\"core\":true,\"versionStr\":\"1.0.3\"},\"34\":{\"summary\":\"Single line of text\",\"core\":true,\"versionStr\":\"1.0.6\"},\"38\":{\"summary\":\"Multiple checkbox toggles\",\"core\":true,\"versionStr\":\"1.0.7\"},\"36\":{\"summary\":\"Selection of a single value from a select pulldown\",\"core\":true,\"versionStr\":\"1.0.2\"},\"78\":{\"summary\":\"Groups one or more fields together in a container\",\"core\":true,\"versionStr\":\"1.0.1\"},\"56\":{\"summary\":\"One or more image uploads (sortable)\",\"core\":true,\"versionStr\":\"1.2.2\"},\"35\":{\"summary\":\"Multiple lines of text\",\"core\":true,\"versionStr\":\"1.0.3\"},\"55\":{\"summary\":\"One or more file uploads (sortable)\",\"core\":true,\"versionStr\":\"1.2.5\"},\"131\":{\"summary\":\"Form button element that you can optionally pass an href attribute to.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"25\":{\"summary\":\"Multiple selection, progressive enhancement to select multiple\",\"core\":true,\"versionStr\":\"1.2.1\"},\"60\":{\"summary\":\"Select one or more pages\",\"core\":true,\"versionStr\":\"1.0.7\"},\"80\":{\"summary\":\"E-Mail address in valid format\",\"core\":true,\"versionStr\":\"1.0.1\"},\"86\":{\"summary\":\"Text input validated as a ProcessWire Page name field\",\"core\":true,\"versionStr\":\"1.0.6\"},\"94\":{\"summary\":\"Inputfield that accepts date and optionally time\",\"core\":true,\"versionStr\":\"1.0.6\"},\"155\":{\"summary\":\"CKEditor textarea rich text editor.\",\"core\":true,\"versionStr\":\"1.6.1\"},\"79\":{\"summary\":\"Contains any other markup and optionally child Inputfields\",\"core\":true,\"versionStr\":\"1.0.2\"},\"43\":{\"summary\":\"Select multiple items from a list\",\"core\":true,\"versionStr\":\"1.0.1\"},\"161\":{\"summary\":\"Select an icon\",\"core\":true,\"versionStr\":\"0.0.2\"},\"39\":{\"summary\":\"Radio buttons for selection of a single item\",\"core\":true,\"versionStr\":\"1.0.5\"},\"112\":{\"summary\":\"Handles input of Page Title and auto-generation of Page Name (when name is blank)\",\"core\":true,\"versionStr\":\"1.0.2\"},\"122\":{\"summary\":\"Password input with confirmation field that doesn\'t ever echo the input back.\",\"core\":true,\"versionStr\":\"1.0.2\"},\"149\":{\"summary\":\"Build a page finding selector visually.\",\"author\":\"Avoine + ProcessWire\",\"core\":true,\"versionStr\":\"0.2.7\"},\"32\":{\"summary\":\"Form submit button\",\"core\":true,\"versionStr\":\"1.0.2\"},\"30\":{\"summary\":\"Contains one or more fields in a form\",\"core\":true,\"versionStr\":\"1.0.7\"},\"15\":{\"summary\":\"Selection of a single page from a ProcessWire page tree list\",\"core\":true,\"versionStr\":\"1.0.1\"},\"137\":{\"summary\":\"Selection of multiple pages from a ProcessWire page tree list\",\"core\":true,\"versionStr\":\"1.0.2\"},\"37\":{\"summary\":\"Single checkbox toggle\",\"core\":true,\"versionStr\":\"1.0.5\"},\"41\":{\"summary\":\"Text input validated as a ProcessWire name field\",\"core\":true,\"versionStr\":\"1.0.0\"},\"50\":{\"summary\":\"List, edit or install\\/uninstall modules\",\"core\":true,\"versionStr\":\"1.1.8\"},\"48\":{\"summary\":\"Edit individual fields that hold page data\",\"core\":true,\"versionStr\":\"1.1.3\",\"searchable\":\"fields\"},\"136\":{\"summary\":\"Manage system permissions\",\"core\":true,\"versionStr\":\"1.0.1\"},\"121\":{\"summary\":\"Provides a link capability as used by some Fieldtype modules (like rich text editors).\",\"core\":true,\"versionStr\":\"1.0.8\"},\"160\":{\"summary\":\"View and manage system logs.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"0.0.1\",\"permissions\":{\"logs-view\":\"Can view system logs\",\"logs-edit\":\"Can manage system logs\"},\"page\":{\"name\":\"logs\",\"parent\":\"setup\",\"title\":\"Logs\"}},\"150\":{\"summary\":\"Admin tool for finding and listing pages by any property.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"0.2.4\",\"permissions\":{\"page-lister\":\"Use Page Lister\"}},\"87\":{\"summary\":\"Acts as a placeholder Process for the admin root. Ensures proper flow control after login.\",\"core\":true,\"versionStr\":\"1.0.1\"},\"68\":{\"summary\":\"Manage user roles and what permissions are attached\",\"core\":true,\"versionStr\":\"1.0.4\"},\"83\":{\"summary\":\"All page views are routed through this Process\",\"core\":true,\"versionStr\":\"1.0.4\"},\"104\":{\"summary\":\"Provides a page search engine for admin use.\",\"core\":true,\"versionStr\":\"1.0.6\"},\"12\":{\"summary\":\"List pages in a hierarchal tree structure\",\"core\":true,\"versionStr\":\"1.2.2\"},\"134\":{\"summary\":\"List, Edit and Add pages of a specific type\",\"core\":true,\"versionStr\":\"1.0.1\"},\"7\":{\"summary\":\"Edit a Page\",\"core\":true,\"versionStr\":\"1.0.9\"},\"129\":{\"summary\":\"Provides image manipulation functions for image fields and rich text editors.\",\"core\":true,\"versionStr\":\"1.2.0\"},\"76\":{\"summary\":\"Lists the Process assigned to each child page of the current\",\"core\":true,\"versionStr\":\"1.0.1\"},\"14\":{\"summary\":\"Handles page sorting and moving for PageList\",\"core\":true,\"versionStr\":\"1.0.0\"},\"66\":{\"summary\":\"Manage system users\",\"core\":true,\"versionStr\":\"1.0.7\",\"searchable\":\"users\"},\"10\":{\"summary\":\"Login to ProcessWire\",\"core\":true,\"versionStr\":\"1.0.6\"},\"109\":{\"summary\":\"Handles emptying of Page trash\",\"core\":true,\"versionStr\":\"1.0.3\"},\"158\":{\"summary\":\"Shows a list of recently edited pages in your admin.\",\"author\":\"Ryan Cramer\",\"href\":\"http:\\/\\/modules.processwire.com\\/\",\"core\":true,\"versionStr\":\"0.0.2\",\"permissions\":{\"page-edit-recent\":\"Can see recently edited pages\"},\"page\":{\"name\":\"recent-pages\",\"parent\":\"page\",\"title\":\"Recent\"}},\"47\":{\"summary\":\"List and edit the templates that control page output\",\"core\":true,\"versionStr\":\"1.1.4\",\"searchable\":\"templates\"},\"17\":{\"summary\":\"Add a new page\",\"core\":true,\"versionStr\":\"1.0.8\"},\"138\":{\"summary\":\"Enables user to change their password, email address and other settings that you define.\",\"core\":true,\"versionStr\":\"1.0.4\"},\"125\":{\"summary\":\"Throttles login attempts to help prevent dictionary attacks.\",\"core\":true,\"versionStr\":\"1.0.3\"},\"169\":{\"summary\":\"Login or register for an account in ProcessWire\",\"versionStr\":\"0.0.2\"},\"164\":{\"summary\":\"Create or edit forms and manage submitted entries.\",\"versionStr\":\"0.3.2\"},\"163\":{\"summary\":\"Allows Repeater-like multiplying of fieldsets in FormBuilder forms in the frontend\",\"versionStr\":\"0.0.5\"},\"166\":{\"summary\":\"Form Builder file upload input (alpha test)\",\"versionStr\":\"0.0.2\"},\"165\":{\"summary\":\"Create or edit forms and manage submitted entries.\",\"versionStr\":\"0.3.2\"},\"168\":{\"summary\":\"Edit pages on the frontend using lightboxed admin.\",\"author\":\"Roland Toth\",\"href\":\"https:\\/\\/github.com\\/rolandtoth\\/FrontEndEditLightbox\",\"versionStr\":\"1.4.0\"},\"167\":{\"summary\":\"Import CSV files to create ProcessWire pages.\",\"versionStr\":\"1.0.6\"},\"171\":{\"summary\":\"Generates PDF files of pages. The HTML markup of the PDFs is customizable with ProcessWire templates.\",\"author\":\"Stefan Wanzenried (Wanze)\",\"href\":\"http:\\/\\/processwire.com\\/talk\\/topic\\/3008-module-pages2pdf\\/\",\"versionStr\":\"1.1.7\"},\"172\":{\"summary\":\"Wrapper class around the library mPDF to create PDF files, optimized for ProcessWire\",\"author\":\"Stefan Wanzenried (Wanze)\",\"href\":\"http:\\/\\/processwire.com\\/talk\\/topic\\/3008-module-pages2pdf\\/\",\"versionStr\":\"1.0.4\"}}', '2010-04-08 03:10:10'),
('FileCompiler__162d66e3c0bb065ceb1e4a0fdc11dc4d', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/modules\\/FormBuilder\\/FormBuilder.module\",\"hash\":\"07da93c3f15bc330a50b7a767e0a43e1\",\"size\":18534,\"time\":1556138616,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FormBuilder\\/FormBuilder.module\",\"hash\":\"84f1395204cea7d307fb65e41aaf0e94\",\"size\":19609,\"time\":1556138616}}', '2010-04-08 03:10:10'),
('FileCompiler__cbd376b861b72e89886c2692b0ae50ea', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/ready.php\",\"hash\":\"ff84b02b32ba2b25c8c3f6ee8c99fb53\",\"size\":409,\"time\":1545409848,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/ready.php\",\"hash\":\"ff84b02b32ba2b25c8c3f6ee8c99fb53\",\"size\":409,\"time\":1545409848}}', '2010-04-08 03:10:10'),
('Modules.site/modules/', 'Helloworld/Helloworld.module\nFormBuilderMultiplier-master/FormBuilderMultiplier.module\nLoginRegister/LoginRegister.module\nFormBuilder/FormBuilder.module\nFormBuilder/InputfieldFormBuilderFile.module\nFormBuilder/ProcessFormBuilder.module\nFrontEndEditLightbox/FrontEndEditLightbox.module\nPages2Pdf-master/Pages2Pdf.module\nPages2Pdf-master/WirePDF.module\nryancramerdesign-ImportPagesCSV-c3ea7f1/ImportPagesCSV.module', '2010-04-08 03:10:10'),
('FileCompiler__ca70f5a8572b5050123a6742be1b72c9', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/init.php\",\"hash\":\"c8577def0694099f1f4c2b96c9661c44\",\"size\":585,\"time\":1545409848,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/init.php\",\"hash\":\"c8577def0694099f1f4c2b96c9661c44\",\"size\":585,\"time\":1545409848}}', '2010-04-08 03:10:10'),
('FileCompiler__6cc908a92a86fd8abc6fc32c39956004', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1545409848,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1545409848}}', '2010-04-08 03:10:10'),
('FileCompiler__cadf393f42ead190b9f5e886aefdcfdb', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/finished.php\",\"hash\":\"b389c166f04239b71b589ac6ba492436\",\"size\":320,\"time\":1545409848,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/finished.php\",\"hash\":\"b389c166f04239b71b589ac6ba492436\",\"size\":320,\"time\":1545409848}}', '2010-04-08 03:10:10'),
('FileCompiler__4f036d6d15c4b61ee4e1c8286fcad0f9', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/modules\\/FormBuilder\\/FormBuilderConfig.php\",\"hash\":\"3ebae51f94053491372e6c49db2412a1\",\"size\":12726,\"time\":1556138618,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FormBuilder\\/FormBuilderConfig.php\",\"hash\":\"e3498f5b9d50a4d3cd3581e995553d46\",\"size\":13598,\"time\":1556138618}}', '2010-04-08 03:10:10'),
('FileCompiler__3defdffd1a1188d1f7a7e5fde59cd1fd', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/modules\\/FormBuilder\\/InputfieldFormBuilderFile.module\",\"hash\":\"b0c40a9ccaad717c76ceb1921032e3a2\",\"size\":12503,\"time\":1556138618,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FormBuilder\\/InputfieldFormBuilderFile.module\",\"hash\":\"947afb75c61ef23b54515a3b00758581\",\"size\":12771,\"time\":1556138618}}', '2010-04-08 03:10:10'),
('FileCompiler__db2c985dda83fc3214a855759543332e', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/modules\\/FormBuilder\\/InputfieldFormBuilder.php\",\"hash\":\"2bec87ab53922ca5796c9fbfebc350ed\",\"size\":886,\"time\":1556138616,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FormBuilder\\/InputfieldFormBuilder.php\",\"hash\":\"a3059d055e260c712414e546cd9ed29d\",\"size\":899,\"time\":1556138616}}', '2010-04-08 03:10:10'),
('FileCompiler__6d476f3449dd13e77e43d31f9b7e675f', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/modules\\/FormBuilder\\/ProcessFormBuilder.module\",\"hash\":\"a53dd61dafbfa8c04fc34f581f6e7e86\",\"size\":108637,\"time\":1556138618,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FormBuilder\\/ProcessFormBuilder.module\",\"hash\":\"ced79fed30010567073fdfba9c5522b0\",\"size\":110409,\"time\":1556138618}}', '2010-04-08 03:10:10'),
('FileCompiler__062c412ae09c05e2e99553b1a68f8192', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/modules\\/FormBuilder\\/FormBuilderMain.php\",\"hash\":\"1c0a3ad98d5f9ca828aa4b44f97da767\",\"size\":21979,\"time\":1556138618,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FormBuilder\\/FormBuilderMain.php\",\"hash\":\"93ac65cfe97e2e2c74a581e233d293c8\",\"size\":23429,\"time\":1556138618}}', '2010-04-08 03:10:10'),
('FileCompiler__af3c5cdabc7cd55002c43cdeb49524c2', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/modules\\/FormBuilder\\/FormBuilderException.php\",\"hash\":\"49499a9f91c796fdb64f15f517747f41\",\"size\":192,\"time\":1556138618,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FormBuilder\\/FormBuilderException.php\",\"hash\":\"49499a9f91c796fdb64f15f517747f41\",\"size\":192,\"time\":1556138618}}', '2010-04-08 03:10:10'),
('FileCompiler__d20998f603318e010ef32aa5e1917e7e', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/modules\\/FormBuilder\\/FormBuilderData.php\",\"hash\":\"51b492ade8da9b864734a0ba75331297\",\"size\":2179,\"time\":1556138618,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FormBuilder\\/FormBuilderData.php\",\"hash\":\"51b492ade8da9b864734a0ba75331297\",\"size\":2179,\"time\":1556138618}}', '2010-04-08 03:10:10'),
('FileCompiler__fa110725194aeea6c8b0939b60200943', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/modules\\/FormBuilder\\/FormBuilderField.php\",\"hash\":\"5e98a18e942c0323e23def82fff97010\",\"size\":7345,\"time\":1556138618,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FormBuilder\\/FormBuilderField.php\",\"hash\":\"5e98a18e942c0323e23def82fff97010\",\"size\":7345,\"time\":1556138618}}', '2010-04-08 03:10:10'),
('FileCompiler__09a49fbb4d5d37620e7de7cc64b2329b', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/modules\\/FormBuilder\\/FormBuilderForm.php\",\"hash\":\"3f5f9cb69b9ed1fb46c986a9d843b099\",\"size\":6558,\"time\":1556138618,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FormBuilder\\/FormBuilderForm.php\",\"hash\":\"cc4b99b48ba8bf0a0929690eaa7b789b\",\"size\":6972,\"time\":1556138618}}', '2010-04-08 03:10:10'),
('FileCompiler__d438fa570f60fa71cbb51f56eb0a7fd4', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/modules\\/FormBuilder\\/FormBuilderEntries.php\",\"hash\":\"e67c5b627e4e3895140a7c8dce750bca\",\"size\":11236,\"time\":1556138618,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FormBuilder\\/FormBuilderEntries.php\",\"hash\":\"67515c385a285231ac3485d79ff50ed8\",\"size\":11301,\"time\":1556138618}}', '2010-04-08 03:10:10'),
('FileCompiler__01fb2f53b34443b23cae2ca234d1bf45', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/modules\\/FormBuilder\\/FormBuilderFramework.php\",\"hash\":\"45340cb8998a6d1523d91f93b5e97093\",\"size\":4749,\"time\":1556138618,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FormBuilder\\/FormBuilderFramework.php\",\"hash\":\"6d3bf1d6d537eef4171741d87d891cdf\",\"size\":4775,\"time\":1556138618}}', '2010-04-08 03:10:10'),
('FileCompiler__3bccb57137e243ea1082c49ac103d11a', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/modules\\/FormBuilder\\/FormBuilderRender.php\",\"hash\":\"2848ffbb48af144f97252ed294d6637a\",\"size\":9564,\"time\":1556138618,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FormBuilder\\/FormBuilderRender.php\",\"hash\":\"54490173540deaa500fda508e8a33c25\",\"size\":9603,\"time\":1556138618}}', '2010-04-08 03:10:10'),
('FileCompiler__7a5d732997f70174913248813b8a9fb1', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/modules\\/FormBuilder\\/FormBuilderEmail.php\",\"hash\":\"e5f2ca74cb8b4fd063b997a67cc74abb\",\"size\":9743,\"time\":1556138616,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FormBuilder\\/FormBuilderEmail.php\",\"hash\":\"2dcb802c833a2ba7655631ed091bc4a9\",\"size\":10011,\"time\":1556138616}}', '2010-04-08 03:10:10'),
('FileCompiler__7af7f328990f87a16c39ecbb0b92d0c0', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/modules\\/FormBuilder\\/FormBuilderInstall.php\",\"hash\":\"7d06389f7bf8989a67fed728a62dc4ac\",\"size\":4434,\"time\":1556138616,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FormBuilder\\/FormBuilderInstall.php\",\"hash\":\"5dce4073908fdf348e971a1e50cf9fea\",\"size\":4622,\"time\":1556138616}}', '2010-04-08 03:10:10'),
('FileCompiler__e36a37c490d86da301322f09cd673d96', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/modules\\/FormBuilder\\/FormBuilderProcessor.php\",\"hash\":\"784a2449d6a0b3470e6ab7edcc48fd2b\",\"size\":44883,\"time\":1556138618,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FormBuilder\\/FormBuilderProcessor.php\",\"hash\":\"246e00e06bfa52e00b70909f36550e49\",\"size\":46528,\"time\":1556138618}}', '2010-04-08 03:10:10'),
('Permissions.names', '{\"form-builder\":1028,\"form-builder-add\":1029,\"logs-edit\":1013,\"logs-view\":1012,\"page-delete\":34,\"page-edit\":32,\"page-edit-recent\":1010,\"page-lister\":1006,\"page-lock\":54,\"page-move\":35,\"page-sort\":50,\"page-template\":51,\"page-view\":36,\"profile-edit\":53,\"user-admin\":52}', '2010-04-08 03:10:10'),
('FileCompiler__674fa6a72ae4fb89f5b1e41fda4990c8', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/modules\\/FormBuilder\\/frameworks\\/FormBuilderFrameworkBasic.php\",\"hash\":\"59a2b280f681558e2881cc04e1f9fece\",\"size\":4067,\"time\":1556138618,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FormBuilder\\/frameworks\\/FormBuilderFrameworkBasic.php\",\"hash\":\"acf82e073264899de19607b79db09d52\",\"size\":4093,\"time\":1556138618}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES
('FileCompiler__27424d5f0e110d35cf608c76ab16f260', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/templates\\/form-builder.php\",\"hash\":\"7aa1a9f6b5bf370ee11246731bea869a\",\"size\":1068,\"time\":1560227872,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/form-builder.php\",\"hash\":\"5786fe0ca9f386a8af9f17d2fe9d3c4a\",\"size\":1154,\"time\":1560227872}}', '2010-04-08 03:10:10'),
('FileCompiler__f2dd2a042f03605d1f4143eb5d04b582', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/modules\\/FrontEndEditLightbox\\/FrontEndEditLightbox.module\",\"hash\":\"70575df35deb23f3f366d35e62012cf4\",\"size\":20043,\"time\":1532525376,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FrontEndEditLightbox\\/FrontEndEditLightbox.module\",\"hash\":\"75a53b86f31d454e42f8a0d95bced196\",\"size\":24014,\"time\":1532525376}}', '2010-04-08 03:10:10'),
('FileCompiler__e73d4a0634740c66d968a15f095b3236', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/modules\\/ryancramerdesign-ImportPagesCSV-c3ea7f1\\/ImportPagesCSV.module\",\"hash\":\"bdda78e3b9d14fb07428d5abd102cd75\",\"size\":16089,\"time\":1471519992,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ryancramerdesign-ImportPagesCSV-c3ea7f1\\/ImportPagesCSV.module\",\"hash\":\"4892a5571873a2f2536fdd6d1cfd0140\",\"size\":16349,\"time\":1471519992}}', '2010-04-08 03:10:10'),
('FileCompiler__5c72304295898bed2d25081db62e7851', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/templates\\/_init.php\",\"hash\":\"efcac07b17fcc3a0a82bd26fac2d855d\",\"size\":1297,\"time\":1545409848,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_init.php\",\"hash\":\"efcac07b17fcc3a0a82bd26fac2d855d\",\"size\":1297,\"time\":1545409848}}', '2010-04-08 03:10:10'),
('FileCompiler__2b28045c65abf64e5f2b45e99629ec16', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/templates\\/_main.php\",\"hash\":\"d4a74144031e1be4c4fe68d6e25b9036\",\"size\":11771,\"time\":1560240651,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_main.php\",\"hash\":\"d4a74144031e1be4c4fe68d6e25b9036\",\"size\":11771,\"time\":1560240651}}', '2010-04-08 03:10:10'),
('FileCompiler__15d10aa2af8a3c2b2636fd6ce4a5389e', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/templates\\/basic-page.php\",\"hash\":\"bd2f290478915d2ed8a355dad51d3c7f\",\"size\":594,\"time\":1545409848,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/basic-page.php\",\"hash\":\"bd2f290478915d2ed8a355dad51d3c7f\",\"size\":594,\"time\":1545409848}}', '2010-04-08 03:10:10'),
('FileCompiler__aef86407cdae85ffe14169e3c25edac5', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/templates\\/cliente_listado.php\",\"hash\":\"9992c107b8585d032b288cf219aca8e3\",\"size\":3084,\"time\":1560236589,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/cliente_listado.php\",\"hash\":\"9992c107b8585d032b288cf219aca8e3\",\"size\":3084,\"time\":1560236589}}', '2010-04-08 03:10:10'),
('FileCompiler__84052cf30615356690c64cf1d0f3b047', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/templates\\/home.php\",\"hash\":\"5620c530517881a31942bd23573c5ebf\",\"size\":7474,\"time\":1560243283,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/home.php\",\"hash\":\"5620c530517881a31942bd23573c5ebf\",\"size\":7474,\"time\":1560243283}}', '2010-04-08 03:10:10'),
('FileCompiler__a250d3ffbcfde04ea2caa1fdaeab7622', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/templates\\/cliente_nuevo.php\",\"hash\":\"2525bd9b7bec45fa69d056e334e28aa6\",\"size\":1065,\"time\":1560234699,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/cliente_nuevo.php\",\"hash\":\"2525bd9b7bec45fa69d056e334e28aa6\",\"size\":1065,\"time\":1560234699}}', '2010-04-08 03:10:10'),
('FileCompiler__6503a1b07ccc051e6915284ab3ac2d67', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/modules\\/FormBuilder\\/ProcessFormBuilderEmbed.php\",\"hash\":\"6dbe41aad3fd16b09b56724e81371357\",\"size\":8091,\"time\":1556138618,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FormBuilder\\/ProcessFormBuilderEmbed.php\",\"hash\":\"c8635fc9a698c7b01edc31aa9e73646d\",\"size\":11564,\"time\":1556138618}}', '2010-04-08 03:10:10'),
('FileCompiler__1248d69db6239968dbc3153a36522e85', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/templates\\/cliente_localizacion.php\",\"hash\":\"a6c1f299a483b63a7376a2398fd4fc03\",\"size\":2434,\"time\":1560236143,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/cliente_localizacion.php\",\"hash\":\"a6c1f299a483b63a7376a2398fd4fc03\",\"size\":2434,\"time\":1560236143}}', '2010-04-08 03:10:10'),
('FileCompiler__2842da91a23fc2fdffb3102f952ce132', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/templates\\/localizacion.php\",\"hash\":\"fbe95b057cf0ed3dcc84294b13d8a73f\",\"size\":3124,\"time\":1560236644,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/localizacion.php\",\"hash\":\"fbe95b057cf0ed3dcc84294b13d8a73f\",\"size\":3124,\"time\":1560236644}}', '2010-04-08 03:10:10'),
('FileCompiler__233af96588bb79c91421928d561a7c61', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/modules\\/FrontEndEditLightbox-master\\/FrontEndEditLightbox.module\",\"hash\":\"70575df35deb23f3f366d35e62012cf4\",\"size\":20043,\"time\":1532525376,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FrontEndEditLightbox-master\\/FrontEndEditLightbox.module\",\"hash\":\"73f225398a5322be36e686ee1dc1c9f7\",\"size\":24245,\"time\":1532525376}}', '2010-04-08 03:10:10'),
('FileCompiler__52245f9244d54d91dc925fe35c62c9aa', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/templates\\/cliente_modificar.php\",\"hash\":\"c82905fd1cea7a138556631b9b697ac3\",\"size\":3638,\"time\":1560238193,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/cliente_modificar.php\",\"hash\":\"c82905fd1cea7a138556631b9b697ac3\",\"size\":3638,\"time\":1560238193}}', '2010-04-08 03:10:10'),
('FileCompiler__ecdbe9aff6b66b91259f7310daade7a0', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/templates\\/cliente_deshabilitar.php\",\"hash\":\"4f4ce1370a5f79746ee62b01f8530ad8\",\"size\":3071,\"time\":1560238172,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/cliente_deshabilitar.php\",\"hash\":\"4f4ce1370a5f79746ee62b01f8530ad8\",\"size\":3071,\"time\":1560238172}}', '2010-04-08 03:10:10'),
('Modules.info', '{\"170\":{\"name\":\"TextformatterMarkdownExtra\",\"title\":\"Markdown\\/Parsedown Extra\",\"version\":130,\"singular\":1,\"created\":1560213315,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\"},\"61\":{\"name\":\"TextformatterEntities\",\"title\":\"HTML Entity Encoder (htmlspecialchars)\",\"version\":100,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\"},\"152\":{\"name\":\"PagePathHistory\",\"title\":\"Page Path History\",\"version\":4,\"autoload\":true,\"singular\":true,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\"},\"117\":{\"name\":\"JqueryUI\",\"title\":\"jQuery UI\",\"version\":196,\"singular\":true,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"103\":{\"name\":\"JqueryTableSorter\",\"title\":\"jQuery Table Sorter Plugin\",\"version\":221,\"singular\":1,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\"},\"116\":{\"name\":\"JqueryCore\",\"title\":\"jQuery Core\",\"version\":183,\"singular\":true,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"45\":{\"name\":\"JqueryWireTabs\",\"title\":\"jQuery Wire Tabs Plugin\",\"version\":108,\"created\":1560200741,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"151\":{\"name\":\"JqueryMagnific\",\"title\":\"jQuery Magnific Popup\",\"version\":1,\"singular\":1,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\"},\"139\":{\"name\":\"SystemUpdater\",\"title\":\"System Updater\",\"version\":16,\"singular\":true,\"created\":1560200741,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"113\":{\"name\":\"MarkupPageArray\",\"title\":\"PageArray Markup\",\"version\":100,\"autoload\":true,\"singular\":true,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\"},\"156\":{\"name\":\"MarkupHTMLPurifier\",\"title\":\"HTML Purifier\",\"version\":492,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\"},\"98\":{\"name\":\"MarkupPagerNav\",\"title\":\"Pager (Pagination) Navigation\",\"version\":105,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\"},\"67\":{\"name\":\"MarkupAdminDataTable\",\"title\":\"Admin Data Table\",\"version\":107,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"115\":{\"name\":\"PageRender\",\"title\":\"Page Render\",\"version\":105,\"autoload\":true,\"singular\":true,\"created\":1560200741,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"6\":{\"name\":\"FieldtypeFile\",\"title\":\"Files\",\"version\":105,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"105\":{\"name\":\"FieldtypeFieldsetOpen\",\"title\":\"Fieldset (Open)\",\"version\":101,\"singular\":1,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"4\":{\"name\":\"FieldtypePage\",\"title\":\"Page Reference\",\"version\":105,\"created\":1560200741,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"1\":{\"name\":\"FieldtypeTextarea\",\"title\":\"Textarea\",\"version\":107,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"27\":{\"name\":\"FieldtypeModule\",\"title\":\"Module Reference\",\"version\":101,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"57\":{\"name\":\"FieldtypeImage\",\"title\":\"Images\",\"version\":101,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"111\":{\"name\":\"FieldtypePageTitle\",\"title\":\"Page Title\",\"version\":100,\"singular\":true,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"135\":{\"name\":\"FieldtypeURL\",\"title\":\"URL\",\"version\":101,\"singular\":1,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"84\":{\"name\":\"FieldtypeInteger\",\"title\":\"Integer\",\"version\":101,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"106\":{\"name\":\"FieldtypeFieldsetClose\",\"title\":\"Fieldset (Close)\",\"version\":100,\"singular\":1,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"162\":{\"name\":\"FieldtypeOptions\",\"title\":\"Select Options\",\"version\":1,\"singular\":true,\"created\":1560200880,\"namespace\":\"ProcessWire\\\\\"},\"133\":{\"name\":\"FieldtypePassword\",\"title\":\"Password\",\"version\":101,\"singular\":true,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"97\":{\"name\":\"FieldtypeCheckbox\",\"title\":\"Checkbox\",\"version\":101,\"singular\":1,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"89\":{\"name\":\"FieldtypeFloat\",\"title\":\"Float\",\"version\":105,\"singular\":1,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"3\":{\"name\":\"FieldtypeText\",\"title\":\"Text\",\"version\":100,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"107\":{\"name\":\"FieldtypeFieldsetTabOpen\",\"title\":\"Fieldset in Tab (Open)\",\"version\":100,\"singular\":1,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"29\":{\"name\":\"FieldtypeEmail\",\"title\":\"E-Mail\",\"version\":100,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\"},\"28\":{\"name\":\"FieldtypeDatetime\",\"title\":\"Datetime\",\"version\":104,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\"},\"159\":{\"name\":\"AdminThemeUikit\",\"title\":\"Uikit\",\"version\":30,\"requiresVersions\":{\"ProcessWire\":[\">=\",\"3.0.100\"]},\"autoload\":\"template=admin\",\"created\":1560200768,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\"},\"148\":{\"name\":\"AdminThemeDefault\",\"title\":\"Default\",\"version\":14,\"autoload\":\"template=admin\",\"created\":1560200741,\"configurable\":19,\"namespace\":\"ProcessWire\\\\\"},\"114\":{\"name\":\"PagePermissions\",\"title\":\"Page Permissions\",\"version\":105,\"autoload\":true,\"singular\":true,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"85\":{\"name\":\"InputfieldInteger\",\"title\":\"Integer\",\"version\":104,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"108\":{\"name\":\"InputfieldURL\",\"title\":\"URL\",\"version\":102,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\"},\"40\":{\"name\":\"InputfieldHidden\",\"title\":\"Hidden\",\"version\":101,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"90\":{\"name\":\"InputfieldFloat\",\"title\":\"Float\",\"version\":103,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"34\":{\"name\":\"InputfieldText\",\"title\":\"Text\",\"version\":106,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"38\":{\"name\":\"InputfieldCheckboxes\",\"title\":\"Checkboxes\",\"version\":107,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"36\":{\"name\":\"InputfieldSelect\",\"title\":\"Select\",\"version\":102,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"78\":{\"name\":\"InputfieldFieldset\",\"title\":\"Fieldset\",\"version\":101,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"56\":{\"name\":\"InputfieldImage\",\"title\":\"Images\",\"version\":122,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"35\":{\"name\":\"InputfieldTextarea\",\"title\":\"Textarea\",\"version\":103,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"55\":{\"name\":\"InputfieldFile\",\"title\":\"Files\",\"version\":125,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"131\":{\"name\":\"InputfieldButton\",\"title\":\"Button\",\"version\":100,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"25\":{\"name\":\"InputfieldAsmSelect\",\"title\":\"asmSelect\",\"version\":121,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"60\":{\"name\":\"InputfieldPage\",\"title\":\"Page\",\"version\":107,\"created\":1560200741,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"80\":{\"name\":\"InputfieldEmail\",\"title\":\"Email\",\"version\":101,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\"},\"86\":{\"name\":\"InputfieldPageName\",\"title\":\"Page Name\",\"version\":106,\"created\":1560200741,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"94\":{\"name\":\"InputfieldDatetime\",\"title\":\"Datetime\",\"version\":106,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"155\":{\"name\":\"InputfieldCKEditor\",\"title\":\"CKEditor\",\"version\":161,\"installs\":[\"MarkupHTMLPurifier\"],\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\"},\"79\":{\"name\":\"InputfieldMarkup\",\"title\":\"Markup\",\"version\":102,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"43\":{\"name\":\"InputfieldSelectMultiple\",\"title\":\"Select Multiple\",\"version\":101,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"161\":{\"name\":\"InputfieldIcon\",\"title\":\"Icon\",\"version\":2,\"created\":1560200775,\"namespace\":\"ProcessWire\\\\\"},\"39\":{\"name\":\"InputfieldRadios\",\"title\":\"Radio Buttons\",\"version\":105,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"112\":{\"name\":\"InputfieldPageTitle\",\"title\":\"Page Title\",\"version\":102,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"122\":{\"name\":\"InputfieldPassword\",\"title\":\"Password\",\"version\":102,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"149\":{\"name\":\"InputfieldSelector\",\"title\":\"Selector\",\"version\":27,\"autoload\":\"template=admin\",\"created\":1560200741,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"addFlag\":32},\"32\":{\"name\":\"InputfieldSubmit\",\"title\":\"Submit\",\"version\":102,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"30\":{\"name\":\"InputfieldForm\",\"title\":\"Form\",\"version\":107,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"15\":{\"name\":\"InputfieldPageListSelect\",\"title\":\"Page List Select\",\"version\":101,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"137\":{\"name\":\"InputfieldPageListSelectMultiple\",\"title\":\"Page List Select Multiple\",\"version\":102,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"37\":{\"name\":\"InputfieldCheckbox\",\"title\":\"Checkbox\",\"version\":105,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"41\":{\"name\":\"InputfieldName\",\"title\":\"Name\",\"version\":100,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"50\":{\"name\":\"ProcessModule\",\"title\":\"Modules\",\"version\":118,\"permission\":\"module-admin\",\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true,\"nav\":[{\"url\":\"?site#tab_site_modules\",\"label\":\"Site\",\"icon\":\"plug\",\"navJSON\":\"navJSON\\/?site=1\"},{\"url\":\"?core#tab_core_modules\",\"label\":\"Core\",\"icon\":\"plug\",\"navJSON\":\"navJSON\\/?core=1\"},{\"url\":\"?configurable#tab_configurable_modules\",\"label\":\"Configure\",\"icon\":\"gear\",\"navJSON\":\"navJSON\\/?configurable=1\"},{\"url\":\"?install#tab_install_modules\",\"label\":\"Install\",\"icon\":\"sign-in\",\"navJSON\":\"navJSON\\/?install=1\"},{\"url\":\"?reset=1\",\"label\":\"Refresh\",\"icon\":\"refresh\"}]},\"48\":{\"name\":\"ProcessField\",\"title\":\"Fields\",\"version\":113,\"icon\":\"cube\",\"permission\":\"field-admin\",\"created\":1560200741,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true,\"addFlag\":32},\"136\":{\"name\":\"ProcessPermission\",\"title\":\"Permissions\",\"version\":101,\"icon\":\"gear\",\"permission\":\"permission-admin\",\"singular\":1,\"created\":1560200741,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"121\":{\"name\":\"ProcessPageEditLink\",\"title\":\"Page Edit Link\",\"version\":108,\"icon\":\"link\",\"permission\":\"page-edit\",\"singular\":1,\"created\":1560200741,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"160\":{\"name\":\"ProcessLogger\",\"title\":\"Logs\",\"version\":1,\"icon\":\"tree\",\"permission\":\"logs-view\",\"singular\":1,\"created\":1560200775,\"namespace\":\"ProcessWire\\\\\",\"useNavJSON\":true},\"150\":{\"name\":\"ProcessPageLister\",\"title\":\"Lister\",\"version\":24,\"icon\":\"search\",\"permission\":\"page-lister\",\"created\":1560200741,\"configurable\":true,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true,\"addFlag\":32},\"87\":{\"name\":\"ProcessHome\",\"title\":\"Admin Home\",\"version\":101,\"permission\":\"page-view\",\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"68\":{\"name\":\"ProcessRole\",\"title\":\"Roles\",\"version\":104,\"icon\":\"gears\",\"permission\":\"role-admin\",\"created\":1560200741,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"83\":{\"name\":\"ProcessPageView\",\"title\":\"Page View\",\"version\":104,\"permission\":\"page-view\",\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"104\":{\"name\":\"ProcessPageSearch\",\"title\":\"Page Search\",\"version\":106,\"permission\":\"page-edit\",\"singular\":1,\"created\":1560200741,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"12\":{\"name\":\"ProcessPageList\",\"title\":\"Page List\",\"version\":122,\"icon\":\"sitemap\",\"permission\":\"page-edit\",\"created\":1560200741,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"134\":{\"name\":\"ProcessPageType\",\"title\":\"Page Type\",\"version\":101,\"singular\":1,\"created\":1560200741,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true,\"addFlag\":32},\"7\":{\"name\":\"ProcessPageEdit\",\"title\":\"Page Edit\",\"version\":109,\"icon\":\"edit\",\"permission\":\"page-edit\",\"singular\":1,\"created\":1560200741,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"129\":{\"name\":\"ProcessPageEditImageSelect\",\"title\":\"Page Edit Image\",\"version\":120,\"permission\":\"page-edit\",\"singular\":1,\"created\":1560200741,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"76\":{\"name\":\"ProcessList\",\"title\":\"List\",\"version\":101,\"permission\":\"page-view\",\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"14\":{\"name\":\"ProcessPageSort\",\"title\":\"Page Sort and Move\",\"version\":100,\"permission\":\"page-edit\",\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"66\":{\"name\":\"ProcessUser\",\"title\":\"Users\",\"version\":107,\"icon\":\"group\",\"permission\":\"user-admin\",\"created\":1560200741,\"configurable\":\"ProcessUserConfig.php\",\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"10\":{\"name\":\"ProcessLogin\",\"title\":\"Login\",\"version\":106,\"permission\":\"page-view\",\"created\":1560200741,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"109\":{\"name\":\"ProcessPageTrash\",\"title\":\"Page Trash\",\"version\":103,\"singular\":1,\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"158\":{\"name\":\"ProcessRecentPages\",\"title\":\"Recent Pages\",\"version\":2,\"icon\":\"clock-o\",\"permission\":\"page-edit-recent\",\"singular\":1,\"created\":1560200768,\"namespace\":\"ProcessWire\\\\\",\"useNavJSON\":true,\"nav\":[{\"url\":\"?edited=1\",\"label\":\"Edited\",\"icon\":\"users\",\"navJSON\":\"navJSON\\/?edited=1\"},{\"url\":\"?added=1\",\"label\":\"Created\",\"icon\":\"users\",\"navJSON\":\"navJSON\\/?added=1\"},{\"url\":\"?edited=1&me=1\",\"label\":\"Edited by me\",\"icon\":\"user\",\"navJSON\":\"navJSON\\/?edited=1&me=1\"},{\"url\":\"?added=1&me=1\",\"label\":\"Created by me\",\"icon\":\"user\",\"navJSON\":\"navJSON\\/?added=1&me=1\"},{\"url\":\"another\\/\",\"label\":\"Add another\",\"icon\":\"plus-circle\",\"navJSON\":\"anotherNavJSON\\/\"}]},\"47\":{\"name\":\"ProcessTemplate\",\"title\":\"Templates\",\"version\":114,\"icon\":\"cubes\",\"permission\":\"template-admin\",\"created\":1560200741,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"17\":{\"name\":\"ProcessPageAdd\",\"title\":\"Page Add\",\"version\":108,\"icon\":\"plus-circle\",\"permission\":\"page-edit\",\"created\":1560200741,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"138\":{\"name\":\"ProcessProfile\",\"title\":\"User Profile\",\"version\":104,\"permission\":\"profile-edit\",\"singular\":1,\"created\":1560200741,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"125\":{\"name\":\"SessionLoginThrottle\",\"title\":\"Session Login Throttle\",\"version\":103,\"autoload\":\"function\",\"singular\":true,\"created\":1560200741,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\"},\"169\":{\"name\":\"LoginRegister\",\"title\":\"Login\\/Register\",\"version\":2,\"icon\":\"user-plus\",\"created\":1560213314,\"configurable\":4},\"164\":{\"name\":\"FormBuilder\",\"title\":\"Form Builder\",\"version\":32,\"installs\":[\"ProcessFormBuilder\",\"InputfieldFormBuilderFile\"],\"autoload\":true,\"singular\":true,\"created\":1560202672,\"configurable\":true,\"namespace\":\"\\\\\"},\"163\":{\"name\":\"FormBuilderMultiplier\",\"title\":\"FormBuilder Fieldset Multiplier\",\"version\":\"0.0.5\",\"requiresVersions\":{\"FormBuilder\":[\">=\",0]},\"autoload\":true,\"created\":1560202644},\"166\":{\"name\":\"InputfieldFormBuilderFile\",\"title\":\"File (for FormBuilder)\",\"version\":2,\"requiresVersions\":{\"FormBuilder\":[\">=\",0]},\"created\":1560202672,\"namespace\":\"\\\\\"},\"165\":{\"name\":\"ProcessFormBuilder\",\"title\":\"Forms\",\"version\":32,\"icon\":\"building\",\"requiresVersions\":{\"FormBuilder\":[\">=\",0]},\"permission\":\"form-builder\",\"singular\":1,\"created\":1560202672,\"namespace\":\"\\\\\",\"useNavJSON\":true},\"168\":{\"name\":\"FrontEndEditLightbox\",\"title\":\"Front-End Edit Lightbox\",\"version\":\"1.4.0\",\"icon\":\"paper-plane\",\"autoload\":true,\"singular\":true,\"created\":1560211627,\"configurable\":3,\"namespace\":\"\\\\\"},\"167\":{\"name\":\"ImportPagesCSV\",\"title\":\"Import Pages from CSV\",\"version\":106,\"singular\":true,\"created\":1560203592,\"namespace\":\"\\\\\"},\"171\":{\"name\":\"Pages2Pdf\",\"title\":\"Pages2Pdf\",\"version\":117,\"installs\":[\"WirePDF\"],\"autoload\":true,\"singular\":true,\"configurable\":true,\"namespace\":\"\\\\\"},\"172\":{\"name\":\"WirePDF\",\"title\":\"WirePDF\",\"version\":104,\"configurable\":true,\"namespace\":\"\\\\\"}}', '2010-04-08 03:10:10'),
('FileCompiler__52c9965be29dd8c30385bb4124d53225', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/templates\\/bienvenida.php\",\"hash\":\"206542e957576f671fb21104b414cbac\",\"size\":2808,\"time\":1556805212,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/bienvenida.php\",\"hash\":\"206542e957576f671fb21104b414cbac\",\"size\":2808,\"time\":1556805212}}', '2010-04-08 03:10:10'),
('FileCompiler__7574cb88d20be9e9d74bece497f9457f', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/modules\\/Pages2Pdf-master\\/Pages2Pdf.module\",\"hash\":\"46fafa474058e6e7d6b6ba96bd163bb5\",\"size\":18600,\"time\":1557255302,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Pages2Pdf-master\\/Pages2Pdf.module\",\"hash\":\"bc16368eca2d12fecbb2bd0d96ca0311\",\"size\":19185,\"time\":1557255302}}', '2010-04-08 03:10:10'),
('FileCompiler__47478375a221b18486202f641f9464cd', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/modules\\/Pages2Pdf-master\\/WirePDF.module\",\"hash\":\"78b72749eed8d8016c24911a00eeaf2b\",\"size\":10559,\"time\":1557255302,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Pages2Pdf-master\\/WirePDF.module\",\"hash\":\"ad5fead8ea3802b5855c8537563acca1\",\"size\":11265,\"time\":1557255302}}', '2010-04-08 03:10:10'),
('FileCompiler__3bb3e4f4d6a04c7da94008367879c792', '{\"source\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/templates\\/reporte.php\",\"hash\":\"2beb3dd243ab0f3248a7648264257923\",\"size\":1355,\"time\":1560241446,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/Applications\\/XAMPP\\/xamppfiles\\/htdocs\\/pw-sysweb\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/reporte.php\",\"hash\":\"2beb3dd243ab0f3248a7648264257923\",\"size\":1355,\"time\":1560241446}}', '2010-04-08 03:10:10');

-- --------------------------------------------------------

--
-- Table structure for table `fieldgroups`
--

CREATE TABLE `fieldgroups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) CHARACTER SET ascii NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fieldgroups`
--

INSERT INTO `fieldgroups` (`id`, `name`) VALUES
(2, 'admin'),
(3, 'user'),
(4, 'role'),
(5, 'permission'),
(1, 'home'),
(88, 'sitemap'),
(83, 'basic-page'),
(80, 'search'),
(97, 'cliente'),
(98, 'localizacion'),
(99, 'bitacora'),
(100, 'form-builder'),
(101, 'cliente_listado'),
(102, 'cliente_nuevo'),
(103, 'cliente_localizacion'),
(104, 'cliente_modificar'),
(105, 'cliente_deshabilitar'),
(106, 'bienvenida'),
(107, 'reporte');

-- --------------------------------------------------------

--
-- Table structure for table `fieldgroups_fields`
--

CREATE TABLE `fieldgroups_fields` (
  `fieldgroups_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `fields_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `sort` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `data` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fieldgroups_fields`
--

INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES
(2, 1, 0, NULL),
(3, 4, 2, NULL),
(3, 92, 1, NULL),
(4, 5, 0, NULL),
(5, 1, 0, NULL),
(3, 97, 3, NULL),
(1, 76, 3, NULL),
(80, 1, 0, NULL),
(83, 76, 3, NULL),
(83, 79, 2, NULL),
(1, 79, 2, NULL),
(83, 44, 5, NULL),
(88, 1, 0, NULL),
(1, 78, 1, NULL),
(88, 79, 1, NULL),
(83, 82, 4, NULL),
(83, 78, 1, NULL),
(3, 3, 0, NULL),
(97, 105, 5, NULL),
(2, 2, 1, NULL),
(83, 1, 0, NULL),
(97, 98, 2, NULL),
(1, 44, 5, NULL),
(98, 1, 0, NULL),
(97, 102, 4, NULL),
(1, 82, 4, NULL),
(97, 101, 3, NULL),
(1, 1, 0, NULL),
(99, 103, 1, NULL),
(97, 99, 1, NULL),
(99, 104, 2, NULL),
(97, 106, 6, NULL),
(99, 1, 0, NULL),
(97, 1, 0, NULL),
(97, 107, 7, NULL),
(100, 1, 0, NULL),
(101, 1, 0, NULL),
(102, 1, 0, NULL),
(103, 1, 0, NULL),
(104, 1, 0, NULL),
(105, 1, 0, NULL),
(106, 1, 0, NULL),
(107, 1, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fields`
--

CREATE TABLE `fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(128) CHARACTER SET ascii NOT NULL,
  `name` varchar(250) CHARACTER SET ascii NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `label` varchar(250) NOT NULL DEFAULT '',
  `data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fields`
--

INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES
(1, 'FieldtypePageTitle', 'title', 13, 'Title', '{\"required\":1,\"textformatters\":[\"TextformatterEntities\"],\"size\":0,\"maxlength\":255}'),
(2, 'FieldtypeModule', 'process', 25, 'Process', '{\"description\":\"The process that is executed on this page. Since this is mostly used by ProcessWire internally, it is recommended that you don\'t change the value of this unless adding your own pages in the admin.\",\"collapsed\":1,\"required\":1,\"moduleTypes\":[\"Process\"],\"permanent\":1}'),
(3, 'FieldtypePassword', 'pass', 24, 'Set Password', '{\"collapsed\":1,\"size\":50,\"maxlength\":128}'),
(5, 'FieldtypePage', 'permissions', 24, 'Permissions', '{\"derefAsPage\":0,\"parent_id\":31,\"labelFieldName\":\"title\",\"inputfield\":\"InputfieldCheckboxes\"}'),
(4, 'FieldtypePage', 'roles', 24, 'Roles', '{\"derefAsPage\":0,\"parent_id\":30,\"labelFieldName\":\"name\",\"inputfield\":\"InputfieldCheckboxes\",\"description\":\"User will inherit the permissions assigned to each role. You may assign multiple roles to a user. When accessing a page, the user will only inherit permissions from the roles that are also assigned to the page\'s template.\"}'),
(92, 'FieldtypeEmail', 'email', 9, 'E-Mail Address', '{\"size\":70,\"maxlength\":255}'),
(82, 'FieldtypeTextarea', 'sidebar', 0, 'Sidebar', '{\"inputfieldClass\":\"InputfieldCKEditor\",\"rows\":5,\"contentType\":1,\"toolbar\":\"Format, Bold, Italic, -, RemoveFormat\\r\\nNumberedList, BulletedList, -, Blockquote\\r\\nPWLink, Unlink, Anchor\\r\\nPWImage, Table, HorizontalRule, SpecialChar\\r\\nPasteText, PasteFromWord\\r\\nScayt, -, Sourcedialog\",\"inlineMode\":0,\"useACF\":1,\"usePurifier\":1,\"formatTags\":\"p;h2;h3;h4;h5;h6;pre;address\",\"extraPlugins\":[\"pwimage\",\"pwlink\",\"sourcedialog\"],\"removePlugins\":\"image,magicline\",\"toggles\":[2,4,8],\"collapsed\":2}'),
(44, 'FieldtypeImage', 'images', 0, 'Images', '{\"extensions\":\"gif jpg jpeg png\",\"adminThumbs\":1,\"inputfieldClass\":\"InputfieldImage\",\"maxFiles\":0,\"descriptionRows\":1,\"fileSchema\":6,\"textformatters\":[\"TextformatterEntities\"],\"outputFormat\":1,\"defaultValuePage\":0,\"defaultGrid\":0,\"icon\":\"camera\"}'),
(79, 'FieldtypeTextarea', 'summary', 1, 'Summary', '{\"textformatters\":[\"TextformatterEntities\"],\"inputfieldClass\":\"InputfieldTextarea\",\"collapsed\":2,\"rows\":3,\"contentType\":0}'),
(76, 'FieldtypeTextarea', 'body', 0, 'Body', '{\"inputfieldClass\":\"InputfieldCKEditor\",\"rows\":10,\"contentType\":1,\"toolbar\":\"Format, Bold, Italic, -, RemoveFormat\\r\\nNumberedList, BulletedList, -, Blockquote\\r\\nPWLink, Unlink, Anchor\\r\\nPWImage, Table, HorizontalRule, SpecialChar\\r\\nPasteText, PasteFromWord\\r\\nScayt, -, Sourcedialog\",\"inlineMode\":0,\"useACF\":1,\"usePurifier\":1,\"formatTags\":\"p;h2;h3;h4;h5;h6;pre;address\",\"extraPlugins\":[\"pwimage\",\"pwlink\",\"sourcedialog\"],\"removePlugins\":\"image,magicline\",\"toggles\":[2,4,8]}'),
(78, 'FieldtypeText', 'headline', 0, 'Headline', '{\"description\":\"Use this instead of the Title if a longer headline is needed than what you want to appear in navigation.\",\"textformatters\":[\"TextformatterEntities\"],\"collapsed\":2,\"size\":0,\"maxlength\":1024}'),
(97, 'FieldtypeModule', 'admin_theme', 8, 'Admin Theme', '{\"moduleTypes\":[\"AdminTheme\"],\"labelField\":\"title\",\"inputfieldClass\":\"InputfieldRadios\"}'),
(98, 'FieldtypeText', 'cliente_nombre_completo', 0, 'Nombre Completo de Cliente', '{\"tags\":\"CLIENTE\",\"collapsed\":0,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0}'),
(99, 'FieldtypeOptions', 'cliente_estado', 0, 'Estado del cliente', '{\"inputfieldClass\":\"InputfieldRadios\",\"collapsed\":0,\"tags\":\"CLIENTE\",\"optionColumns\":0,\"required\":1,\"defaultValue\":1}'),
(100, 'FieldtypeText', 'cliente_empresa', 0, 'Nombre de la empresa', '{\"tags\":\"CLIENTE\",\"collapsed\":0,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0}'),
(101, 'FieldtypePage', 'cliente_location', 0, 'Ubicación', '{\"derefAsPage\":1,\"inputfield\":\"InputfieldSelect\",\"parent_id\":0,\"template_id\":44,\"labelFieldName\":\"title\",\"collapsed\":0,\"tags\":\"CLIENTE\"}'),
(102, 'FieldtypeText', 'cliente_location_detalle', 0, 'Descripción de ubicación', '{\"tags\":\"CLIENTE\",\"collapsed\":0,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0}'),
(103, 'FieldtypePage', 'cliente_bitacora', 0, 'Cliente', '{\"tags\":\"BITACORA\",\"derefAsPage\":1,\"inputfield\":\"InputfieldSelect\",\"parent_id\":0,\"template_id\":43,\"labelFieldName\":\"title\",\"collapsed\":0}'),
(104, 'FieldtypeTextarea', 'bitacora_comentario', 0, 'Descripción de registro', '{\"tags\":\"BITACORA\",\"inputfieldClass\":\"InputfieldTextarea\",\"contentType\":0,\"collapsed\":0,\"minlength\":0,\"maxlength\":0,\"showCount\":0,\"rows\":5}'),
(105, 'FieldtypeText', 'cliente_correo', 0, 'Correo', '{\"tags\":\"CLIENTE\",\"collapsed\":0,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0}'),
(106, 'FieldtypeText', 'cliente_telefono', 0, 'Celular', '{\"tags\":\"CLIENTE\",\"collapsed\":0,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0}'),
(107, 'FieldtypeText', 'cliente_sitio_web', 0, 'Cliente sitio web', '{\"tags\":\"CLIENTE\",\"collapsed\":0,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0}');

-- --------------------------------------------------------

--
-- Table structure for table `fieldtype_options`
--

CREATE TABLE `fieldtype_options` (
  `fields_id` int(10) UNSIGNED NOT NULL,
  `option_id` int(10) UNSIGNED NOT NULL,
  `title` text,
  `value` varchar(250) DEFAULT NULL,
  `sort` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fieldtype_options`
--

INSERT INTO `fieldtype_options` (`fields_id`, `option_id`, `title`, `value`, `sort`) VALUES
(99, 1, 'Activo', 'act', 0),
(99, 2, 'Inactivo', 'ina', 1);

-- --------------------------------------------------------

--
-- Table structure for table `field_admin_theme`
--

CREATE TABLE `field_admin_theme` (
  `pages_id` int(10) UNSIGNED NOT NULL,
  `data` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `field_bitacora_comentario`
--

CREATE TABLE `field_bitacora_comentario` (
  `pages_id` int(10) UNSIGNED NOT NULL,
  `data` mediumtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `field_body`
--

CREATE TABLE `field_body` (
  `pages_id` int(10) UNSIGNED NOT NULL,
  `data` mediumtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `field_body`
--

INSERT INTO `field_body` (`pages_id`, `data`) VALUES
(27, '<h3>The page you were looking for is not found.</h3><p>Please use our search engine or navigation above to find the page.</p>'),
(1, '<h2>What is ProcessWire?</h2>\n\n<p>ProcessWire gives you full control over your fields, templates and markup. It provides a powerful template system that works the way you do. Not to mention, ProcessWire\'s API makes working with your content easy and enjoyable. <a href=\"http://processwire.com\">Learn more</a></p>\n\n<h3>About this site profile</h3>\n\n<p>This is a basic minimal site for you to use in developing your own site or to learn from. There are a few pages here to serve as examples, but this site profile does not make any attempt to demonstrate all that ProcessWire can do. To learn more or ask questions, visit the <a href=\"http://www.processwire.com/talk/\" target=\"_blank\" rel=\"noreferrer noopener\">ProcessWire forums</a> or <a href=\"http://modules.processwire.com/categories/site-profile/\">browse more site profiles</a>. If you are building a new site, this minimal profile is a good place to start. You may use these existing templates and design as they are, or you may replace them entirely.</p>\n\n<h3>Browse the site</h3>'),
(1002, '<h2>Ut capio feugiat saepius torqueo olim</h2>\r\n\r\n<h3>In utinam facilisi eum vicis feugait nimis</h3>\r\n\r\n<p>Iusto incassum appellatio cui macto genitus vel. Lobortis aliquam luctus, roto enim, imputo wisi tamen. Ratis odio, genitus acsi, neo illum consequat consectetuer ut.</p>\r\n\r\n<blockquote>\r\n<p>Wisi fere virtus cogo, ex ut vel nullus similis vel iusto. Tation incassum adsum in, quibus capto premo diam suscipere facilisi. Uxor laoreet mos capio premo feugait ille et. Pecus abigo immitto epulae duis vel. Neque causa, indoles verto, decet ingenium dignissim.</p>\r\n</blockquote>\r\n\r\n<p>Patria iriure vel vel autem proprius indoles ille sit. Tation blandit refoveo, accumsan ut ulciscor lucidus inhibeo capto aptent opes, foras.</p>\r\n\r\n<h3>Dolore ea valde refero feugait utinam luctus</h3>\r\n\r\n<p><img alt=\"Copyright by Austin Cramer for DesignIntelligence. This is a placeholder while he makes new ones for us.\" class=\"align_left\"	src=\"/site/assets/files/1002/psych_cartoon_4-20.400x0.jpg\" />Usitas, nostrud transverbero, in, amet, nostrud ad. Ex feugiat opto diam os aliquam regula lobortis dolore ut ut quadrum. Esse eu quis nunc jugis iriure volutpat wisi, fere blandit inhibeo melior, hendrerit, saluto velit. Eu bene ideo dignissim delenit accumsan nunc. Usitas ille autem camur consequat typicus feugait elit ex accumsan nutus accumsan nimis pagus, occuro. Immitto populus, qui feugiat opto pneum letalis paratus. Mara conventio torqueo nibh caecus abigo sit eum brevitas. Populus, duis ex quae exerci hendrerit, si antehabeo nobis, consequat ea praemitto zelus.</p>\r\n\r\n<p>Immitto os ratis euismod conventio erat jus caecus sudo. code test Appellatio consequat, et ibidem ludus nulla dolor augue abdo tego euismod plaga lenis. Sit at nimis venio venio tego os et pecus enim pneum magna nobis ad pneum. Saepius turpis probo refero molior nonummy aliquam neque appellatio jus luctus acsi. Ulciscor refero pagus imputo eu refoveo valetudo duis dolore usitas. Consequat suscipere quod torqueo ratis ullamcorper, dolore lenis, letalis quia quadrum plaga minim.</p>'),
(1001, '<h2>Si lobortis singularis genitus ibidem saluto.</h2><p>Dolore ad nunc, mos accumsan paratus duis suscipit luptatum facilisis macto uxor iaceo quadrum. Demoveo, appellatio elit neque ad commodo ea. Wisi, iaceo, tincidunt at commoveo rusticus et, ludus. Feugait at blandit bene blandit suscipere abdo duis ideo bis commoveo pagus ex, velit. Consequat commodo roto accumsan, duis transverbero.</p>'),
(1004, '<h2>Pertineo vel dignissim, natu letalis fere odio</h2><p>Magna in gemino, gilvus iusto capto jugis abdo mos aptent acsi qui. Utrum inhibeo humo humo duis quae. Lucidus paulatim facilisi scisco quibus hendrerit conventio adsum.</p><h3>Si lobortis singularis genitus ibidem saluto</h3><ul><li>Feugiat eligo foras ex elit sed indoles hos elit ex antehabeo defui et nostrud.</li><li>Letatio valetudo multo consequat inhibeo ille dignissim pagus et in quadrum eum eu.</li><li>Aliquam si consequat, ut nulla amet et turpis exerci, adsum luctus ne decet, delenit.</li><li>Commoveo nunc diam valetudo cui, aptent commoveo at obruo uxor nulla aliquip augue.</li></ul><p>Iriure, ex velit, praesent vulpes delenit capio vero gilvus inhibeo letatio aliquip metuo qui eros. Transverbero demoveo euismod letatio torqueo melior. Ut odio in suscipit paulatim amet huic letalis suscipere eros causa, letalis magna.</p><ol><li>Feugiat eligo foras ex elit sed indoles hos elit ex antehabeo defui et nostrud.</li><li>Letatio valetudo multo consequat inhibeo ille dignissim pagus et in quadrum eum eu.</li><li>Aliquam si consequat, ut nulla amet et turpis exerci, adsum luctus ne decet, delenit.</li><li>Commoveo nunc diam valetudo cui, aptent commoveo at obruo uxor nulla aliquip augue.</li></ol>');

-- --------------------------------------------------------

--
-- Table structure for table `field_cliente_bitacora`
--

CREATE TABLE `field_cliente_bitacora` (
  `pages_id` int(10) UNSIGNED NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `field_cliente_correo`
--

CREATE TABLE `field_cliente_correo` (
  `pages_id` int(10) UNSIGNED NOT NULL,
  `data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `field_cliente_correo`
--

INSERT INTO `field_cliente_correo` (`pages_id`, `data`) VALUES
(1031, 'itisnado11@gmail.com'),
(1033, 'kmqfashions@kmqzl.com'),
(1034, 'gerencia.ventas@coinla.net'),
(1035, 'ventas@jumbozl.com'),
(1036, 'vertigo@vertigozl.com'),
(1037, 'ventas@intratex.com'),
(1038, 'melgars@grupocar12.com'),
(1039, 'gerencia@goldamerica.net'),
(1040, 'marcoxenia@hotmail.com'),
(1041, 'dvta99@163.com'),
(1042, 'ventas@mirapanamazl.com'),
(1043, 'monturas.gafas.nlp@gmail.com'),
(1044, 'ventas@panamafasteners.com'),
(1045, 'na@rimatex.net'),
(1046, 'starauto@cwpanama.net'),
(1047, 'tshirtmax@yahoo.com'),
(1048, 'ventas@uetainc.com'),
(1049, 'ventas@ajinternational.com.pa'),
(1050, 'marketing@studmark.com'),
(1051, 'yalal@afisa.com'),
(1052, 'ventas@agromarauto.com.pa'),
(1053, 'cconti@alcontrading.com'),
(1054, 'ventas@alphatrading.com'),
(1055, 'ventas@amazonz.com'),
(1056, 'info@americazl.com'),
(1057, 'astudios@tcarrier.net'),
(1058, 'adctex888@hotmail.com'),
(1059, 'ventas@anacasti.com'),
(1060, 'argelia@argeliazl.com'),
(1061, 'info@argyros.com.pa'),
(1062, 'panama@aristgom.com'),
(1063, 'ventas@aryanzl.com'),
(1064, 'asatex_sa@yahoo.com'),
(1065, 'usuarios@auzonalibrecolon.com'),
(1066, 'info@aamundial.com'),
(1067, 'ventas@acpanama.com'),
(1068, 'zlibre@autoimportpty.com'),
(1069, 'ventas@casajaponpanama.com'),
(1070, 'babytec@babyteczl.com'),
(1071, 'info@westonsport.net'),
(1072, 'colon@airpma.net'),
(1073, 'abisa-comercial@hotmail.com'),
(1074, 'maricela@actwellpanama.com'),
(1075, 'wsantos@aduanasantos.com'),
(1076, 'abadcda@cwpanama.com'),
(1077, 'info@aeromar-logistics.com'),
(1078, 'sales@vilcointl.com'),
(1079, 'was@wash-fz.com'),
(1080, 'ventas1@wl-la.com'),
(1081, 'winkoint@gmail.com'),
(1082, 'info@worldtargetint.com'),
(1083, 'ventas@worldtimeinc.net'),
(1084, 'infolatin@gmx.yamaha.com'),
(1085, 'yuhangintl@hotmail.com'),
(1086, 'zeneosazl@gmail.com'),
(1087, 'zmpanama2014@gmail.com'),
(1088, 'info@ziviczl.com'),
(1089, 'info@ajpanamacorp.com'),
(1090, 'ventas@vestirama.com'),
(1091, 'ventas@vidapanama.com'),
(1092, 'ventas@unionzl.net'),
(1093, 'ventas@avaunison.com'),
(1094, 'uyustools@cwpanama.net'),
(1095, 'jsjogreen@varelahermanos.com'),
(1096, 'venecia@cwpanama.net'),
(1097, 'takeroll4@hotmail.com'),
(1098, 'ventas@tamilatin.com'),
(1099, 'tancorp@cwpanama.net'),
(1100, 'tango@tangoint.com'),
(1101, 'tecnolamamerica@cwpanama.net'),
(1102, 'tessone@tessone.com'),
(1103, 'gerencia@textelapanama.com'),
(1104, 'ventas@texaszl.com'),
(1105, 'panama.quick@qti-usa.com'),
(1106, 'sanjeev@timezoneinc.com'),
(1107, 'topshoes100@yahoo.com.ar'),
(1108, 'panama@torotrac.com'),
(1109, 'tshirtmax@yahoo.com'),
(1110, 'ventas@sylinternationalcorp.com'),
(1111, 'computo@supremezl.com'),
(1112, 'aclientes2@supropanama.com'),
(1113, 'surtijoyaszl@hotmail.com'),
(1114, 'sensen@cwpanama.net'),
(1115, 'srtex.cp@126.com'),
(1116, 'sinkatex@sinkatex.com'),
(1117, 'sirena@sirenaelectronics.com'),
(1118, 'sirena@sirenazl.com'),
(1119, 'ventas@solardiamond.com'),
(1120, 'compras@solvijaya.com'),
(1121, 'sonneti@cwpanama.net'),
(1122, 'speedzonalibre@gmail.com'),
(1123, 'splashpma@hotmail.com'),
(1124, 'ventas@sportgalleryzl.com'),
(1125, 'starco@starcozl.com'),
(1126, 'tinko@tinko.com.pa'),
(1127, 'patricialiu62@gmail.com'),
(1128, 'sansimsa@gmail.com'),
(1129, 'sunglasses@seaandsunstore.com'),
(1130, 'selinternational1@gmail.com'),
(1131, 'rocket@cwpanama.net'),
(1132, 'ventas@rodeodepot.net'),
(1133, 'ventas@rodeoimport.com'),
(1134, 'rosen@rosenzonalibre.com'),
(1135, 'runnerathletic@gmail.com'),
(1136, 'sthonore@sthonore.com.pa'),
(1137, 'ventas@pickens.com.pa'),
(1138, 'premier@premiermundo.com'),
(1139, 'ventas@prestigefashion.com'),
(1140, 'ventas@primavera.com.pa'),
(1141, 'ventas@puntomodazl.com'),
(1142, 'premium@tcarrier.net'),
(1143, 'info@rada.com.pa'),
(1144, 'rcnventas@cwpanama.net'),
(1145, 'rafkas@rafkas.net'),
(1146, 'admin@raquelcosmetics.com'),
(1147, 'ray_panama@yahoo.com'),
(1148, 'raycozl@cwpanama.net'),
(1149, 'ricardof@cwpanama.net'),
(1150, 'monalisazl@hotmail.com'),
(1151, 'info@piazza.com.pa'),
(1152, 'picasso@cwpanama.net'),
(1153, 'info@perfectpty.com'),
(1154, 'bhills3060@hotmail.com'),
(1155, 'elcoronelzl@cwpanama.net'),
(1156, 'ventas@perfumeriamiracle.com'),
(1157, 'mail@novazl.com'),
(1158, 'ventas@ofimak.com'),
(1159, 'info@onlybrandsonline.com'),
(1160, 'ventas@panafoto.com'),
(1161, 'panasur@cwpanama.net'),
(1162, 'mundi@paranaint.com'),
(1163, 'fwaked@pariszl.com'),
(1164, 'info@361latino.com'),
(1165, 'ventas@nipponamericapma.com'),
(1166, 'ventas@noritex.com'),
(1167, 'north@tcarrier.net'),
(1168, 'rony@nostrumzl.com'),
(1169, 'info@bambary.com'),
(1170, 'newstyle@newstylezl.com'),
(1171, 'ventas@nikomotozl.com'),
(1172, 'ventas@landos.com.pa'),
(1173, 'pa@laparkan.com'),
(1174, 'infopty@learlogistics.net'),
(1175, 'info@eiffpma.com'),
(1176, 'info@expressdistributors.com'),
(1177, 'ventas@fcipty.com'),
(1178, 'filssa@filssa.com'),
(1179, 'fletecon@fletecon.com'),
(1180, 'ventas@forwardinglogistic.com'),
(1181, 'giofepa@cwpanama.net'),
(1182, 'ventas@generalcargo.net'),
(1183, 'ventas@geraenterprise.com'),
(1184, 'irma@impgs.com'),
(1185, 'jc@cainco.com'),
(1186, 'klcpanama1@gmail.com'),
(1187, 'operaciones@klgimport.com'),
(1188, 'aduanayservicios@hotmail.com'),
(1190, 'operaciones@dormarpanama.com'),
(1191, 'dualtec@dualtecgroup.com'),
(1192, 'conytran@cwpanama.net'),
(1193, 'servicioalcliente@aldepositospanama.com'),
(1194, 'info@arturoarauz.com'),
(1197, 'info@blulogistics.com.pa'),
(1198, 'bmpricing@hotmail.com'),
(1199, 'cargasintegradaspanama@gmail.com'),
(1200, 'sales@cfscargo.com'),
(1201, 'cargoxpresslogistic@gmail.com'),
(1202, 'info@comandotrucks.com'),
(1203, 'contactos@connexion-pa.com'),
(1204, 'air@aircargatransp.com'),
(1205, 'info@asw-colonfreezone.com.pa');

-- --------------------------------------------------------

--
-- Table structure for table `field_cliente_empresa`
--

CREATE TABLE `field_cliente_empresa` (
  `pages_id` int(10) UNSIGNED NOT NULL,
  `data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `field_cliente_estado`
--

CREATE TABLE `field_cliente_estado` (
  `pages_id` int(10) UNSIGNED NOT NULL,
  `data` int(10) UNSIGNED NOT NULL,
  `sort` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `field_cliente_estado`
--

INSERT INTO `field_cliente_estado` (`pages_id`, `data`, `sort`) VALUES
(1031, 1, 0),
(1181, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `field_cliente_location`
--

CREATE TABLE `field_cliente_location` (
  `pages_id` int(10) UNSIGNED NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `field_cliente_location`
--

INSERT INTO `field_cliente_location` (`pages_id`, `data`, `sort`) VALUES
(1031, 1015, 0),
(1033, 1015, 0),
(1043, 1015, 0),
(1053, 1015, 0),
(1063, 1015, 0),
(1073, 1015, 0),
(1083, 1015, 0),
(1092, 1015, 0),
(1102, 1015, 0),
(1112, 1015, 0),
(1123, 1015, 0),
(1133, 1015, 0),
(1143, 1015, 0),
(1196, 1015, 0),
(1197, 1015, 0),
(1198, 1015, 0),
(1199, 1015, 0),
(1200, 1015, 0),
(1201, 1015, 0),
(1202, 1015, 0),
(1203, 1015, 0),
(1204, 1015, 0),
(1205, 1015, 0),
(1034, 1016, 0),
(1044, 1016, 0),
(1054, 1016, 0),
(1064, 1016, 0),
(1074, 1016, 0),
(1084, 1016, 0),
(1093, 1016, 0),
(1103, 1016, 0),
(1113, 1016, 0),
(1124, 1016, 0),
(1134, 1016, 0),
(1144, 1016, 0),
(1035, 1017, 0),
(1045, 1017, 0),
(1055, 1017, 0),
(1065, 1017, 0),
(1075, 1017, 0),
(1085, 1017, 0),
(1094, 1017, 0),
(1104, 1017, 0),
(1114, 1017, 0),
(1125, 1017, 0),
(1135, 1017, 0),
(1145, 1017, 0),
(1036, 1018, 0),
(1046, 1018, 0),
(1056, 1018, 0),
(1066, 1018, 0),
(1076, 1018, 0),
(1086, 1018, 0),
(1095, 1018, 0),
(1105, 1018, 0),
(1115, 1018, 0),
(1126, 1018, 0),
(1136, 1018, 0),
(1146, 1018, 0),
(1037, 1019, 0),
(1047, 1019, 0),
(1057, 1019, 0),
(1067, 1019, 0),
(1077, 1019, 0),
(1087, 1019, 0),
(1096, 1019, 0),
(1106, 1019, 0),
(1116, 1019, 0),
(1127, 1019, 0),
(1137, 1019, 0),
(1147, 1019, 0),
(1038, 1020, 0),
(1048, 1020, 0),
(1058, 1020, 0),
(1068, 1020, 0),
(1078, 1020, 0),
(1088, 1020, 0),
(1097, 1020, 0),
(1107, 1020, 0),
(1117, 1020, 0),
(1128, 1020, 0),
(1138, 1020, 0),
(1148, 1020, 0),
(1039, 1021, 0),
(1049, 1021, 0),
(1059, 1021, 0),
(1069, 1021, 0),
(1079, 1021, 0),
(1089, 1021, 0),
(1098, 1021, 0),
(1108, 1021, 0),
(1118, 1021, 0),
(1129, 1021, 0),
(1139, 1021, 0),
(1149, 1021, 0),
(1157, 1021, 0),
(1158, 1021, 0),
(1159, 1021, 0),
(1160, 1021, 0),
(1161, 1021, 0),
(1162, 1021, 0),
(1163, 1021, 0),
(1164, 1021, 0),
(1165, 1021, 0),
(1166, 1021, 0),
(1167, 1021, 0),
(1168, 1021, 0),
(1169, 1021, 0),
(1170, 1021, 0),
(1171, 1021, 0),
(1172, 1021, 0),
(1173, 1021, 0),
(1174, 1021, 0),
(1175, 1021, 0),
(1176, 1021, 0),
(1177, 1021, 0),
(1178, 1021, 0),
(1179, 1021, 0),
(1180, 1021, 0),
(1181, 1021, 0),
(1182, 1021, 0),
(1183, 1021, 0),
(1184, 1021, 0),
(1185, 1021, 0),
(1186, 1021, 0),
(1187, 1021, 0),
(1188, 1021, 0),
(1189, 1021, 0),
(1190, 1021, 0),
(1191, 1021, 0),
(1192, 1021, 0),
(1193, 1021, 0),
(1194, 1021, 0),
(1195, 1021, 0),
(1040, 1022, 0),
(1050, 1022, 0),
(1060, 1022, 0),
(1070, 1022, 0),
(1080, 1022, 0),
(1090, 1022, 0),
(1099, 1022, 0),
(1109, 1022, 0),
(1119, 1022, 0),
(1130, 1022, 0),
(1140, 1022, 0),
(1150, 1022, 0),
(1041, 1023, 0),
(1051, 1023, 0),
(1061, 1023, 0),
(1071, 1023, 0),
(1081, 1023, 0),
(1091, 1023, 0),
(1100, 1023, 0),
(1110, 1023, 0),
(1120, 1023, 0),
(1131, 1023, 0),
(1141, 1023, 0),
(1151, 1023, 0),
(1042, 1024, 0),
(1052, 1024, 0),
(1062, 1024, 0),
(1072, 1024, 0),
(1082, 1024, 0),
(1101, 1024, 0),
(1111, 1024, 0),
(1121, 1024, 0),
(1122, 1024, 0),
(1132, 1024, 0),
(1142, 1024, 0),
(1152, 1024, 0),
(1153, 1024, 0),
(1154, 1024, 0),
(1155, 1024, 0),
(1156, 1024, 0);

-- --------------------------------------------------------

--
-- Table structure for table `field_cliente_location_detalle`
--

CREATE TABLE `field_cliente_location_detalle` (
  `pages_id` int(10) UNSIGNED NOT NULL,
  `data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `field_cliente_location_detalle`
--

INSERT INTO `field_cliente_location_detalle` (`pages_id`, `data`) VALUES
(1031, 'Istiweb, 12 de octubre al frente del Comercial Panamá');

-- --------------------------------------------------------

--
-- Table structure for table `field_cliente_nombre_completo`
--

CREATE TABLE `field_cliente_nombre_completo` (
  `pages_id` int(10) UNSIGNED NOT NULL,
  `data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `field_cliente_nombre_completo`
--

INSERT INTO `field_cliente_nombre_completo` (`pages_id`, `data`) VALUES
(1031, 'Isaac Giancarlo Tisnado'),
(1033, 'Aaron, Hank'),
(1034, 'Abagnale, Frank'),
(1035, 'Abbey, Edward'),
(1036, 'Abel, Reuben'),
(1037, 'Abelson, Hal'),
(1038, 'Abourezk, James'),
(1039, 'Abrams, Creighton'),
(1040, 'Ace, Jane'),
(1041, 'Acton, John (Lord Acton)'),
(1042, 'Adams, Abigail'),
(1043, 'Adams, Douglas'),
(1044, 'Adams, Henry'),
(1045, 'Adams, John'),
(1046, 'Adams, John Quincy'),
(1047, 'Adams, Samuel'),
(1048, 'Adams, Scott'),
(1049, 'Addams, Jane'),
(1050, 'Addison, Joseph'),
(1051, 'Adorno, Theodor'),
(1052, 'Adler, Alfred'),
(1053, 'Aeschylus'),
(1054, 'Aesop'),
(1055, 'Affleck, Ben'),
(1056, 'Agena, Keiko'),
(1057, 'Agnew, Spiro'),
(1058, 'Ahbez, Eden'),
(1059, 'Ahern, Bertie'),
(1060, 'Ah Koy, James'),
(1061, 'Ahmad'),
(1062, 'Aiken, Clay'),
(1063, 'Aiken, Conrad'),
(1064, 'Akinola, Peter Jasper (Archbishop)'),
(1065, 'Adriana C. Ocampo Uria'),
(1066, 'Albert Einstein'),
(1067, 'Anna K. Behrensmeyer'),
(1068, 'Blaise Pascal'),
(1069, 'Caroline Herschel'),
(1070, 'Cecilia Payne-Gaposchkin'),
(1071, 'Chien-Shiung Wu'),
(1072, 'Dorothy Hodgkin'),
(1073, 'Edmond Halley'),
(1074, 'Edwin Powell Hubble'),
(1075, 'Elizabeth Blackburn'),
(1076, 'Enrico Fermi'),
(1077, 'Erwin Schroedinger'),
(1078, 'Flossie Wong-Staal'),
(1079, 'Frieda Robscheit-Robbins'),
(1080, 'Geraldine Seydoux'),
(1081, 'Gertrude B. Elion'),
(1082, 'Ingrid Daubechies'),
(1083, 'Jacqueline K. Barton'),
(1084, 'Jane Goodall'),
(1085, 'Jocelyn Bell Burnell'),
(1086, 'Johannes Kepler'),
(1087, 'Lene Vestergaard Hau'),
(1088, 'Lise Meitner'),
(1089, 'Lord Kelvin'),
(1090, 'Maria Mitchell'),
(1091, 'Marie Curie'),
(1092, 'Max Planck'),
(1093, 'Melissa Franklin'),
(1094, 'Michael Faraday'),
(1095, 'Mildred S. Dresselhaus'),
(1096, 'Nicolaus Copernicus'),
(1097, 'Niels Bohr'),
(1098, 'Patricia S. Goldman-Rakic'),
(1099, 'Patty Jo Watson'),
(1100, 'Polly Matzinger'),
(1101, 'Richard Phillips Feynman'),
(1102, 'Rita Levi-Montalcini'),
(1103, 'Rosalind Franklin'),
(1104, 'Ruzena Bajcsy'),
(1105, 'Sarah Boysen'),
(1106, 'Shannon W. Lucid'),
(1107, 'Shirley Ann Jackson'),
(1108, 'Sir Ernest Rutherford'),
(1109, 'Sir Isaac Newton'),
(1110, 'Stephen Hawking'),
(1111, 'Werner Karl Heisenberg'),
(1112, 'Wilhelm Conrad Roentgen'),
(1113, 'Wolfgang Ernst Pauli'),
(1114, 'Adriana C. Ocampo Uria'),
(1115, 'Albert Einstein'),
(1116, 'Anna K. Behrensmeyer'),
(1117, 'Blaise Pascal'),
(1118, 'Caroline Herschel'),
(1119, 'Cecilia Payne-Gaposchkin'),
(1120, 'Chien-Shiung Wu'),
(1121, 'Dorothy Hodgkin'),
(1122, 'Edmond Halley'),
(1123, 'Edwin Powell Hubble'),
(1124, 'Elizabeth Blackburn'),
(1125, 'Enrico Fermi'),
(1126, 'Erwin Schroedinger'),
(1127, 'Flossie Wong-Staal'),
(1128, 'Frieda Robscheit-Robbins'),
(1129, 'Geraldine Seydoux'),
(1130, 'Gertrude B. Elion'),
(1131, 'Ingrid Daubechies'),
(1132, 'Jacqueline K. Barton'),
(1133, 'Jane Goodall'),
(1134, 'Jocelyn Bell Burnell'),
(1135, 'Johannes Kepler'),
(1136, 'Lene Vestergaard Hau'),
(1137, 'Lise Meitner'),
(1138, 'Lord Kelvin'),
(1139, 'Maria Mitchell'),
(1140, 'Marie Curie'),
(1141, 'Max Born'),
(1142, 'Max Planck'),
(1143, 'Melissa Franklin'),
(1144, 'Michael Faraday'),
(1145, 'Mildred S. Dresselhaus'),
(1146, 'Nicolaus Copernicus'),
(1147, 'Niels Bohr'),
(1148, 'Patricia S. Goldman-Rakic'),
(1149, 'Patty Jo Watson'),
(1150, 'Polly Matzinger'),
(1151, 'Richard Phillips Feynman'),
(1152, 'Rita Levi-Montalcini'),
(1153, 'Rosalind Franklin'),
(1154, 'Ruzena Bajcsy'),
(1155, 'Sarah Boysen'),
(1156, 'Shannon W. Lucid'),
(1157, 'Shirley Ann Jackson'),
(1158, 'Sir Ernest Rutherford'),
(1159, 'Sir Isaac Newton'),
(1160, 'Stephen Hawking'),
(1161, 'Wilhelm Conrad Roentgen'),
(1162, 'Wolfgang Ernst Pauli'),
(1163, 'Adriana C. Ocampo Uria'),
(1164, 'Albert Einstein'),
(1165, 'Anna K. Behrensmeyer'),
(1166, 'Blaise Pascal'),
(1167, 'Caroline Herschel'),
(1168, 'Cecilia Payne-Gaposchkin'),
(1169, 'Chien-Shiung Wu'),
(1170, 'Dorothy Hodgkin'),
(1171, 'Edmond Halley'),
(1172, 'Edwin Powell Hubble'),
(1173, 'Elizabeth Blackburn'),
(1174, 'Enrico Fermi'),
(1175, 'Erwin Schroedinger'),
(1176, 'Flossie Wong-Staal'),
(1177, 'Frieda Robscheit-Robbins'),
(1178, 'Geraldine Seydoux'),
(1179, 'Gertrude B. Elion'),
(1180, 'Ingrid Daubechies'),
(1181, 'Jacqueline K. Barton'),
(1182, 'Jane Goodall'),
(1183, 'Jocelyn Bell Burnell'),
(1184, 'Johannes Kepler'),
(1185, 'Lene Vestergaard Hau'),
(1186, 'Lise Meitner'),
(1187, 'Lord Kelvin'),
(1188, 'Maria Mitchell'),
(1189, 'Marie Curie'),
(1190, 'Max Born'),
(1191, 'Max Planck'),
(1192, 'Melissa Franklin'),
(1193, 'Michael Faraday'),
(1194, 'Mildred S. Dresselhaus'),
(1195, 'Nicolaus Copernicus'),
(1196, 'Niels Bohr'),
(1197, 'Patricia S. Goldman-Rakic'),
(1198, 'Patty Jo Watson'),
(1199, 'Polly Matzinger'),
(1200, 'Richard Phillips Feynman'),
(1201, 'Rita Levi-Montalcini'),
(1202, 'Rosalind Franklin'),
(1203, 'Ruzena Bajcsy'),
(1204, 'Sarah Boysen'),
(1205, 'Shannon W. Lucid');

-- --------------------------------------------------------

--
-- Table structure for table `field_cliente_sitio_web`
--

CREATE TABLE `field_cliente_sitio_web` (
  `pages_id` int(10) UNSIGNED NOT NULL,
  `data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `field_cliente_sitio_web`
--

INSERT INTO `field_cliente_sitio_web` (`pages_id`, `data`) VALUES
(1031, 'www.istiweb.com'),
(1033, 'http://www.kmqzl.com'),
(1034, 'http://panaparkfreezone.com/'),
(1036, 'http://www.vertigozl.com/'),
(1037, 'http://intratex.com'),
(1039, 'http://www.goldamerica.net'),
(1043, 'http://www.monturasygafasnlp.com'),
(1046, 'http://www.starautopa.com'),
(1047, 'http://www.unicrese.net'),
(1048, 'http://www.uetainc.com'),
(1049, 'http://www.ajinternational.com.pa'),
(1050, 'http://www.studmark.com'),
(1051, 'http://www.afisa.com'),
(1052, 'http://www.agromarauto.com.pa'),
(1053, 'http://www.alcontrading.com'),
(1054, 'http://www.alphatrading.com'),
(1056, 'http://www.americazl.com'),
(1057, 'http://www.americanstudios.com.pa'),
(1059, 'http://www.anacasti.com'),
(1060, 'http://www.argeliazl.com'),
(1061, 'http://www.argyros.com.pa'),
(1063, 'http://www.aryanzl.com'),
(1065, 'http://www.auzonalibrecolon.com'),
(1066, 'http://www.aamundial.com'),
(1067, 'http://www.acpanama.com'),
(1069, 'http://www.casajaponpanama.com'),
(1071, 'http://www.rodeosportzl.com'),
(1072, 'http://www.airpma.net'),
(1074, 'http://www.actwellpanama.com'),
(1075, 'http://www.aduanasantos.com'),
(1077, 'http://www.aeromar-logistic.com'),
(1078, 'http://www.vilcointl.com'),
(1079, 'http://www.wash-fz.com'),
(1080, 'http://www.wl-la.com'),
(1082, 'http://www.worldtargetint.com'),
(1083, 'http://www.worldtimeinc.net'),
(1087, 'http://www.zmpanama.com'),
(1088, 'http://www.zivicfz.com'),
(1089, 'http://www.ajpanamacorp.com'),
(1090, 'http://www.vestirama.com'),
(1091, 'http://www.vidapanama.com'),
(1093, 'http://www.avaunison.com'),
(1094, 'http://www.uyusa.com'),
(1095, 'http://www.varelahermanos.com'),
(1096, 'http://www.veneciazonalibre.com'),
(1101, 'http://www.tecnolamla.com'),
(1102, 'http://www.tessone.com%20%20%20%20'),
(1108, 'http://www.torotrac.com'),
(1109, 'http://www.unicrese.com'),
(1110, 'http://www.sylpanama.com'),
(1117, 'http://www.sirenaelectronics.com%20'),
(1118, 'http://www.sirenazl.com'),
(1120, 'http://www.solvijaya.com'),
(1125, 'http://www.starcozl.com'),
(1128, 'http://www.sansimsa.com'),
(1130, 'http://www.selinternationalsa.com'),
(1133, 'http://www.rodeoimport.com'),
(1134, 'http://www.rosenzonalibre.com'),
(1135, 'http://www.runnerathletic.com'),
(1136, 'http://www.sthonore.com.pa'),
(1137, 'http://www.pickensexport.com'),
(1138, 'http://www.premiermundo.com'),
(1140, 'http://www.primavera.com.pa'),
(1144, 'http://www.rcnint.com'),
(1146, 'http://www.raquelcosmetics.com'),
(1153, 'http://www.repuestosperfect.com'),
(1157, 'http://www.novazl.com'),
(1158, 'http://www.ofimak.com'),
(1160, 'http://www.panafoto.com'),
(1162, 'http://www.paranaint.com'),
(1165, 'http://www.nipponamerica.com'),
(1166, 'http://www.noritex.com%20'),
(1169, 'http://www.bambary.com'),
(1171, 'http://www.nikomotozl.com'),
(1173, 'http://www.laparkan.com'),
(1174, 'http://www.lcepty.com'),
(1175, 'http://www.eternityintlgroup.com'),
(1176, 'http://www.expresdistributors.com'),
(1177, 'http://www.fcipty.com'),
(1178, 'http://www.filssa.com'),
(1179, 'http://www.fletecon.com'),
(1180, 'http://www.forwardinglogistic.com'),
(1181, 'http://www.giofepa.com'),
(1183, 'http://www.geraenterprise.com'),
(1185, 'http://www.cainco.com%20.'),
(1186, 'http://www.klclogistics.com.pa'),
(1187, 'http://www.klgimport.com%20'),
(1191, 'http://www.dualteccargopanama.com%20.'),
(1193, 'http://aldepositospanama.com/'),
(1194, 'http://www.arturoarauz.com'),
(1195, 'http://www.bancoaliado.com'),
(1197, 'http://www.blulogistics.com.pa'),
(1200, 'http://www.cfscargo.com'),
(1202, 'http://www.comandotrucks.com'),
(1203, 'http://www.connexionlogistics.com');

-- --------------------------------------------------------

--
-- Table structure for table `field_cliente_telefono`
--

CREATE TABLE `field_cliente_telefono` (
  `pages_id` int(10) UNSIGNED NOT NULL,
  `data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `field_cliente_telefono`
--

INSERT INTO `field_cliente_telefono` (`pages_id`, `data`) VALUES
(1031, '68610691'),
(1033, '+507 447-2122'),
(1034, '+507 263-3697'),
(1035, '+507 446-5000'),
(1036, '+507 446-3150'),
(1037, '+507 446-4444'),
(1038, '+507 430-3491'),
(1039, '+507 441-4366'),
(1040, '+507 474-1388'),
(1042, '+507 441-4241'),
(1043, '+507 474-0727'),
(1044, '+507 393-9612'),
(1045, '+507 441-7000'),
(1046, '+507 449-0625'),
(1047, '+507 474-3388'),
(1048, '-7329'),
(1049, '+507 430-3931'),
(1050, '+507 447-2837'),
(1051, '+507 441-6202'),
(1052, '+507 474-1292'),
(1053, '+507 474-7010'),
(1054, '+507 446-3872'),
(1055, '+507 441-1035'),
(1056, '+507 433-2880'),
(1057, '+507 431-1305'),
(1058, '+507 445-2468'),
(1059, '+507 441-4704'),
(1060, '+507 431-2700'),
(1061, '+507 447-3175'),
(1062, '+507 447-3005'),
(1063, '+507 474-5505'),
(1064, '+507 441-5478'),
(1065, '+507 441-4244'),
(1066, '+507 433-3851'),
(1067, '+507 441-5150'),
(1068, '+507 441-5554'),
(1069, '+507 430-3635'),
(1070, '+507 446-3235'),
(1071, '(507) 441-2242'),
(1072, '(507) 441-6920'),
(1073, '(507) 431-0084'),
(1074, '(507) 439-8272'),
(1075, '(507) 260-6377'),
(1076, '(507) 279-0091'),
(1077, '474-8246'),
(1078, '(507) 441-7198'),
(1079, '(507) 446-7888'),
(1080, '(507) 430-1737'),
(1081, '(507) 441-3862'),
(1082, '(507) 446-3099'),
(1083, '(507) 441-2372'),
(1084, '(507) 269-5311'),
(1085, '(507) 433-3500'),
(1086, '(507) 474-9528'),
(1087, '(507) 433-2838'),
(1088, '(507) 441-1001'),
(1089, '(507) 474-2686'),
(1090, '(507) 441-2100'),
(1091, '(507) 433-9000'),
(1092, '(507) 441-8487'),
(1093, '(507) 431-1177'),
(1094, '(507) 441-0519'),
(1095, '(507) 474-5610'),
(1096, '(507) 441-0524'),
(1097, '(507) 447-3528'),
(1098, '(507) 430-4473'),
(1099, '(507) 445-3673'),
(1100, '(507) 446-4600'),
(1101, '(507) 430-4005'),
(1102, '(507) 441-2063'),
(1103, '(507) 441-5000'),
(1104, '(507) 447-0154'),
(1105, '(507) 433-0211'),
(1106, '(507) 441-0959'),
(1107, '(507) 441-1162'),
(1108, '(507) 233-6080'),
(1109, '(507) 474-3388'),
(1110, '(507) 261-5215'),
(1111, '(507) 441-4474'),
(1112, '(507) 300-5630'),
(1113, '(507) 474-7630'),
(1114, '(507) 430-6413'),
(1115, '(507) 433-1212'),
(1116, '(507) 431-0510'),
(1117, '(507) 4742211'),
(1118, '(507) 441-5185'),
(1119, '(507) 433-0156'),
(1120, '(507) 441-9299'),
(1121, '(507) 447-2511'),
(1122, '(507) 474-4232'),
(1123, '(507) 441-8111'),
(1124, '(507) 447-3904'),
(1125, '(507) 441-7866'),
(1126, '(507) 431-1112'),
(1127, '(507) 431-0397'),
(1128, '(507) 447-0793'),
(1129, '(507) 445-0000'),
(1130, '(507) 449-0287'),
(1131, '(507) 441-0423'),
(1132, '(507) 441-9090'),
(1133, '(507) 441-8710'),
(1134, '(507) 445-3000'),
(1135, '(507) 474-3333'),
(1136, '(507) 446-3350'),
(1137, '(507) 473-0937'),
(1138, '(507) 439-1292'),
(1139, '(507) 397-8899'),
(1140, '(507) 433-3000'),
(1141, '(507) 447-0330'),
(1142, '(507) 431-0670'),
(1143, '(507) 474-2737'),
(1144, '(507) 441-8404'),
(1145, '(507) 441-8444'),
(1146, '(507) 431-8088'),
(1147, '(507) 441-6058'),
(1148, '(507) 830-7011'),
(1149, '(507) 441-4971'),
(1150, 'Tel: (507) 446-664'),
(1151, '(507) 204-6111'),
(1152, '(507) 441-5850'),
(1153, '(507) 474-4202'),
(1154, '(507) 441-3060'),
(1155, '(507) 447-1719'),
(1156, '(507) 445-0901'),
(1157, '(507) 441-2000'),
(1158, '(507) 439-8115'),
(1159, '(507) 431-0538'),
(1160, '507) 433-1600'),
(1161, '(507) 445-4588'),
(1162, '(507) 441-2211'),
(1163, '(507) 441-0005'),
(1164, '(507) 441-1515'),
(1165, '(507) 439-5501'),
(1166, '(507) 300-3003'),
(1167, '(507) 441-7502'),
(1168, '(507) 431-0075'),
(1169, '+507 302-5620'),
(1170, '(507) 445-4198'),
(1171, '(507) 441-3302'),
(1172, '(507) 441-4137'),
(1173, '(507) 431-0760'),
(1174, '(507) 4470204'),
(1175, '(507) 430-3960'),
(1176, '(507) 431-0140'),
(1177, '(507) 430-9300'),
(1178, '(507) 430-3831'),
(1179, '(507) 390-9500'),
(1180, '(507) 430-1362'),
(1181, '(507) 444-1384'),
(1182, '(507) 430-0512'),
(1183, '(507) 431-1311'),
(1184, '(507) 474-2414'),
(1185, '(507) 431-0777'),
(1186, '(507) 431-1640'),
(1187, '(507) 474-2581'),
(1188, '(507) 260-2255'),
(1189, '(507) 441-2555'),
(1190, '(507) 431-0621'),
(1191, '(507) 269-8581'),
(1192, '(507) 441-0842'),
(1193, '(507) 430-2568'),
(1194, '(507) 264-2210'),
(1195, '(507) 441-4447'),
(1196, '(507) 441-2985'),
(1197, '(507) 261-0044'),
(1198, '(507) 449-0102'),
(1199, '(507) 433-2308'),
(1200, '(507) 236-0300'),
(1201, '(507) 394-4830'),
(1202, '(507) 230-4785'),
(1203, '(507) 841-9000'),
(1204, '(507) 441-4022'),
(1205, '(507) 269-8988');

-- --------------------------------------------------------

--
-- Table structure for table `field_email`
--

CREATE TABLE `field_email` (
  `pages_id` int(10) UNSIGNED NOT NULL,
  `data` varchar(250) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `field_email`
--

INSERT INTO `field_email` (`pages_id`, `data`) VALUES
(41, 'itisnado11@gmail.com'),
(1211, 'isaac@tisnado.com');

-- --------------------------------------------------------

--
-- Table structure for table `field_headline`
--

CREATE TABLE `field_headline` (
  `pages_id` int(10) UNSIGNED NOT NULL,
  `data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `field_headline`
--

INSERT INTO `field_headline` (`pages_id`, `data`) VALUES
(1, 'Minimal Site Profile'),
(1001, 'About Us'),
(27, '404 Page Not Found');

-- --------------------------------------------------------

--
-- Table structure for table `field_images`
--

CREATE TABLE `field_images` (
  `pages_id` int(10) UNSIGNED NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) UNSIGNED NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `field_images`
--

INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES
(1002, 'psych_cartoon_4-20.jpg', 0, 'Copyright by Austin Cramer for DesignIntelligence. This is a placeholder while he makes new ones for us.', '2019-06-10 23:05:41', '2019-06-10 23:05:41', NULL),
(1, 'rough_cartoon_puppet.jpg', 1, 'Copyright by Austin Cramer for DesignIntelligence. This is a placeholder while he makes new ones for us.', '2019-06-10 23:05:41', '2019-06-10 23:05:41', NULL),
(1, 'airport_cartoon_3.jpg', 0, 'Copyright by Austin Cramer for DesignIntelligence. This is a placeholder while he makes new ones for us.', '2019-06-10 23:05:41', '2019-06-10 23:05:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `field_pass`
--

CREATE TABLE `field_pass` (
  `pages_id` int(10) UNSIGNED NOT NULL,
  `data` char(40) NOT NULL,
  `salt` char(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=ascii;

--
-- Dumping data for table `field_pass`
--

INSERT INTO `field_pass` (`pages_id`, `data`, `salt`) VALUES
(41, 'zs1XNZpAqcEQ44fb3etLG33SU48c3PW', '$2y$11$Eiky7.OWXg9NvljAbc7n0e'),
(40, '', ''),
(1211, 'Nxkq60J3GKSSzw/W4lq1ngN.ltDe0Ku', '$2y$11$YitS0JR3WMgqAB6l3uGbNu');

-- --------------------------------------------------------

--
-- Table structure for table `field_permissions`
--

CREATE TABLE `field_permissions` (
  `pages_id` int(10) UNSIGNED NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `field_permissions`
--

INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES
(38, 32, 1),
(38, 34, 2),
(38, 35, 3),
(37, 36, 0),
(38, 36, 0),
(38, 50, 4),
(38, 51, 5),
(38, 52, 7),
(38, 53, 8),
(38, 54, 6);

-- --------------------------------------------------------

--
-- Table structure for table `field_process`
--

CREATE TABLE `field_process` (
  `pages_id` int(11) NOT NULL DEFAULT '0',
  `data` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `field_process`
--

INSERT INTO `field_process` (`pages_id`, `data`) VALUES
(6, 17),
(3, 12),
(8, 12),
(9, 14),
(10, 7),
(11, 47),
(16, 48),
(300, 104),
(21, 50),
(29, 66),
(23, 10),
(304, 138),
(31, 136),
(22, 76),
(30, 68),
(303, 129),
(2, 87),
(302, 121),
(301, 109),
(28, 76),
(1007, 150),
(1009, 158),
(1011, 160),
(1030, 165),
(1032, 167);

-- --------------------------------------------------------

--
-- Table structure for table `field_roles`
--

CREATE TABLE `field_roles` (
  `pages_id` int(10) UNSIGNED NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `field_roles`
--

INSERT INTO `field_roles` (`pages_id`, `data`, `sort`) VALUES
(40, 37, 0),
(41, 37, 0),
(1211, 37, 1),
(41, 38, 2),
(1211, 1209, 0);

-- --------------------------------------------------------

--
-- Table structure for table `field_sidebar`
--

CREATE TABLE `field_sidebar` (
  `pages_id` int(10) UNSIGNED NOT NULL,
  `data` mediumtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `field_sidebar`
--

INSERT INTO `field_sidebar` (`pages_id`, `data`) VALUES
(1, '<h3>About ProcessWire</h3>\n\n<p>ProcessWire is an open source CMS and web application framework aimed at the needs of designers, developers and their clients.</p>\n\n<ul><li><a href=\"http://processwire.com/talk/\">Support</a> </li>\n	<li><a href=\"http://processwire.com/docs/\">Documentation</a></li>\n	<li><a href=\"http://processwire.com/docs/tutorials/\">Tutorials</a></li>\n	<li><a href=\"http://cheatsheet.processwire.com\">API Cheatsheet</a></li>\n	<li><a href=\"http://modules.processwire.com\">Modules/Plugins</a></li>\n</ul>'),
(1002, '<h3>Sudo nullus</h3>\r\n\r\n<p>Et torqueo vulpes vereor luctus augue quod consectetuer antehabeo causa patria tation ex plaga ut. Abluo delenit wisi iriure eros feugiat probo nisl aliquip nisl, patria. Antehabeo esse camur nisl modo utinam. Sudo nullus ventosus ibidem facilisis saepius eum sino pneum, vicis odio voco opto.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `field_summary`
--

CREATE TABLE `field_summary` (
  `pages_id` int(10) UNSIGNED NOT NULL,
  `data` mediumtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `field_summary`
--

INSERT INTO `field_summary` (`pages_id`, `data`) VALUES
(1002, 'Dolore ea valde refero feugait utinam luctus. Probo velit commoveo et, delenit praesent, suscipit zelus, hendrerit zelus illum facilisi, regula. '),
(1001, 'This is a placeholder page with two child pages to serve as an example. '),
(1005, 'View this template\'s source for a demonstration of how to create a basic site map. '),
(1004, 'Mos erat reprobo in praesent, mara premo, obruo iustum pecus velit lobortis te sagaciter populus.'),
(1, 'ProcessWire is an open source CMS and web application framework aimed at the needs of designers, developers and their clients. ');

-- --------------------------------------------------------

--
-- Table structure for table `field_title`
--

CREATE TABLE `field_title` (
  `pages_id` int(10) UNSIGNED NOT NULL,
  `data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `field_title`
--

INSERT INTO `field_title` (`pages_id`, `data`) VALUES
(11, 'Templates'),
(16, 'Fields'),
(22, 'Setup'),
(3, 'Pages'),
(6, 'Add Page'),
(8, 'Tree'),
(9, 'Save Sort'),
(10, 'Edit'),
(21, 'Modules'),
(29, 'Users'),
(30, 'Roles'),
(2, 'Admin'),
(7, 'Trash'),
(27, '404 Page'),
(302, 'Insert Link'),
(23, 'Login'),
(304, 'Profile'),
(301, 'Empty Trash'),
(300, 'Search'),
(303, 'Insert Image'),
(28, 'Access'),
(31, 'Permissions'),
(32, 'Edit pages'),
(34, 'Delete pages'),
(35, 'Move pages (change parent)'),
(36, 'View pages'),
(50, 'Sort child pages'),
(51, 'Change templates on pages'),
(52, 'Administer users'),
(53, 'User can update profile/password'),
(54, 'Lock or unlock a page'),
(1, 'IT-Sysweb'),
(1001, 'About'),
(1002, 'Child page example 1'),
(1000, 'Search'),
(1004, 'Child page example 2'),
(1005, 'Site Map'),
(1006, 'Use Page Lister'),
(1007, 'Find'),
(1009, 'Recent'),
(1010, 'Can see recently edited pages'),
(1011, 'Logs'),
(1012, 'Can view system logs'),
(1013, 'Can manage system logs'),
(1014, 'Clientes por localización'),
(1015, 'Panamá'),
(1016, 'Panamá Oeste'),
(1017, 'Veraguas'),
(1018, 'Los Santos'),
(1019, 'Herrera'),
(1020, 'Darién'),
(1021, 'Chiriquí'),
(1022, 'Colón'),
(1023, 'Coclé'),
(1024, 'Bocas del Toro'),
(1025, 'Clientes'),
(1026, 'Bitácora'),
(1027, 'Form Builder'),
(1028, 'Access Form Builder admin page'),
(1029, 'Add new or import Form Builder forms'),
(1030, 'Forms'),
(1031, 'Isaac Giancarlo Tisnado'),
(1032, 'Import Pages From CSV'),
(1033, 'KMQ MODA, S.A. / KMQ OPTICOS, S.A.'),
(1034, 'PANAPARK FREE ZONE'),
(1035, 'JUMBO ZONA LIBRE, S.A.'),
(1036, 'VERTIGO ZONA LIBRE, S.A.'),
(1037, 'INTRATEX, S.A.'),
(1038, 'CARLINDO MONTACARGAS'),
(1039, 'GOLD AMERICA'),
(1040, 'MARCO XENIA, S.A.'),
(1041, 'DVTA INTERNATIONAL, S.A.'),
(1042, 'MIRA (PANAMA) INC.'),
(1043, 'MONTURAS Y GAFAS N.L.P., S.A.'),
(1044, 'PANAMA FASTENERS'),
(1045, 'RIMATEX, S.A.'),
(1046, 'STAR AUTO PARTS, S.A.'),
(1047, 'T-SHIRTS & CAP CO. (UNICRESE)'),
(1048, 'UETA LATINOAMERICA INC.'),
(1049, 'A J INTERNATIONAL GROUP, S.A.'),
(1050, 'ABOUGANEM Y CIA, S.A.'),
(1051, 'AFISA ZONA LIBRE'),
(1052, 'AGROMARAUTO INTERNATIONAL, S.A.'),
(1053, 'ALCON TRADING'),
(1054, 'ALPHA TRADING'),
(1055, 'AMAZON ZONA LIBRE'),
(1056, 'AMERICA ZONA LIBRE, S.A.'),
(1057, 'AMERICAN STUDIOS, S.A.'),
(1058, 'AMOR DE CASA TEXTILE, S.A.'),
(1059, 'ANACASTI INTERNACIONAL, S.A.'),
(1060, 'ARGELIA INTERNACIONAL, S.A.'),
(1061, 'ARGYROS INC.'),
(1062, 'ARISTGOM'),
(1063, 'ARYAN INTERNCIONAL ZONA LIBRE'),
(1064, 'ASATEX INT., S.A.'),
(1065, 'ASOCIACION DE USUARIOS DE LA ZONA LIBRE DE COLON'),
(1066, 'AUTO ACCESORIOS MUNDIALES, S.A.'),
(1067, 'AUTO CENTER ZONA LIBRE, S.A.'),
(1068, 'AUTO IMPORT INTERNACIONAL, S.A.'),
(1069, 'AUTOPIEZAS CASA JAPON, S.A.'),
(1070, 'BABYTEX, S.A.'),
(1071, 'RODEO SPORT'),
(1072, 'A.I.R. AJUSTADORES-SURVEYORS'),
(1073, 'ABISA INTERNACIONAL, S.A.'),
(1074, 'ACTWELL PANAMA, S.A.'),
(1075, 'ADUANAS SANTOS, S.A.'),
(1076, 'ADUANAS VISUETTE, DUTARY Y ABAD, S.A.'),
(1077, 'AERO-MAR LOGISTIC INTERNACIONAL, S.A.'),
(1078, 'VILCO INTERNACIONAL S.A.'),
(1079, 'WASHINGTON INTERNATIONAL S.A.'),
(1080, 'WESTINGHOUSE LIGHTING LATIN AMERICA'),
(1081, 'WINKO INT´L S.A'),
(1082, 'WORLD TARGET INTERNATIONAL, S.A.'),
(1083, 'WORLD TIME INC.'),
(1084, 'YAMAHA MUSIC LATIN AMERICA'),
(1085, 'YUHANG INTERNATIONAL, S.A.'),
(1086, 'ZENEO, S.A.'),
(1087, 'ZHONG-MA INTERNATIONAL INVESTMENT, S.A.'),
(1088, 'ZIVIC, S.A.'),
(1089, 'A J PANAMA CORPORATION'),
(1090, 'VESTIRAMA, S.A.'),
(1091, 'VIDA PANAMA'),
(1092, 'UNION ZONA LIBRE, S.A.'),
(1093, 'UNISON INTERNATIONAL, S.A. (AVA)'),
(1094, 'UYUSTOOLS PANAMA, S.A.'),
(1095, 'VARELA INT., S.A.'),
(1096, 'VENECIA ENTERPRISE S.A.'),
(1097, 'TAKE ROLL COMPANY, LTD., S.A.'),
(1098, 'TAMI LATIN AMERICAN, S.A.'),
(1099, 'TAN´S CORPORATION S.A.'),
(1100, 'TANGO INTERNATIONAL CORP., S.A.'),
(1101, 'TECNOLAM AMERICA'),
(1102, 'TESSONE, S.A.'),
(1103, 'TEX TELA ZONA LIBRE, S.A.'),
(1104, 'TEXAS INTERNACIONAL, S.A.'),
(1105, 'THE QUICK TRADING PANAMA, S.A.'),
(1106, 'TIME ZONE INC.'),
(1107, 'TOP SHOES INTERNATIONAL'),
(1108, 'TOROTRAC (CAMPOTENCIA, S.A.)'),
(1109, 'T-SHIRTS &amp, CAP CO. (UNICRESE)'),
(1110, 'SYL INTERNATIONAL, S.A.'),
(1111, 'SUPREME FASHION'),
(1112, 'SUPRO MUNDIAL, S.A'),
(1113, 'SURTIJOYAS ZONA LIBRE, S.A.'),
(1114, 'SEN SEN INT`L, S.A.'),
(1115, 'SILVER RIVER TEX, S.A.'),
(1116, 'SINKATEX INTERNACIONAL'),
(1117, 'SIRENA ELECTRONICS'),
(1118, 'SIRENA INTERNACIONAL'),
(1119, 'SKIN GLASS, S.A. (SOLAR DIAMOND)'),
(1120, 'SOLVIJAYA ZONA LIBRE, S.A.'),
(1121, 'SONNETI INTERNACIONAL, S.A.'),
(1122, 'SPEED ZONA LIBRE, S.A.'),
(1123, 'SPLASH INT., S.A.'),
(1124, 'SPORT GALLERY CORP., S.A.'),
(1125, 'STARCO, S.A.'),
(1126, 'SUNLAND TRADING INC.'),
(1127, 'SUNTEX, S.A.'),
(1128, 'SANSIM INC.'),
(1129, 'SEA &amp, SUN'),
(1130, 'SEL INTERNATIONAL, S.A.'),
(1131, 'ROCKET INTERNACIONAL, S.A.'),
(1132, 'RODEO DEPOT'),
(1133, 'RODEO IMPORT'),
(1134, 'ROSEN IMPORTADORES Y EXPORTADORES, S.A.'),
(1135, 'RUNNER ATHLETIC, S.A.'),
(1136, 'SAINT HONORE, S.A.'),
(1137, 'PICKENS EXPORT &amp, IMPORT INC.'),
(1138, 'PREMIER ELECTRIC'),
(1139, 'PRESTIGE FASHION, S.A.'),
(1140, 'PRIMAVERA MANUFACTURING, S.A.'),
(1141, 'PUNTO MODA'),
(1142, 'RABAZA, S.A.'),
(1143, 'RADA INTERNACIONAL'),
(1144, 'R.C.N. INTERNACIONAL, S.A.'),
(1145, 'RAFKAS IMPORT &amp, EXPORT, S.A.'),
(1146, 'RAQUEL COSMETICS, S.A.'),
(1147, 'RAY PANAMA IMPORT &amp, EXPORT S.A.'),
(1148, 'RAYCO, S.A.'),
(1149, 'RIMIAR INT´L, S.A'),
(1150, 'PERFUMERIA MONALISA'),
(1151, 'PIAZZA INTERNATIONAL, S.A.'),
(1152, 'PICASSO INTERNACIONAL, S.A.'),
(1153, 'PERFECT TRADING COMPANY CORP.'),
(1154, 'PERFUMERIA BEVERLY HILLS, S.A.'),
(1155, 'PERFUMERIA EL CORONEL Z.L., S.A.'),
(1156, 'PERFUMERIA MIRACLE, S.A.'),
(1157, 'NOVA ZONA LIBRE, S.A.'),
(1158, 'OFIMAK ZONA LIBRE'),
(1159, 'ONLY BRANDS, S.A.'),
(1160, 'PANAFOTO ZONA LIBRE, S.A.'),
(1161, 'PANASUR Z.L.'),
(1162, 'PARANA INTERNACIONAL, S.A.'),
(1163, 'PARIS ZONA LIBRE, S.A.'),
(1164, 'PAZZOS ZONA LIBRE, S.A.'),
(1165, 'NIPPON AMERICA GROUP PANAMA INC.'),
(1166, 'NORITEX, S.A.'),
(1167, 'NORTH TRADING INC.'),
(1168, 'NOSTRUM ENTERPRISES, S.A.'),
(1169, 'BAMBARY CORP.'),
(1170, 'NEW STYLE FREE ZONE CORP.'),
(1171, 'NIKOMOTO, S.A.'),
(1172, 'CARGA LANDOS, S.A.'),
(1173, 'LA PARKAN PANAMA (Air &amp, Ocean Shipping)'),
(1174, 'LEAR LOGISTICS CORP.'),
(1175, 'ETERNITY INT´L FREIGHT FORWARDER (PANAMA) INC.'),
(1176, 'EXPRESS DISTRIBUTORS INC.'),
(1177, 'FCI LOGISTICS'),
(1178, 'FIRST LOGISTICS SERVICES, S.A. (FILSSA)'),
(1179, 'FLETES CONSOLIDADOS, S.A.'),
(1180, 'FORWARDING &amp, LOGISTIC SOLUTIONS (FLS AMAD)'),
(1181, 'GIOFEPA, S.A.'),
(1182, 'GENERAL CARGO'),
(1183, 'GERA ENTERPRISES, S.A.'),
(1184, 'IMPORTACIONES Y SERVICIOS GS, S.A.'),
(1185, 'J. CAIN &amp, CO'),
(1186, 'KLC PANAMA LOGISTICS, S.A'),
(1187, 'KLG IMPORT &amp, EXPORT, S.A.'),
(1188, 'CORRETAJE DE ADUANA Y SERVICIOS'),
(1189, 'CREDICORP BANK'),
(1190, 'DORMAR PANAMA, S.A.'),
(1191, 'DUALTEC'),
(1192, 'CONSOLIDACIONES Y TRAMITES, Z.L.'),
(1193, 'ALDEPOSITOS ZONA LIBRE, S.A.'),
(1194, 'ARTURO ARAUZ INT., S.A.'),
(1195, 'BANCO ALIADO, S.A.'),
(1196, 'BANK OF CHINA'),
(1197, 'BLU LOGISTICS'),
(1198, 'BRANDSMART ZONA LIBRE, S.A.'),
(1199, 'CARGAS INTEGRADAS, S.A.'),
(1200, 'CARGO FREIGHT SERVICES'),
(1201, 'CARGOXPRESS LOGISTICS, S.A.'),
(1202, 'COMANDO TRUCKS LOGISTICS GROUP'),
(1203, 'CONNEXION LOGISTICS CORP.'),
(1204, 'AIR CARGO TRANS, S.A.'),
(1205, 'AIR SEA WORLDWIDE PANAMA, S.A.'),
(1206, 'Nuevo Cliente'),
(1207, 'Modificar cliente'),
(1208, 'Deshabilitar cliente'),
(1210, 'Login'),
(1212, 'Reporte');

-- --------------------------------------------------------

--
-- Table structure for table `forms`
--

CREATE TABLE `forms` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL,
  `data` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forms`
--

INSERT INTO `forms` (`id`, `name`, `data`) VALUES
(1, 'cliente_nuevo', '{\"roles\":{\"form-submit\":[\"guest\"],\"form-list\":[],\"form-edit\":[],\"form-delete\":[],\"entries-list\":[],\"entries-edit\":[],\"entries-delete\":[],\"entries-page\":[]},\"framework\":\"Basic\",\"submitText\":\"Submit\",\"successMessage\":\"Thank you, your form has been submitted.\",\"errorMessage\":\"One or more errors prevented submission of the form. Please correct and try again.\",\"frBasic_cssURL\":\"\\/site\\/modules\\/FormBuilder\\/frameworks\\/basic\\/main.css\",\"emailSubject\":\"Form Submission\",\"responderSubject\":\"Auto-Response\",\"saveFlags\":9,\"listFields\":[\"form_cliente_nombre\"],\"savePageTemplate\":43,\"savePageStatus\":1,\"savePageFields\":{\"1\":\"form_cliente_nombre\",\"98\":\"form_cliente_nombre\",\"101\":\"form_cliente_location\",\"102\":\"form_cliente_location_descripcion\",\"105\":\"form_cliente_correo\",\"106\":\"form_cliente_telefono\",\"107\":\"form_cliente_sitio_web\"},\"savePageParent\":1025,\"children\":{\"form_cliente_nombre\":{\"type\":\"Text\",\"label\":\"Nombre completo\",\"maxlength\":2048},\"form_cliente_location\":{\"type\":\"Page\",\"label\":\"Ubicaci\\u00f3n\",\"inputfield\":\"InputfieldSelect\",\"template_id\":\"44\",\"labelFieldName\":\"title\"},\"form_cliente_location_descripcion\":{\"type\":\"Text\",\"label\":\"Descripci\\u00f3n de ubicaci\\u00f3n\",\"maxlength\":2048},\"form_cliente_empresa\":{\"type\":\"Text\",\"label\":\"Nombre de la empresa\",\"maxlength\":2048},\"form_cliente_correo\":{\"type\":\"Text\",\"label\":\"Correo\",\"maxlength\":2048},\"form_cliente_telefono\":{\"type\":\"Text\",\"label\":\"Tel\\u00e9fono\",\"maxlength\":2048},\"form_cliente_sitio_web\":{\"type\":\"Text\",\"label\":\"Sitio web cliente\",\"maxlength\":2048}}}');

-- --------------------------------------------------------

--
-- Table structure for table `forms_entries`
--

CREATE TABLE `forms_entries` (
  `id` int(10) UNSIGNED NOT NULL,
  `forms_id` int(10) UNSIGNED NOT NULL,
  `data` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forms_entries`
--

INSERT INTO `forms_entries` (`id`, `forms_id`, `data`, `created`) VALUES
(2, 1, '{\"form_cliente_nombre\":\"Isaac Giancarlo Tisnado\",\"form_cliente_location\":\"1015\",\"form_cliente_location_descripcion\":\"Istiweb, 12 de octubre al frente del Comercial Panam\\u00e1\",\"form_cliente_empresa\":\"ISTIWEB\",\"form_cliente_correo\":\"itisnado11@gmail.com\",\"form_cliente_telefono\":\"68610691\",\"form_cliente_sitio_web\":\"www.istiweb.com\",\"cliente_nuevo_submit\":\"Submit\",\"_savePage\":1031,\"_savePageTime\":1560228599}', '2019-06-11 11:49:59');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `class` varchar(128) CHARACTER SET ascii NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES
(1, 'FieldtypeTextarea', 0, '', '2019-06-11 04:05:41'),
(2, 'FieldtypeNumber', 0, '', '2019-06-11 04:05:41'),
(3, 'FieldtypeText', 0, '', '2019-06-11 04:05:41'),
(4, 'FieldtypePage', 0, '', '2019-06-11 04:05:41'),
(30, 'InputfieldForm', 0, '', '2019-06-11 04:05:41'),
(6, 'FieldtypeFile', 0, '', '2019-06-11 04:05:41'),
(7, 'ProcessPageEdit', 1, '', '2019-06-11 04:05:41'),
(10, 'ProcessLogin', 0, '', '2019-06-11 04:05:41'),
(12, 'ProcessPageList', 0, '{\"pageLabelField\":\"title\",\"paginationLimit\":25,\"limit\":50}', '2019-06-11 04:05:41'),
(121, 'ProcessPageEditLink', 1, '', '2019-06-11 04:05:41'),
(14, 'ProcessPageSort', 0, '', '2019-06-11 04:05:41'),
(15, 'InputfieldPageListSelect', 0, '', '2019-06-11 04:05:41'),
(117, 'JqueryUI', 1, '', '2019-06-11 04:05:41'),
(17, 'ProcessPageAdd', 0, '', '2019-06-11 04:05:41'),
(125, 'SessionLoginThrottle', 11, '', '2019-06-11 04:05:41'),
(122, 'InputfieldPassword', 0, '', '2019-06-11 04:05:41'),
(25, 'InputfieldAsmSelect', 0, '', '2019-06-11 04:05:41'),
(116, 'JqueryCore', 1, '', '2019-06-11 04:05:41'),
(27, 'FieldtypeModule', 0, '', '2019-06-11 04:05:41'),
(28, 'FieldtypeDatetime', 0, '', '2019-06-11 04:05:41'),
(29, 'FieldtypeEmail', 0, '', '2019-06-11 04:05:41'),
(108, 'InputfieldURL', 0, '', '2019-06-11 04:05:41'),
(32, 'InputfieldSubmit', 0, '', '2019-06-11 04:05:41'),
(33, 'InputfieldWrapper', 0, '', '2019-06-11 04:05:41'),
(34, 'InputfieldText', 0, '', '2019-06-11 04:05:41'),
(35, 'InputfieldTextarea', 0, '', '2019-06-11 04:05:41'),
(36, 'InputfieldSelect', 0, '', '2019-06-11 04:05:41'),
(37, 'InputfieldCheckbox', 0, '', '2019-06-11 04:05:41'),
(38, 'InputfieldCheckboxes', 0, '', '2019-06-11 04:05:41'),
(39, 'InputfieldRadios', 0, '', '2019-06-11 04:05:41'),
(40, 'InputfieldHidden', 0, '', '2019-06-11 04:05:41'),
(41, 'InputfieldName', 0, '', '2019-06-11 04:05:41'),
(43, 'InputfieldSelectMultiple', 0, '', '2019-06-11 04:05:41'),
(45, 'JqueryWireTabs', 0, '', '2019-06-11 04:05:41'),
(46, 'ProcessPage', 0, '', '2019-06-11 04:05:41'),
(47, 'ProcessTemplate', 0, '', '2019-06-11 04:05:41'),
(48, 'ProcessField', 32, '', '2019-06-11 04:05:41'),
(50, 'ProcessModule', 0, '', '2019-06-11 04:05:41'),
(114, 'PagePermissions', 3, '', '2019-06-11 04:05:41'),
(97, 'FieldtypeCheckbox', 1, '', '2019-06-11 04:05:41'),
(115, 'PageRender', 3, '{\"clearCache\":1}', '2019-06-11 04:05:41'),
(55, 'InputfieldFile', 0, '', '2019-06-11 04:05:41'),
(56, 'InputfieldImage', 0, '', '2019-06-11 04:05:41'),
(57, 'FieldtypeImage', 0, '', '2019-06-11 04:05:41'),
(60, 'InputfieldPage', 0, '{\"inputfieldClasses\":[\"InputfieldSelect\",\"InputfieldSelectMultiple\",\"InputfieldCheckboxes\",\"InputfieldRadios\",\"InputfieldAsmSelect\",\"InputfieldPageListSelect\",\"InputfieldPageListSelectMultiple\"]}', '2019-06-11 04:05:41'),
(61, 'TextformatterEntities', 0, '', '2019-06-11 04:05:41'),
(66, 'ProcessUser', 0, '{\"showFields\":[\"name\",\"email\",\"roles\"]}', '2019-06-11 04:05:41'),
(67, 'MarkupAdminDataTable', 0, '', '2019-06-11 04:05:41'),
(68, 'ProcessRole', 0, '{\"showFields\":[\"name\"]}', '2019-06-11 04:05:41'),
(76, 'ProcessList', 0, '', '2019-06-11 04:05:41'),
(78, 'InputfieldFieldset', 0, '', '2019-06-11 04:05:41'),
(79, 'InputfieldMarkup', 0, '', '2019-06-11 04:05:41'),
(80, 'InputfieldEmail', 0, '', '2019-06-11 04:05:41'),
(89, 'FieldtypeFloat', 1, '', '2019-06-11 04:05:41'),
(83, 'ProcessPageView', 0, '', '2019-06-11 04:05:41'),
(84, 'FieldtypeInteger', 0, '', '2019-06-11 04:05:41'),
(85, 'InputfieldInteger', 0, '', '2019-06-11 04:05:41'),
(86, 'InputfieldPageName', 0, '', '2019-06-11 04:05:41'),
(87, 'ProcessHome', 0, '', '2019-06-11 04:05:41'),
(90, 'InputfieldFloat', 0, '', '2019-06-11 04:05:41'),
(94, 'InputfieldDatetime', 0, '', '2019-06-11 04:05:41'),
(98, 'MarkupPagerNav', 0, '', '2019-06-11 04:05:41'),
(129, 'ProcessPageEditImageSelect', 1, '', '2019-06-11 04:05:41'),
(103, 'JqueryTableSorter', 1, '', '2019-06-11 04:05:41'),
(104, 'ProcessPageSearch', 1, '{\"searchFields\":\"title\",\"displayField\":\"title path\"}', '2019-06-11 04:05:41'),
(105, 'FieldtypeFieldsetOpen', 1, '', '2019-06-11 04:05:41'),
(106, 'FieldtypeFieldsetClose', 1, '', '2019-06-11 04:05:41'),
(107, 'FieldtypeFieldsetTabOpen', 1, '', '2019-06-11 04:05:41'),
(109, 'ProcessPageTrash', 1, '', '2019-06-11 04:05:41'),
(111, 'FieldtypePageTitle', 1, '', '2019-06-11 04:05:41'),
(112, 'InputfieldPageTitle', 0, '', '2019-06-11 04:05:41'),
(113, 'MarkupPageArray', 3, '', '2019-06-11 04:05:41'),
(131, 'InputfieldButton', 0, '', '2019-06-11 04:05:41'),
(133, 'FieldtypePassword', 1, '', '2019-06-11 04:05:41'),
(134, 'ProcessPageType', 33, '{\"showFields\":[]}', '2019-06-11 04:05:41'),
(135, 'FieldtypeURL', 1, '', '2019-06-11 04:05:41'),
(136, 'ProcessPermission', 1, '{\"showFields\":[\"name\",\"title\"]}', '2019-06-11 04:05:41'),
(137, 'InputfieldPageListSelectMultiple', 0, '', '2019-06-11 04:05:41'),
(138, 'ProcessProfile', 1, '{\"profileFields\":[\"pass\",\"email\",\"admin_theme\"]}', '2019-06-11 04:05:41'),
(139, 'SystemUpdater', 1, '{\"systemVersion\":16,\"coreVersion\":\"3.0.123\"}', '2019-06-11 04:05:41'),
(148, 'AdminThemeDefault', 10, '{\"colors\":\"classic\"}', '2019-06-11 04:05:41'),
(149, 'InputfieldSelector', 42, '', '2019-06-11 04:05:41'),
(150, 'ProcessPageLister', 32, '', '2019-06-11 04:05:41'),
(151, 'JqueryMagnific', 1, '', '2019-06-11 04:05:41'),
(152, 'PagePathHistory', 3, '', '2019-06-11 04:05:41'),
(155, 'InputfieldCKEditor', 0, '', '2019-06-11 04:05:41'),
(156, 'MarkupHTMLPurifier', 0, '', '2019-06-11 04:05:41'),
(158, 'ProcessRecentPages', 1, '', '2019-06-11 04:06:08'),
(159, 'AdminThemeUikit', 10, '', '2019-06-11 04:06:08'),
(160, 'ProcessLogger', 1, '', '2019-06-11 04:06:15'),
(161, 'InputfieldIcon', 0, '', '2019-06-11 04:06:15'),
(162, 'FieldtypeOptions', 1, '', '2019-06-11 04:08:00'),
(163, 'FormBuilderMultiplier', 2, '', '2019-06-11 04:37:24'),
(164, 'FormBuilder', 3, '{\"licenseKey\":\"PWFB4.per7506.5f8c2b9455288d58b2cfd17f2288109bfdb566dc\",\"inputfieldClasses\":[\"AsmSelect\",\"Checkbox\",\"Checkboxes\",\"Datetime\",\"Email\",\"Fieldset\",\"Float\",\"FormBuilderFile\",\"Integer\",\"Hidden\",\"Page\",\"Radios\",\"Select\",\"SelectMultiple\",\"Text\",\"Textarea\",\"URL\"],\"embedFields\":[],\"embedTag\":\"form-builder\",\"markup_list\":\"<div {attrs}>{out}\\n<\\/div>\",\"markup_item\":\"<div {attrs}>{out}\\n<\\/div>\",\"markup_item_label\":\"<label class=\'ui-widget-header\' for=\'{for}\'>{out}<\\/label>\",\"markup_item_content\":\"<div class=\'ui-widget-content\'>{out}<\\/div>\",\"markup_item_error\":\"<p><span class=\'ui-state-error\'>{out}<\\/span><\\/p>\",\"markup_item_description\":\"<p class=\'description\'>{out}<\\/p>\",\"markup_success\":\"<p class=\'ui-state-highlight\'>{out}<\\/p>\",\"markup_error\":\"<p class=\'ui-state-error\'>{out}<\\/p>\",\"classes_form\":\"\",\"classes_list\":\"Inputfields\",\"classes_list_clearfix\":\"ui-helper-clearfix\",\"classes_item\":\"Inputfield Inputfield_{name} ui-widget {class}\",\"classes_item_required\":\"InputfieldStateRequired\",\"classes_item_error\":\"InputfieldStateError ui-state-error\",\"classes_item_collapsed\":\"InputfieldStateCollapsed\",\"classes_item_column_width\":\"InputfieldColumnWidth\",\"classes_item_column_width_first\":\"InputfieldColumnWidthFirst\",\"akismetKey\":\"\",\"csvDelimiter\":\",\",\"filesPath\":\"{config.paths.cache}form-builder\\/\",\"useRoles\":\"\",\"uninstall\":\"\",\"submit_save_module\":\"Submit\"}', '2019-06-11 04:37:52'),
(165, 'ProcessFormBuilder', 1, '', '2019-06-11 04:37:52'),
(166, 'InputfieldFormBuilderFile', 0, '', '2019-06-11 04:37:52'),
(167, 'ImportPagesCSV', 1, '', '2019-06-11 04:53:12'),
(168, 'FrontEndEditLightbox', 3, '{\"isEnabled\":1,\"text\":\"Editar Cliente\",\"excluded_templates\":[],\"closeConfirmMessage\":\"Are you sure you want to close the editor?\",\"closeOnSave\":1,\"fixedSaveButton\":1,\"enableTemplateEdit\":1,\"skipLoadingScripts\":\"\",\"skipLoadingStyles\":\"\",\"selectorsToHide\":\"\",\"selectorsToHideSuperUser\":\"\",\"fieldHighlightStyle\":\"\",\"styleOverrides\":\"\",\"uninstall\":\"\",\"submit_save_module\":\"Submit\"}', '2019-06-11 07:07:07'),
(169, 'LoginRegister', 0, '{\"features\":[\"login\"],\"registerFields\":[\"name\",\"email\",\"pass\"],\"profileFields\":[\"name\",\"pass\",\"email\"],\"registerRoles\":[\"login-register:1209\"],\"uninstall\":\"\",\"submit_save_module\":\"Submit\"}', '2019-06-11 07:35:14'),
(170, 'TextformatterMarkdownExtra', 1, '', '2019-06-11 07:35:15'),
(171, 'Pages2Pdf', 3, '{\"templates\":[],\"printHeader\":1,\"printFooter\":1,\"cacheTime\":86400,\"filename\":\"{page.name}-pdf-{page.id}.pdf\",\"getVar\":\"pages2pdf\",\"creationMode\":\"1\",\"multilanguageSupport\":\"\",\"uninstall\":\"\",\"submit_save_module\":\"Submit\"}', '2019-06-11 08:16:07'),
(172, 'WirePDF', 0, '', '2019-06-11 08:16:07');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `templates_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(128) CHARACTER SET ascii NOT NULL,
  `status` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_users_id` int(10) UNSIGNED NOT NULL DEFAULT '2',
  `created` timestamp NOT NULL DEFAULT '2015-12-18 11:09:00',
  `created_users_id` int(10) UNSIGNED NOT NULL DEFAULT '2',
  `published` datetime DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES
(1, 0, 1, 'home', 9, '2019-06-11 04:13:02', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 0),
(2, 1, 2, 'cms', 1035, '2019-06-11 04:06:08', 40, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 5),
(3, 2, 2, 'page', 21, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 0),
(6, 3, 2, 'add', 21, '2019-06-11 04:06:17', 40, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 1),
(7, 1, 2, 'trash', 1039, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 6),
(8, 3, 2, 'list', 21, '2019-06-11 04:06:28', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 0),
(9, 3, 2, 'sort', 1047, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 3),
(10, 3, 2, 'edit', 1045, '2019-06-11 04:06:28', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 4),
(11, 22, 2, 'template', 21, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 0),
(16, 22, 2, 'field', 21, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 2),
(21, 2, 2, 'module', 21, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 2),
(22, 2, 2, 'setup', 21, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 1),
(23, 2, 2, 'login', 1035, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 4),
(27, 1, 29, 'http404', 1035, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 3, '2019-06-10 23:05:41', 4),
(28, 2, 2, 'access', 13, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 3),
(29, 28, 2, 'users', 29, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 0),
(30, 28, 2, 'roles', 29, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 1),
(31, 28, 2, 'permissions', 29, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 2),
(32, 31, 5, 'page-edit', 25, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 2),
(34, 31, 5, 'page-delete', 25, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 3),
(35, 31, 5, 'page-move', 25, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 4),
(36, 31, 5, 'page-view', 25, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 0),
(37, 30, 4, 'guest', 25, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 0),
(38, 30, 4, 'superuser', 25, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 1),
(41, 29, 3, 'admin', 1, '2019-06-11 04:06:08', 40, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 0),
(40, 29, 3, 'guest', 25, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 1),
(50, 31, 5, 'page-sort', 25, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 41, '2019-06-10 23:05:41', 5),
(51, 31, 5, 'page-template', 25, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 41, '2019-06-10 23:05:41', 6),
(52, 31, 5, 'user-admin', 25, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 41, '2019-06-10 23:05:41', 10),
(53, 31, 5, 'profile-edit', 1, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 41, '2019-06-10 23:05:41', 13),
(54, 31, 5, 'page-lock', 1, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 41, '2019-06-10 23:05:41', 8),
(300, 3, 2, 'search', 1045, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 6),
(301, 3, 2, 'trash', 1047, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 6),
(302, 3, 2, 'link', 1041, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 7),
(303, 3, 2, 'image', 1041, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 8),
(304, 2, 2, 'profile', 1025, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 41, '2019-06-10 23:05:41', 5),
(1000, 1, 26, 'search', 1025, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 3),
(1001, 1, 29, 'about', 1, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 0),
(1002, 1001, 29, 'what', 1, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 0),
(1004, 1001, 29, 'background', 1, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 1),
(1005, 1, 34, 'site-map', 1, '2019-06-11 04:05:41', 41, '2019-06-11 04:05:41', 2, '2019-06-10 23:05:41', 2),
(1006, 31, 5, 'page-lister', 1, '2019-06-11 04:05:41', 40, '2019-06-11 04:05:41', 40, '2019-06-10 23:05:41', 9),
(1007, 3, 2, 'lister', 1, '2019-06-11 04:05:41', 40, '2019-06-11 04:05:41', 40, '2019-06-10 23:05:41', 9),
(1009, 3, 2, 'recent-pages', 1, '2019-06-11 04:06:08', 40, '2019-06-11 04:06:08', 40, '2019-06-10 23:06:08', 10),
(1010, 31, 5, 'page-edit-recent', 1, '2019-06-11 04:06:08', 40, '2019-06-11 04:06:08', 40, '2019-06-10 23:06:08', 10),
(1011, 22, 2, 'logs', 1, '2019-06-11 04:06:15', 40, '2019-06-11 04:06:15', 40, '2019-06-10 23:06:15', 2),
(1012, 31, 5, 'logs-view', 1, '2019-06-11 04:06:15', 40, '2019-06-11 04:06:15', 40, '2019-06-10 23:06:15', 11),
(1013, 31, 5, 'logs-edit', 1, '2019-06-11 04:06:15', 40, '2019-06-11 04:06:15', 40, '2019-06-10 23:06:15', 12),
(1014, 1, 49, 'localizacion', 1, '2019-06-11 06:38:57', 41, '2019-06-11 04:13:27', 41, '2019-06-10 23:13:30', 6),
(1015, 1014, 44, 'panama', 1, '2019-06-11 04:14:05', 41, '2019-06-11 04:14:05', 41, '2019-06-10 23:14:05', 0),
(1016, 1014, 44, 'panama-oeste', 1, '2019-06-11 04:14:24', 41, '2019-06-11 04:14:14', 41, '2019-06-10 23:14:24', 1),
(1017, 1014, 44, 'veraguas', 1, '2019-06-11 04:14:35', 41, '2019-06-11 04:14:35', 41, '2019-06-10 23:14:35', 2),
(1018, 1014, 44, 'los-santos', 1, '2019-06-11 04:14:43', 41, '2019-06-11 04:14:43', 41, '2019-06-10 23:14:43', 3),
(1019, 1014, 44, 'herrera', 1, '2019-06-11 04:14:51', 41, '2019-06-11 04:14:51', 41, '2019-06-10 23:14:51', 4),
(1020, 1014, 44, 'darien', 1, '2019-06-11 04:14:59', 41, '2019-06-11 04:14:59', 41, '2019-06-10 23:14:59', 5),
(1021, 1014, 44, 'chiriqui', 1, '2019-06-11 04:15:08', 41, '2019-06-11 04:15:06', 41, '2019-06-10 23:15:08', 6),
(1022, 1014, 44, 'colon', 1, '2019-06-11 04:15:19', 41, '2019-06-11 04:15:19', 41, '2019-06-10 23:15:19', 7),
(1023, 1014, 44, 'cocle', 1, '2019-06-11 04:15:34', 41, '2019-06-11 04:15:26', 41, '2019-06-10 23:15:34', 8),
(1024, 1014, 44, 'bocas-del-toro', 1, '2019-06-11 04:15:43', 41, '2019-06-11 04:15:41', 41, '2019-06-10 23:15:41', 9),
(1025, 1, 47, 'clientes', 1, '2019-06-11 05:44:14', 41, '2019-06-11 04:24:48', 41, '2019-06-10 23:24:51', 7),
(1026, 1, 29, 'bitacora', 1, '2019-06-11 04:25:07', 41, '2019-06-11 04:25:04', 41, '2019-06-10 23:25:07', 8),
(1027, 1, 46, 'form-builder', 1025, '2019-06-11 04:37:52', 41, '2019-06-11 04:37:52', 41, '2019-06-10 23:37:52', 9),
(1028, 31, 5, 'form-builder', 1, '2019-06-11 04:37:52', 41, '2019-06-11 04:37:52', 41, '2019-06-10 23:37:52', 13),
(1029, 31, 5, 'form-builder-add', 1, '2019-06-11 04:37:52', 41, '2019-06-11 04:37:52', 41, '2019-06-10 23:37:52', 14),
(1030, 22, 2, 'form-builder', 1, '2019-06-11 04:37:52', 41, '2019-06-11 04:37:52', 41, '2019-06-10 23:37:52', 3),
(1031, 1025, 43, '0.88375900-1560228599', 1, '2019-06-11 07:01:45', 41, '2019-06-11 04:49:59', 41, '2019-06-10 23:49:59', 0),
(1032, 22, 2, 'import-pages-csv', 1, '2019-06-11 04:53:12', 41, '2019-06-11 04:53:12', 41, '2019-06-10 23:53:12', 4),
(1033, 1025, 43, 'kmq-moda-s.a-kmq-opticos-s.a', 1, '2019-06-11 04:57:46', 41, '2019-06-11 04:57:46', 41, '2019-06-10 23:57:46', 1),
(1034, 1025, 43, 'panapark-free-zone', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 2),
(1035, 1025, 43, 'jumbo-zona-libre-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 3),
(1036, 1025, 43, 'vertigo-zona-libre-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 4),
(1037, 1025, 43, 'intratex-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 5),
(1038, 1025, 43, 'carlindo-montacargas', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 6),
(1039, 1025, 43, 'gold-america', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 7),
(1040, 1025, 43, 'marco-xenia-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 8),
(1041, 1025, 43, 'dvta-international-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 9),
(1042, 1025, 43, 'mira-panama-inc', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 10),
(1043, 1025, 43, 'monturas-y-gafas-n.l.p-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 11),
(1044, 1025, 43, 'panama-fasteners', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 12),
(1045, 1025, 43, 'rimatex-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 13),
(1046, 1025, 43, 'star-auto-parts-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 14),
(1047, 1025, 43, 't-shirts-cap-co-unicrese', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 15),
(1048, 1025, 43, 'ueta-latinoamerica-inc', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 16),
(1049, 1025, 43, 'a-j-international-group-s.a', 1, '2019-06-11 07:21:56', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 17),
(1050, 1025, 43, 'abouganem-y-cia-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 18),
(1051, 1025, 43, 'afisa-zona-libre', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 19),
(1052, 1025, 43, 'agromarauto-international-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 20),
(1053, 1025, 43, 'alcon-trading', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 21),
(1054, 1025, 43, 'alpha-trading', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 22),
(1055, 1025, 43, 'amazon-zona-libre', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 23),
(1056, 1025, 43, 'america-zona-libre-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 24),
(1057, 1025, 43, 'american-studios-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 25),
(1058, 1025, 43, 'amor-de-casa-textile-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 26),
(1059, 1025, 43, 'anacasti-internacional-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 27),
(1060, 1025, 43, 'argelia-internacional-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 28),
(1061, 1025, 43, 'argyros-inc', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 29),
(1062, 1025, 43, 'aristgom', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 30),
(1063, 1025, 43, 'aryan-interncional-zona-libre', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 31),
(1064, 1025, 43, 'asatex-int-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 32),
(1065, 1025, 43, 'asociacion-de-usuarios-de-la-zona-libre-de-colon', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 33),
(1066, 1025, 43, 'auto-accesorios-mundiales-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 34),
(1067, 1025, 43, 'auto-center-zona-libre-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 35),
(1068, 1025, 43, 'auto-import-internacional-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 36),
(1069, 1025, 43, 'autopiezas-casa-japon-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 37),
(1070, 1025, 43, 'babytex-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 38),
(1071, 1025, 43, 'rodeo-sport', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 39),
(1072, 1025, 43, 'a.i.r-ajustadores-surveyors', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 40),
(1073, 1025, 43, 'abisa-internacional-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 41),
(1074, 1025, 43, 'actwell-panama-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 42),
(1075, 1025, 43, 'aduanas-santos-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 43),
(1076, 1025, 43, 'aduanas-visuette-dutary-y-abad-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 44),
(1077, 1025, 43, 'aero-mar-logistic-internacional-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 45),
(1078, 1025, 43, 'vilco-internacional-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 46),
(1079, 1025, 43, 'washington-international-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 47),
(1080, 1025, 43, 'westinghouse-lighting-latin-america', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 48),
(1081, 1025, 43, 'winko-intl-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 49),
(1082, 1025, 43, 'world-target-international-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 50),
(1083, 1025, 43, 'world-time-inc', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 51),
(1084, 1025, 43, 'yamaha-music-latin-america', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 52),
(1085, 1025, 43, 'yuhang-international-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 53),
(1086, 1025, 43, 'zeneo-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 54),
(1087, 1025, 43, 'zhong-ma-international-investment-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 55),
(1088, 1025, 43, 'zivic-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 56),
(1089, 1025, 43, 'a-amp-j-panama-corporation', 1, '2019-06-11 07:22:10', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 57),
(1090, 1025, 43, 'vestirama-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 58),
(1091, 1025, 43, 'vida-panama', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 59),
(1092, 1025, 43, 'union-zona-libre-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 60),
(1093, 1025, 43, 'unison-international-s.a-ava', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 61),
(1094, 1025, 43, 'uyustools-panama-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 62),
(1095, 1025, 43, 'varela-int-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 63),
(1096, 1025, 43, 'venecia-enterprise-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 64),
(1097, 1025, 43, 'take-roll-company-ltd-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 65),
(1098, 1025, 43, 'tami-latin-american-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 66),
(1099, 1025, 43, 'tans-corporation-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 67),
(1100, 1025, 43, 'tango-international-corp-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 68),
(1101, 1025, 43, 'tecnolam-america', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 69),
(1102, 1025, 43, 'tessone-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 70),
(1103, 1025, 43, 'tex-tela-zona-libre-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 71),
(1104, 1025, 43, 'texas-internacional-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 72),
(1105, 1025, 43, 'the-quick-trading-panama-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 73),
(1106, 1025, 43, 'time-zone-inc', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 74),
(1107, 1025, 43, 'top-shoes-international', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 75),
(1108, 1025, 43, 'torotrac-campotencia-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 76),
(1109, 1025, 43, 't-shirts-amp-cap-co-unicrese', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 77),
(1110, 1025, 43, 'syl-international-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 78),
(1111, 1025, 43, 'supreme-fashion', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 79),
(1112, 1025, 43, 'supro-mundial-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 80),
(1113, 1025, 43, 'surtijoyas-zona-libre-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 81),
(1114, 1025, 43, 'sen-sen-int-l-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 82),
(1115, 1025, 43, 'silver-river-tex-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 83),
(1116, 1025, 43, 'sinkatex-internacional', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 84),
(1117, 1025, 43, 'sirena-electronics', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 85),
(1118, 1025, 43, 'sirena-internacional', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 86),
(1119, 1025, 43, 'skin-glass-s.a-solar-diamond', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 87),
(1120, 1025, 43, 'solvijaya-zona-libre-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 88),
(1121, 1025, 43, 'sonneti-internacional-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 89),
(1122, 1025, 43, 'speed-zona-libre-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 90),
(1123, 1025, 43, 'splash-int-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 91),
(1124, 1025, 43, 'sport-gallery-corp-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 92),
(1125, 1025, 43, 'starco-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 93),
(1126, 1025, 43, 'sunland-trading-inc', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 94),
(1127, 1025, 43, 'suntex-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 95),
(1128, 1025, 43, 'sansim-inc', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 96),
(1129, 1025, 43, 'sea-amp-sun', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 97),
(1130, 1025, 43, 'sel-international-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 98),
(1131, 1025, 43, 'rocket-internacional-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 99),
(1132, 1025, 43, 'rodeo-depot', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 100),
(1133, 1025, 43, 'rodeo-import', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 101),
(1134, 1025, 43, 'rosen-importadores-y-exportadores-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 102),
(1135, 1025, 43, 'runner-athletic-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 103),
(1136, 1025, 43, 'saint-honore-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 104),
(1137, 1025, 43, 'pickens-export-amp-import-inc', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 105),
(1138, 1025, 43, 'premier-electric', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 106),
(1139, 1025, 43, 'prestige-fashion-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 107),
(1140, 1025, 43, 'primavera-manufacturing-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 108),
(1141, 1025, 43, 'punto-moda', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 109),
(1142, 1025, 43, 'rabaza-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 110),
(1143, 1025, 43, 'rada-internacional', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 111),
(1144, 1025, 43, 'r.c.n-internacional-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 112),
(1145, 1025, 43, 'rafkas-import-amp-export-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 113),
(1146, 1025, 43, 'raquel-cosmetics-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 114),
(1147, 1025, 43, 'ray-panama-import-amp-export-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 115),
(1148, 1025, 43, 'rayco-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 116),
(1149, 1025, 43, 'rimiar-intl-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 117),
(1150, 1025, 43, 'perfumeria-monalisa', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 118),
(1151, 1025, 43, 'piazza-international-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 119),
(1152, 1025, 43, 'picasso-internacional-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 120),
(1153, 1025, 43, 'perfect-trading-company-corp', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 121),
(1154, 1025, 43, 'perfumeria-beverly-hills-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 122),
(1155, 1025, 43, 'perfumeria-el-coronel-z.l-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 123),
(1156, 1025, 43, 'perfumeria-miracle-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 124),
(1157, 1025, 43, 'nova-zona-libre-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 125),
(1158, 1025, 43, 'ofimak-zona-libre', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 126),
(1159, 1025, 43, 'only-brands-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 127),
(1160, 1025, 43, 'panafoto-zona-libre-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 128),
(1161, 1025, 43, 'panasur-z.l', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 129),
(1162, 1025, 43, 'parana-internacional-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 130),
(1163, 1025, 43, 'paris-zona-libre-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 131),
(1164, 1025, 43, 'pazzos-zona-libre-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 132),
(1165, 1025, 43, 'nippon-america-group-panama-inc', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 133),
(1166, 1025, 43, 'noritex-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 134),
(1167, 1025, 43, 'north-trading-inc', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 135),
(1168, 1025, 43, 'nostrum-enterprises-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 136),
(1169, 1025, 43, 'bambary-corp', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 137),
(1170, 1025, 43, 'new-style-free-zone-corp', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 138),
(1171, 1025, 43, 'nikomoto-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 139),
(1172, 1025, 43, 'carga-landos-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 140),
(1173, 1025, 43, 'la-parkan-panama-air-amp-ocean-shipping', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 141),
(1174, 1025, 43, 'lear-logistics-corp', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 142),
(1175, 1025, 43, 'eternity-intl-freight-forwarder-panama-inc', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 143),
(1176, 1025, 43, 'express-distributors-inc', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 144),
(1177, 1025, 43, 'fci-logistics', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 145),
(1178, 1025, 43, 'first-logistics-services-s.a-filssa', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 146),
(1179, 1025, 43, 'fletes-consolidados-s.a', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 147),
(1180, 1025, 43, 'forwarding-amp-logistic-solutions-fls-amad', 1, '2019-06-11 04:57:47', 41, '2019-06-11 04:57:47', 41, '2019-06-10 23:57:47', 148),
(1181, 1025, 43, 'giofepa-s.a', 1, '2019-06-11 07:27:50', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 149),
(1182, 1025, 43, 'general-cargo', 1, '2019-06-11 04:57:48', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 150),
(1183, 1025, 43, 'gera-enterprises-s.a', 1, '2019-06-11 04:57:48', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 151),
(1184, 1025, 43, 'importaciones-y-servicios-gs-s.a', 1, '2019-06-11 04:57:48', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 152),
(1185, 1025, 43, 'j-cain-amp-co', 1, '2019-06-11 04:57:48', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 153),
(1186, 1025, 43, 'klc-panama-logistics-s.a', 1, '2019-06-11 04:57:48', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 154),
(1187, 1025, 43, 'klg-import-amp-export-s.a', 1, '2019-06-11 04:57:48', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 155),
(1188, 1025, 43, 'corretaje-de-aduana-y-servicios', 1, '2019-06-11 04:57:48', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 156),
(1189, 1025, 43, 'credicorp-bank', 1, '2019-06-11 04:57:48', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 157),
(1190, 1025, 43, 'dormar-panama-s.a', 1, '2019-06-11 04:57:48', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 158),
(1191, 1025, 43, 'dualtec', 1, '2019-06-11 04:57:48', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 159),
(1192, 1025, 43, 'consolidaciones-y-tramites-z.l', 1, '2019-06-11 04:57:48', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 160),
(1193, 1025, 43, 'aldepositos-zona-libre-s.a', 1, '2019-06-11 04:57:48', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 161),
(1194, 1025, 43, 'arturo-arauz-int-s.a', 1, '2019-06-11 04:57:48', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 162),
(1195, 1025, 43, 'banco-aliado-s.a', 1, '2019-06-11 04:57:48', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 163),
(1196, 1025, 43, 'bank-of-china', 1, '2019-06-11 04:57:48', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 164),
(1197, 1025, 43, 'blu-logistics', 1, '2019-06-11 04:57:48', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 165),
(1198, 1025, 43, 'brandsmart-zona-libre-s.a', 1, '2019-06-11 04:57:48', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 166),
(1199, 1025, 43, 'cargas-integradas-s.a', 1, '2019-06-11 04:57:48', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 167),
(1200, 1025, 43, 'cargo-freight-services', 1, '2019-06-11 04:57:48', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 168),
(1201, 1025, 43, 'cargoxpress-logistics-s.a', 1, '2019-06-11 04:57:48', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 169),
(1202, 1025, 43, 'comando-trucks-logistics-group', 1, '2019-06-11 04:57:48', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 170),
(1203, 1025, 43, 'connexion-logistics-corp', 1, '2019-06-11 04:57:48', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 171),
(1204, 1025, 43, 'air-cargo-trans-s.a', 1, '2019-06-11 04:57:48', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 172),
(1205, 1025, 43, 'air-sea-worldwide-panama-s.a', 1, '2019-06-11 04:57:48', 41, '2019-06-11 04:57:48', 41, '2019-06-10 23:57:48', 173),
(1206, 1025, 48, 'nuevo-cliente', 1, '2019-06-11 06:24:49', 41, '2019-06-11 06:24:49', 41, '2019-06-11 01:24:49', 174),
(1207, 1025, 50, 'modificar-cliente', 1, '2019-06-11 07:09:11', 41, '2019-06-11 07:09:11', 41, '2019-06-11 02:09:11', 175),
(1208, 1025, 51, 'deshabilitar-cliente', 1, '2019-06-11 07:24:29', 41, '2019-06-11 07:24:21', 41, '2019-06-11 02:24:21', 176),
(1209, 30, 4, 'login-register', 1, '2019-06-11 07:35:14', 41, '2019-06-11 07:35:14', 41, '2019-06-11 02:35:14', 2),
(1210, 1, 52, 'login', 1, '2019-06-11 07:45:47', 41, '2019-06-11 07:45:47', 41, '2019-06-11 02:45:47', 10),
(1211, 29, 3, 'invitado', 1, '2019-06-11 08:03:06', 40, '2019-06-11 08:03:06', 40, '2019-06-11 03:03:06', 2),
(1212, 1, 53, 'reporte', 2049, '2019-06-11 08:18:03', 41, '2019-06-11 08:18:03', 41, NULL, 11);

-- --------------------------------------------------------

--
-- Table structure for table `pages_access`
--

CREATE TABLE `pages_access` (
  `pages_id` int(11) NOT NULL,
  `templates_id` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages_access`
--

INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES
(37, 2, '2019-06-11 04:05:41'),
(38, 2, '2019-06-11 04:05:41'),
(32, 2, '2019-06-11 04:05:41'),
(34, 2, '2019-06-11 04:05:41'),
(35, 2, '2019-06-11 04:05:41'),
(36, 2, '2019-06-11 04:05:41'),
(50, 2, '2019-06-11 04:05:41'),
(51, 2, '2019-06-11 04:05:41'),
(52, 2, '2019-06-11 04:05:41'),
(53, 2, '2019-06-11 04:05:41'),
(54, 2, '2019-06-11 04:05:41'),
(1006, 2, '2019-06-11 04:05:41'),
(1010, 2, '2019-06-11 04:06:08'),
(1012, 2, '2019-06-11 04:06:15'),
(1013, 2, '2019-06-11 04:06:15'),
(1014, 1, '2019-06-11 04:13:27'),
(1212, 1, '2019-06-11 08:18:03'),
(1210, 1, '2019-06-11 07:45:47'),
(1209, 2, '2019-06-11 07:35:14'),
(1208, 1, '2019-06-11 07:24:21'),
(1207, 1, '2019-06-11 07:09:11'),
(1025, 1, '2019-06-11 04:24:48'),
(1026, 1, '2019-06-11 04:25:04'),
(1027, 1, '2019-06-11 04:37:52'),
(1028, 2, '2019-06-11 04:37:52'),
(1029, 2, '2019-06-11 04:37:52'),
(1206, 1, '2019-06-11 06:24:49');

-- --------------------------------------------------------

--
-- Table structure for table `pages_parents`
--

CREATE TABLE `pages_parents` (
  `pages_id` int(10) UNSIGNED NOT NULL,
  `parents_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages_parents`
--

INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES
(2, 1),
(3, 1),
(3, 2),
(7, 1),
(22, 1),
(22, 2),
(28, 1),
(28, 2),
(29, 1),
(29, 2),
(29, 28),
(30, 1),
(30, 2),
(30, 28),
(31, 1),
(31, 2),
(31, 28),
(1001, 1),
(1002, 1),
(1002, 1001),
(1004, 1),
(1004, 1001),
(1005, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages_sortfields`
--

CREATE TABLE `pages_sortfields` (
  `pages_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `sortfield` varchar(20) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `page_path_history`
--

CREATE TABLE `page_path_history` (
  `path` varchar(250) NOT NULL,
  `pages_id` int(10) UNSIGNED NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page_path_history`
--

INSERT INTO `page_path_history` (`path`, `pages_id`, `created`) VALUES
('/localidades', 1014, '2019-06-11 06:36:02');

-- --------------------------------------------------------

--
-- Table structure for table `session_login_throttle`
--

CREATE TABLE `session_login_throttle` (
  `name` varchar(128) NOT NULL,
  `attempts` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `last_attempt` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `session_login_throttle`
--

INSERT INTO `session_login_throttle` (`name`, `attempts`, `last_attempt`) VALUES
('invitado', 1, 1560240594);

-- --------------------------------------------------------

--
-- Table structure for table `templates`
--

CREATE TABLE `templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) CHARACTER SET ascii NOT NULL,
  `fieldgroups_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `flags` int(11) NOT NULL DEFAULT '0',
  `cache_time` mediumint(9) NOT NULL DEFAULT '0',
  `data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `templates`
--

INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES
(2, 'admin', 2, 8, 0, '{\"useRoles\":1,\"parentTemplates\":[2],\"allowPageNum\":1,\"redirectLogin\":23,\"slashUrls\":1,\"noGlobal\":1,\"compile\":3,\"modified\":1545409848,\"ns\":\"ProcessWire\"}'),
(3, 'user', 3, 8, 0, '{\"useRoles\":1,\"noChildren\":1,\"parentTemplates\":[2],\"slashUrls\":1,\"pageClass\":\"User\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noSettings\":1,\"noChangeTemplate\":1,\"nameContentTab\":1}'),
(4, 'role', 4, 8, 0, '{\"noChildren\":1,\"parentTemplates\":[2],\"slashUrls\":1,\"pageClass\":\"Role\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noSettings\":1,\"noChangeTemplate\":1,\"nameContentTab\":1}'),
(5, 'permission', 5, 8, 0, '{\"noChildren\":1,\"parentTemplates\":[2],\"slashUrls\":1,\"guestSearchable\":1,\"pageClass\":\"Permission\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noSettings\":1,\"noChangeTemplate\":1,\"nameContentTab\":1}'),
(1, 'home', 1, 0, 0, '{\"useRoles\":1,\"noParents\":1,\"slashUrls\":1,\"compile\":3,\"modified\":1560243283,\"ns\":\"ProcessWire\",\"roles\":[37]}'),
(29, 'basic-page', 83, 0, 0, '{\"slashUrls\":1,\"compile\":3,\"modified\":1545409848,\"ns\":\"ProcessWire\"}'),
(26, 'search', 80, 0, 0, '{\"noChildren\":1,\"noParents\":1,\"allowPageNum\":1,\"slashUrls\":1,\"compile\":3,\"modified\":1545409848,\"ns\":\"ProcessWire\"}'),
(34, 'sitemap', 88, 0, 0, '{\"noChildren\":1,\"noParents\":1,\"redirectLogin\":23,\"slashUrls\":1,\"compile\":3,\"modified\":1545409848,\"ns\":\"ProcessWire\"}'),
(43, 'cliente', 97, 0, 0, '{\"slashUrls\":1,\"compile\":3,\"modified\":1560226015}'),
(44, 'localizacion', 98, 0, 0, '{\"slashUrls\":1,\"compile\":3,\"modified\":1560236644,\"ns\":\"ProcessWire\"}'),
(45, 'bitacora', 99, 0, 0, '{\"slashUrls\":1,\"compile\":3,\"modified\":1560226820}'),
(46, 'form-builder', 100, 8, 0, '{\"noParents\":1,\"urlSegments\":1,\"slashUrls\":1,\"noGlobal\":1,\"compile\":3,\"modified\":1560228091,\"ns\":\"\\\\\"}'),
(47, 'cliente_listado', 101, 0, 0, '{\"slashUrls\":1,\"compile\":3,\"modified\":1560236589,\"ns\":\"ProcessWire\"}'),
(48, 'cliente_nuevo', 102, 0, 0, '{\"slashUrls\":1,\"compile\":3,\"modified\":1560234699,\"ns\":\"ProcessWire\"}'),
(49, 'cliente_localizacion', 103, 0, 0, '{\"slashUrls\":1,\"compile\":3,\"modified\":1560236143,\"ns\":\"ProcessWire\"}'),
(50, 'cliente_modificar', 104, 0, 0, '{\"slashUrls\":1,\"compile\":3,\"modified\":1560238193,\"ns\":\"ProcessWire\"}'),
(51, 'cliente_deshabilitar', 105, 0, 0, '{\"slashUrls\":1,\"compile\":3,\"modified\":1560238172,\"ns\":\"ProcessWire\"}'),
(52, 'bienvenida', 106, 0, 0, '{\"slashUrls\":1,\"compile\":3,\"modified\":1560239130,\"ns\":\"ProcessWire\"}'),
(53, 'reporte', 107, 0, 0, '{\"slashUrls\":1,\"compile\":3,\"modified\":1560241446,\"ns\":\"ProcessWire\"}');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `caches`
--
ALTER TABLE `caches`
  ADD PRIMARY KEY (`name`),
  ADD KEY `expires` (`expires`);

--
-- Indexes for table `fieldgroups`
--
ALTER TABLE `fieldgroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `fieldgroups_fields`
--
ALTER TABLE `fieldgroups_fields`
  ADD PRIMARY KEY (`fieldgroups_id`,`fields_id`);

--
-- Indexes for table `fields`
--
ALTER TABLE `fields`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `fieldtype_options`
--
ALTER TABLE `fieldtype_options`
  ADD PRIMARY KEY (`fields_id`,`option_id`),
  ADD UNIQUE KEY `title` (`title`(250),`fields_id`),
  ADD KEY `value` (`value`,`fields_id`),
  ADD KEY `sort` (`sort`,`fields_id`);
ALTER TABLE `fieldtype_options` ADD FULLTEXT KEY `title_value` (`title`,`value`);

--
-- Indexes for table `field_admin_theme`
--
ALTER TABLE `field_admin_theme`
  ADD PRIMARY KEY (`pages_id`),
  ADD KEY `data` (`data`);

--
-- Indexes for table `field_bitacora_comentario`
--
ALTER TABLE `field_bitacora_comentario`
  ADD PRIMARY KEY (`pages_id`),
  ADD KEY `data_exact` (`data`(250));
ALTER TABLE `field_bitacora_comentario` ADD FULLTEXT KEY `data` (`data`);

--
-- Indexes for table `field_body`
--
ALTER TABLE `field_body`
  ADD PRIMARY KEY (`pages_id`);
ALTER TABLE `field_body` ADD FULLTEXT KEY `data` (`data`);

--
-- Indexes for table `field_cliente_bitacora`
--
ALTER TABLE `field_cliente_bitacora`
  ADD PRIMARY KEY (`pages_id`,`sort`),
  ADD KEY `data` (`data`,`pages_id`,`sort`);

--
-- Indexes for table `field_cliente_correo`
--
ALTER TABLE `field_cliente_correo`
  ADD PRIMARY KEY (`pages_id`),
  ADD KEY `data_exact` (`data`(250));
ALTER TABLE `field_cliente_correo` ADD FULLTEXT KEY `data` (`data`);

--
-- Indexes for table `field_cliente_empresa`
--
ALTER TABLE `field_cliente_empresa`
  ADD PRIMARY KEY (`pages_id`),
  ADD KEY `data_exact` (`data`(250));
ALTER TABLE `field_cliente_empresa` ADD FULLTEXT KEY `data` (`data`);

--
-- Indexes for table `field_cliente_estado`
--
ALTER TABLE `field_cliente_estado`
  ADD PRIMARY KEY (`pages_id`,`sort`),
  ADD KEY `data` (`data`);

--
-- Indexes for table `field_cliente_location`
--
ALTER TABLE `field_cliente_location`
  ADD PRIMARY KEY (`pages_id`,`sort`),
  ADD KEY `data` (`data`,`pages_id`,`sort`);

--
-- Indexes for table `field_cliente_location_detalle`
--
ALTER TABLE `field_cliente_location_detalle`
  ADD PRIMARY KEY (`pages_id`),
  ADD KEY `data_exact` (`data`(250));
ALTER TABLE `field_cliente_location_detalle` ADD FULLTEXT KEY `data` (`data`);

--
-- Indexes for table `field_cliente_nombre_completo`
--
ALTER TABLE `field_cliente_nombre_completo`
  ADD PRIMARY KEY (`pages_id`),
  ADD KEY `data_exact` (`data`(250));
ALTER TABLE `field_cliente_nombre_completo` ADD FULLTEXT KEY `data` (`data`);

--
-- Indexes for table `field_cliente_sitio_web`
--
ALTER TABLE `field_cliente_sitio_web`
  ADD PRIMARY KEY (`pages_id`),
  ADD KEY `data_exact` (`data`(250));
ALTER TABLE `field_cliente_sitio_web` ADD FULLTEXT KEY `data` (`data`);

--
-- Indexes for table `field_cliente_telefono`
--
ALTER TABLE `field_cliente_telefono`
  ADD PRIMARY KEY (`pages_id`),
  ADD KEY `data_exact` (`data`(250));
ALTER TABLE `field_cliente_telefono` ADD FULLTEXT KEY `data` (`data`);

--
-- Indexes for table `field_email`
--
ALTER TABLE `field_email`
  ADD PRIMARY KEY (`pages_id`),
  ADD KEY `data_exact` (`data`);
ALTER TABLE `field_email` ADD FULLTEXT KEY `data` (`data`);

--
-- Indexes for table `field_headline`
--
ALTER TABLE `field_headline`
  ADD PRIMARY KEY (`pages_id`);
ALTER TABLE `field_headline` ADD FULLTEXT KEY `data` (`data`);

--
-- Indexes for table `field_images`
--
ALTER TABLE `field_images`
  ADD PRIMARY KEY (`pages_id`,`sort`),
  ADD KEY `data` (`data`),
  ADD KEY `modified` (`modified`),
  ADD KEY `created` (`created`);
ALTER TABLE `field_images` ADD FULLTEXT KEY `description` (`description`);
ALTER TABLE `field_images` ADD FULLTEXT KEY `filedata` (`filedata`);

--
-- Indexes for table `field_pass`
--
ALTER TABLE `field_pass`
  ADD PRIMARY KEY (`pages_id`),
  ADD KEY `data` (`data`);

--
-- Indexes for table `field_permissions`
--
ALTER TABLE `field_permissions`
  ADD PRIMARY KEY (`pages_id`,`sort`),
  ADD KEY `data` (`data`,`pages_id`,`sort`);

--
-- Indexes for table `field_process`
--
ALTER TABLE `field_process`
  ADD PRIMARY KEY (`pages_id`),
  ADD KEY `data` (`data`);

--
-- Indexes for table `field_roles`
--
ALTER TABLE `field_roles`
  ADD PRIMARY KEY (`pages_id`,`sort`),
  ADD KEY `data` (`data`,`pages_id`,`sort`);

--
-- Indexes for table `field_sidebar`
--
ALTER TABLE `field_sidebar`
  ADD PRIMARY KEY (`pages_id`);
ALTER TABLE `field_sidebar` ADD FULLTEXT KEY `data` (`data`);

--
-- Indexes for table `field_summary`
--
ALTER TABLE `field_summary`
  ADD PRIMARY KEY (`pages_id`);
ALTER TABLE `field_summary` ADD FULLTEXT KEY `data` (`data`);

--
-- Indexes for table `field_title`
--
ALTER TABLE `field_title`
  ADD PRIMARY KEY (`pages_id`),
  ADD KEY `data_exact` (`data`(255));
ALTER TABLE `field_title` ADD FULLTEXT KEY `data` (`data`);

--
-- Indexes for table `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `forms_entries`
--
ALTER TABLE `forms_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `forms_id` (`forms_id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `class` (`class`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_parent_id` (`name`,`parent_id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `templates_id` (`templates_id`),
  ADD KEY `modified` (`modified`),
  ADD KEY `created` (`created`),
  ADD KEY `status` (`status`),
  ADD KEY `published` (`published`);

--
-- Indexes for table `pages_access`
--
ALTER TABLE `pages_access`
  ADD PRIMARY KEY (`pages_id`),
  ADD KEY `templates_id` (`templates_id`);

--
-- Indexes for table `pages_parents`
--
ALTER TABLE `pages_parents`
  ADD PRIMARY KEY (`pages_id`,`parents_id`);

--
-- Indexes for table `pages_sortfields`
--
ALTER TABLE `pages_sortfields`
  ADD PRIMARY KEY (`pages_id`);

--
-- Indexes for table `page_path_history`
--
ALTER TABLE `page_path_history`
  ADD PRIMARY KEY (`path`),
  ADD KEY `pages_id` (`pages_id`),
  ADD KEY `created` (`created`);

--
-- Indexes for table `session_login_throttle`
--
ALTER TABLE `session_login_throttle`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `templates`
--
ALTER TABLE `templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `fieldgroups_id` (`fieldgroups_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fieldgroups`
--
ALTER TABLE `fieldgroups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `fields`
--
ALTER TABLE `fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `forms`
--
ALTER TABLE `forms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `forms_entries`
--
ALTER TABLE `forms_entries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=173;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1213;

--
-- AUTO_INCREMENT for table `templates`
--
ALTER TABLE `templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
