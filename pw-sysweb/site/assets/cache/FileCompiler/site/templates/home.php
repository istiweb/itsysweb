<?php namespace ProcessWire;

// home.php (homepage) template file. 
// See README.txt for more information

// Primary content is the page body copy and navigation to children. 
// See the _func.php file for the renderNav() function example
$content = $page->body . renderNav($page->children); 

// if there are images, lets choose one to output in the sidebar
if(count($page->images)) {
	// if the page has images on it, grab one of them randomly... 
	$image = $page->images->getRandom();
	// resize it to 400 pixels wide
	$image = $image->width(400); 
	// output the image at the top of the sidebar...
	$sidebar = "<img src='$image->url' alt='$image->description' />";
	// ...and append sidebar text under the image
	$sidebar .= $page->sidebar;	
} else {
	// no images... 
	// append sidebar text if the page has it
	$sidebar = $page->sidebar; 
}
$contentMain = '';

$contentMain .=
'<div class="row">

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Clientes</div>
                      <a href="'. wire("config")->urls->root .'clientes" class="btn btn-primary btn-icon-split btn-lg">
	                    <span class="icon text-white-50">
	                      <i class="fas fa-user"></i>
	                    </span>
	                    <span class="text">Ver listado</span>
	                  </a>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-user fa-2x text-gray-300"></i>
                    </div>
                     
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Cliente nuevo</div>
                       <a href="'. wire("config")->urls->root.'clientes/nuevo-cliente" class="btn btn-success btn-icon-split btn-lg">
	                    <span class="icon text-white-50">
	                      <i class="fas fa-user-plus"></i>
	                    </span>
	                    <span class="text">Crear nuevo</span>
	                  </a>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-user-plus fa-1x  text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Buscar por:</div>
                      <div class="row no-gutters align-items-center">
                        <a href="'. wire("config")->urls->root.'localizacion" class="btn btn-info btn-icon-split btn-lg">
	                    <span class="icon text-white-50">
	                      <i class="fas fa-search-location"></i>
	                    </span>
	                    <span class="text">Ubicación</span>
	                  </a>
                      </div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-search-location fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Dar de baja usuario</div>
                      <a href="'. wire("config")->urls->root.'clientes/deshabilitar-cliente" class="btn btn-danger btn-icon-split btn-lg">
	                    <span class="icon text-white-50">
	                      <i class="fas fa-user-minus"></i>
	                    </span>
	                    <span class="text">Deshabilitar</span>
	                  </a>
                    </div>
                     
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-6 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Modificar cliente:</div>
                      <div class="row no-gutters align-items-center">
                        <a  href="'. wire("config")->urls->root.'clientes/modificar-cliente" class="btn btn-warning btn-icon-split btn-lg">
                      <span class="icon text-white-50">
                        <i class="fas fa-user-edit"></i>
                      </span>
                      <span class="text">Editar información de cliente </span>
                    </a>
                      </div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-user-edit fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-6 col-md-6 mb-4">
              <div class="card border-left-secondary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-secondary text-uppercase mb-1">Generar documento:</div>
                      <div class="row no-gutters align-items-center">
                        <a target="_blank" href="'. wire("config")->urls->root.'reporte" class="btn btn-secondary btn-icon-split btn-lg">
                      <span class="icon text-white-50">
                        <i class="fas fa-file-pdf"></i>
                      </span>
                      <span class="text">Generar Reporte PDF </span>
                    </a>
                      </div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-file-pdf fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>




          </div>';