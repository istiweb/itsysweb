<?php namespace ProcessWire;
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

	<title>
	 	<?php
		echo $page->title;
		if(stripos($page->title, 'SIAFF | Sistema de Información Administrativa FECE y FEAA') === false) {
			echo ' | SIAFF';
		}
		?>
	</title>

    <!-- Bootstrap core CSS-->
    <link href="<?php echo $config->urls->templates; ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="<?php echo $config->urls->templates; ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template-->
    <link href="<?php echo $config->urls->templates; ?>css/sb-admin.css" rel="stylesheet">
    <link href="<?php echo $config->urls->templates; ?>css/custom.css" rel="stylesheet">

  </head>

  <body class="bg-login">

    <div class="container-fluid">
      <div class="card card-login mx-auto mt-5">
        <div class="card-header">
          <div class="row">
            <div class="col-xl-4 col-sm-4 col-md-4 mb-3">
              <img src="<?php echo $config->urls->templates; ?>img/meduca-logo.png" class="img-fluid">
            </div>
            <div class="col-xl-8 col-sm-8 col-md-8 mb-3 mt-1 card-title">
              <b>SIAFF-MEDUCA</b>
            </div>
          </div>
        </div>
        <div class="card-body">
          <p class="mr-5 ml-5 text-b">Ingrese su usuario y contraseña para acceder al sistema; en la parte inferior encontrará link de ayuda.</p> 
            <img  width="100%" src="<?php echo $config->urls->templates; ?>img/gray-line.png" class="img-fluid mb-3">

			<?php  

        $loginRegister = $modules->get('LoginRegister');
        $loginRegister->set('renderStyles', false); 
				if($user->isLoggedin() && !$input->get('profile') && !$input->get('logout')) {
					// you take over and render your own output or redirect elsewhere
					$session->redirect(wire("config")->urls->root); 
				} else {
          // let the LoginRegister module have control
          
					echo $modules->get('LoginRegister')->execute(); 
				}
			
			?>
 
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="<?php echo $config->urls->templates; ?>vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo $config->urls->templates; ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?php echo $config->urls->templates; ?>vendor/jquery-easing/jquery.easing.min.js"></script>

  </body>

</html>
