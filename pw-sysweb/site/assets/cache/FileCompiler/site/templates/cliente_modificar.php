<?php namespace ProcessWire;

// basic-page.php template file 
// See README.txt for more information

// Primary content is the page's body copy
$content = $page->body; 

// If the page has children, then render navigation to them under the body.
// See the _func.php for the renderNav example function.
if($page->hasChildren) {
  $content .= renderNav($page->children);
}

// if the rootParent (section) page has more than 1 child, then render 
// section navigation in the sidebar
if($page->rootParent->hasChildren > 1) {
  $sidebar = renderNavTree($page->rootParent, 3) . $page->sidebar; 
}

$contentMain = '';

$contentMain .=

'<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Listado de clientes en el sistema</h1>
          <p class="mb-4">En la siguiente tabla usted podrá ver el listado de clientes con su información, usted podrá <b> modificar la información</b> de el cliente haciendo clic en editar</p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Clientes</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive" style="font-size:10px;">
                <table class="table table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
                  <thead class="thead-success">
                    <tr>
                      <th>Cliente</th>
                      <th>Nombre cliente</th>
                      <th>Ubicación</th>
                      <th>Dirección</th>
                      <th>Correo</th>
                      <th>Teléfono</th>
                      <th>Sitio web</th>
                      <th>Acción</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Cliente</th>
                      <th>Nombre cliente</th>
                      <th>Ubicación</th>
                      <th>Dirección</th>
                      <th>Correo</th>
                      <th>Teléfono</th>
                      <th>Sitio web</th>
                      <th>Acción</th>
                    </tr>
                  </tfoot>
                  <tbody>';

/************ Query a base de datos para buscar clientes *****/

$clienteinfo = $pages->find("template=cliente, sort=-created, cliente_estado=1|0");

foreach ($clienteinfo as $key => $cliente) {
  $contentMain .= ' 
                    <tr>
                      <td>'. $cliente->title .'  </td>
                      <td>'. $cliente->cliente_nombre_completo .'</td>
                      <td>'. $cliente->cliente_location->title .'</td>
                      <td>'. $cliente->cliente_location_detalle .'</td>
                      <td>'. $cliente->cliente_correo .'</td>
                      <td>'. $cliente->cliente_telefono .'</td>
                      <td>'. $cliente->cliente_sitio_web .'</td>
                       <td class="table-warning">'. $cliente->feel(array(
                            "text" => "Editar cliente",
                              "fields" => "title,cliente_nombre_completo,cliente_location,cliente_location_detalle,cliente_correo,cliente_telefono,cliente_sitio_web"
                              )
                          ) .'</td>
                    </tr>';
}
$contentMain .= ' 


                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->
';