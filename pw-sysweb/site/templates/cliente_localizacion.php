<?php namespace ProcessWire;

// home.php (homepage) template file. 
// See README.txt for more information

// Primary content is the page body copy and navigation to children. 
// See the _func.php file for the renderNav() function example
$content = $page->body . renderNav($page->children); 

// if there are images, lets choose one to output in the sidebar
if(count($page->images)) {
	// if the page has images on it, grab one of them randomly... 
	$image = $page->images->getRandom();
	// resize it to 400 pixels wide
	$image = $image->width(400); 
	// output the image at the top of the sidebar...
	$sidebar = "<img src='$image->url' alt='$image->description' />";
	// ...and append sidebar text under the image
	$sidebar .= $page->sidebar;	
} else {
	// no images... 
	// append sidebar text if the page has it
	$sidebar = $page->sidebar; 
}
$contentMain = '';

$contentMain .=
'<div class="row">';


$clienteLocalizacion = $pages->find("template=localizacion, sort=created");

foreach ($clienteLocalizacion as $key => $ubicacion) {


  $clienteCantidad = $pages->find("template=cliente, sort=created, cliente_location=$ubicacion");
   
  $contentMain .= ' 
                    <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Buscar en:</div>
                      <div class="row no-gutters align-items-center">
                        <a href="'. $ubicacion->httpUrl . '" class="btn btn-info btn-icon-split btn-lg">
                      <span class="icon text-white-50">
                        <i class="fas fa-search-location"></i>
                      </span>
                      <span class="text">'. $ubicacion->title . ' ('.count($clienteCantidad )  .')</span>
                    </a>
                      </div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-search-location fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>';
}

            
  $contentMain .= '
            

             
          </div>';