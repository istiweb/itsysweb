


<?php
/*
 * Pages2Pdf default template
 *
 * This template is used if you have enabled creating PDF files for a template in the module config
 * but didn't create a corresponding template file in '/site/templates/pages2pdf/ folder.
 *
 * Styles defined in styles.css file
 */


 

 $html .=
'<div class="row">';


$clienteLocalizacion = $pages->find("template=localizacion, sort=created");

foreach ($clienteLocalizacion as $key => $ubicacion) {


  $clienteCantidad = $pages->find("template=cliente, sort=created, cliente_location=$ubicacion");
   
  $html .= ' 
                    <!-- Earnings (Monthly) Card Example -->
            <div >
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Estadísticas en: '. $ubicacion->title . ' ('.count($clienteCantidad )  .')<hr></div>
                      <div class="row no-gutters align-items-center">
                         
                       
                      <span class="text">'. $ubicacion->title . ' ('.count($clienteCantidad )  .')</span>
                      <br>';
                      $html .= '<table>';
                      $html .= '<tbody>';
                      $html .= '<thead>';
                        $html .= '<tr>';
                        $html .= '<th>Cliente</th>
                                  <th>Nombre cliente</th>
                                  <th>Ubicación</th> 
                                  <th>Correo</th>
                                  <th>Teléfono</th> '; 
                        $html .= '</tr>';
                        $html .= '</thead>';
                       
                      foreach ($clienteCantidad as $key => $itemClientes) {
                         
                        $html .= '<tr>
                         <td>'. $itemClientes->title .'  </td>
                      <td>'. $itemClientes->cliente_nombre_completo .'</td>
                      <td>'. $itemClientes->cliente_location->title .'</td> 
                      <td>'. $itemClientes->cliente_correo .'</td>
                      <td>'. $itemClientes->cliente_telefono .'</td> ';

                        $html .= '</tr>'; 
                        
                      }
                      $html .= '</tbody>';
                      $html .= '</table>';

 $html .= '                   
                      </div>
                    </div>
                     
                  </div>
                </div>
              </div>
            </div>';
}

            
  $html .= '
            

             
          </div>';


?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Reporte 2019</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo $config->urls->templates; ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo $config->urls->templates; ?>css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

<?php

    echo  $html;

?>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo $config->urls->templates; ?>vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo $config->urls->templates; ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo $config->urls->templates; ?>vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo $config->urls->templates; ?>js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="<?php echo $config->urls->templates; ?>vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?php echo $config->urls->templates; ?>js/demo/chart-area-demo.js"></script>
  <script src="<?php echo $config->urls->templates; ?>js/demo/chart-pie-demo.js"></script>
  <script src="<?php echo $config->urls->templates; ?>js/demo/chart-bar-demo.js"></script>

</body>

</html>

